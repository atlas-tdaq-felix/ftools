#include <iostream>
#include <iomanip>
#include <fstream> // for ifstream
using namespace std;
#include <unistd.h>

#include "nlohmann/json.hpp"
using namespace nlohmann;
// NOTE: Ignore the deprecated-declarations warning of gcc13, see FLX-2310
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include "yaml-cpp/yaml.h"
#pragma GCC diagnostic pop

#include "felixtag.h"
//#include "FlxConfig.h"
#include "arg.h"

// Version identifier: year, month, day, release number
const int VERSION_ID = 0x22122300;

static const int32_t FLX_LINKS  = 24;

// The keys of the {key,value}-pairs in RM4 JSON files
static const char *KEY_LINK     = "Link";
static const char *KEY_LINKMODE = "LinkMode";
static const char *KEY_TTCCLOCK = "SetTtcClock";
static const char *KEY_CHUNKMAX = "LinkChunkMaxSize";
static const char *KEY_EGROUPS  = "TheEgroups";
static const char *KEY_EGROUP   = "Egroup";
static const char *KEY_ENA_FH   = "EnableFromHost";
static const char *KEY_ENA_TH   = "EnableToHost";
static const char *KEY_MODE_FH  = "ModeFromHost";
static const char *KEY_MODE_TH  = "ModeToHost";
static const char *KEY_STREAMID = "StreamId";
static const char *KEY_SETTINGS = "RegisterSettings";
static const char *KEY_FIELDNAME= "Name";
static const char *KEY_FIELDVAL = "Value";

// The keys of the {key,value}-pairs in RM5 YAML files
static const char *YML_FORMAT     = "Format";
static const char *YML_LINKS      = "Links";
static const char *YML_LINK       = "Link";
static const char *YML_LINKMODE   = "LinkMode";
static const char *YML_TTCCLOCK   = "SetTtcClock";
static const char *YML_CHUNKMAX   = "LinkChunkMaxSize";
//static const char *YML_EGROUPS    = "Egroups";    // .jelc backwards compat
//static const char *YML_EGROUPS_X  = "TheEgroups"; // .jelc backwards compat
static const char *YML_EGROUP     = "Egroup";
static const char *YML_EGROUPS_TH = "EgroupsToHost";
static const char *YML_WIDTH_TH   = "WidthToHost";
static const char *YML_ENA_TH     = "EnableToHost";
static const char *YML_MODE_TH    = "ModeToHost";
static const char *YML_STREAMID   = "StreamId";
static const char *YML_DMAINDICES = "DmaIndices";
static const char *YML_EGROUPS_FH = "EgroupsFromHost";
static const char *YML_WIDTH_FH   = "WidthFromHost";
static const char *YML_ENA_FH     = "EnableFromHost";
static const char *YML_MODE_FH    = "ModeFromHost";
static const char *YML_TTCOPTION  = "TtcOption";
static const char *YML_SETTINGS   = "RegisterSettings";
static const char *YML_FIELDNAME  = "Name";
static const char *YML_FIELDVAL   = "Value";

typedef struct register_setting {
  std::string name;
  uint64_t value;
} regsetting_t;

// ----------------------------------------------------------------------------

int main( int argc, char *argv[] )
{
  int    opt;
  string filename;

  // Parse the options
  while( (opt = getopt(argc, argv, "hV")) != -1 )
    {
      switch( opt )
        {
        case 'h':
          usage();
          return 0;
        case 'V':
          cout << "Version " << hex << VERSION_ID << dec
               << " (tag: " << FELIX_TAG << ")" << endl;
          return 0;
        default: // '?'
          usage();
          return 1;
        }
    }

  // Name of file with RM4 .jelc configuration
  if( optind < argc )
    filename = string( argv[optind] );
  if( filename.empty() )
    {
      cout << "### Missing configuration file name" << endl;
      return 1;
    }

  // Open file
  std::ifstream file( filename );
  if( !file.is_open() )
    {
      cout << "### Failed to open file: " << filename << endl;
      return 0;
    }

  // File name extension
  std::string ext;
  size_t pos = filename.find_last_of( "." );
  if( pos != string::npos )
    ext = filename.substr( pos+1 );
  if( ext != std::string("jelc") )
    {
      cout << "### WARNING expecting a file with .jelc extension" << endl;
      //return 1;
    }

  // Read file contents (ASCII)
  int32_t  linknr = 0;
  uint32_t egroup = 0;
  uint32_t enables, chunksizes, linkmode, streamid;
  uint64_t modes;
  bool     ttc_clock = false;
  std::vector<regsetting_t> regsettings;

  // To YAML format
  YAML::Emitter yaml;
  yaml << YAML::BeginMap;
  yaml << YAML::Key << YML_FORMAT << YAML::Value << 2;

  try {
    // "Links:"
    yaml << YAML::Key << YML_LINKS << YAML::Value;
    yaml << YAML::BeginSeq;

    // Read JSON formatted file
    json j;
    file >> j;
    for( json::iterator it = j.begin(); it != j.end(); ++it )
      {
        json lnk = *it;
        linknr = lnk[KEY_LINK];
        if( linknr > FLX_LINKS ) // Shouldn't read in past this number!
          continue;
        //if( linknr == -1 ) // Emulator configuration
        //  linknr = FLX_LINKS;

        chunksizes = lnk[KEY_CHUNKMAX];
        //_gbtConfig[linknr].setMaxChunkWord( chunksizes );

        linkmode = lnk[KEY_LINKMODE];
        //_gbtConfig[linknr].setLinkMode( linkmode );

        if( linknr == 0 )
          {
            if( lnk.find( KEY_TTCCLOCK ) != lnk.end() )
              ttc_clock = lnk[KEY_TTCCLOCK];
            else
              ttc_clock = false;
            //_gbtConfig[linknr].setTtcClock( ttc_clock );
          }

        // "Link:"
        yaml << YAML::BeginMap;
        yaml << YAML::Key   << YML_LINK
             << YAML::Value << linknr;

        yaml << YAML::Key   << YML_LINKMODE
             << YAML::Value << linkmode
             << YAML::Key   << YML_CHUNKMAX
             << YAML::Value << chunksizes;

        if( linknr == 0 )
          yaml << YAML::Key   << YML_TTCCLOCK
               << YAML::Value << ttc_clock;

        json egrps = lnk[KEY_EGROUPS];
        istringstream iss;

        // "EgroupsToHost:"
        yaml << YAML::Key << YML_EGROUPS_TH << YAML::Value;
        yaml << YAML::BeginSeq;

        // First pass: ToHost
        for( json::iterator it2 = egrps.begin(); it2 != egrps.end(); ++it2 )
          {
            json egrp = *it2;
            egroup = egrp[KEY_EGROUP];
            if( egroup > 7 ) continue;

            // Convert to integers (from a hexadecimal representation)
            enables = 0; modes = 0;
            iss.clear();
            iss.str( egrp[KEY_ENA_TH] );
            iss >> hex >> enables;
            iss.clear();
            iss.str( egrp[KEY_MODE_TH] );
            iss >> hex >> modes;
            //_gbtConfig[linknr].setEnablesToHost( egroup, enables );
            //_gbtConfig[linknr].setModesToHost( egroup, modes );

            // Optional item (for backwards-compatibility)
            streamid = 0;
            if( egrp.find( KEY_STREAMID ) != egrp.end() )
              {
                iss.clear();
                iss.str( egrp[KEY_STREAMID] );
                iss >> hex >> streamid;
                //_gbtConfig[linknr].setStreamIdBits( egroup, streamid );
              }

            // Translate from RM4 to RM5
            uint64_t width_th = 0, ena_th = 0, mode_th = 0;
            if( egroup == 7 )
              {
                // 'Mini' egroup
                width_th = 0; // 2-bit
                ena_th   = enables;
                mode_th  = modes;
              }
            else if( enables & 0x07F80 )
              {
                width_th = 0; // 2-bit
                ena_th   = enables >> 7;
                mode_th  = modes >> 7*4;
              }
            else if( enables & 0x00078 )
              {
                width_th = 1; // 4-bit
                // Enables
                if( enables & 0x00040 ) ena_th |= 0x40;
                if( enables & 0x00020 ) ena_th |= 0x10;
                if( enables & 0x00010 ) ena_th |= 0x04;
                if( enables & 0x00008 ) ena_th |= 0x01;
                // Modes
                if( enables & 0x00040 ) mode_th |= ((modes >> 6*4) & 0xF)<<6*4;
                if( enables & 0x00020 ) mode_th |= ((modes >> 5*4) & 0xF)<<4*4;
                if( enables & 0x00010 ) mode_th |= ((modes >> 4*4) & 0xF)<<2*4;
                if( enables & 0x00008 ) mode_th |= ((modes >> 3*4) & 0xF)<<0*4;
              }
            else if( enables & 0x00006 )
              {
                width_th = 2; // 8-bit
                // Enables
                if( enables & 0x00004 ) ena_th |= 0x10;
                if( enables & 0x00002 ) ena_th |= 0x01;
                // Modes
                if( enables & 0x00004 ) mode_th |= ((modes >> 2*4) & 0xF)<<4*4;
                if( enables & 0x00002 ) mode_th |= ((modes >> 1*4) & 0xF)<<0*4;
              }
            else if( enables & 0x00001 )
              {
                width_th = 3; // 16-bit
                // Enables
                if( enables & 0x00001 ) ena_th |= 0x01;
                // Modes
                if( enables & 0x00001 ) mode_th |= modes & 0xF;
              }
            ena_th |= enables & 0x10000; // In case of FULLMODE

            // Convert items to string
            string ena_th_str, mode_th_str, streamid_str;
            ostringstream oss;
            oss << setfill('0') << hex;

            oss.clear(); oss.str( "" ); // Reset
            oss << "0x" << setw(4) << ena_th;
            ena_th_str = oss.str();

            oss.clear(); oss.str( "" ); // Reset
            oss << "0x" << setw(15) << mode_th;
            mode_th_str = oss.str();

            oss.clear(); oss.str( "" ); // Reset
            oss << "0x" << setw(2) << streamid;
            streamid_str = oss.str();

            // Write ToHost egroup to YAML
            string dmaindices_str( "0x00000000" ); // Assign all to DMA #0
            yaml << YAML::BeginMap;
            yaml << YAML::Key   << YML_EGROUP
                 << YAML::Value << egroup

                 << YAML::Key   << YML_WIDTH_TH
                 << YAML::Value << width_th  // Integer
                 << YAML::Key   << YML_ENA_TH
                 << YAML::Value << ena_th_str
                 << YAML::Key   << YML_MODE_TH
                 << YAML::Value << mode_th_str
                 << YAML::Key   << YML_DMAINDICES
                 << YAML::Value << dmaindices_str
                 << YAML::Key   << YML_STREAMID
                 << YAML::Value << streamid_str;
            yaml << YAML::EndMap;
          }

        // End "EgroupsToHost"
        yaml << YAML::EndSeq;

        // "EgroupsFromHost:"
        yaml << YAML::Key << YML_EGROUPS_FH << YAML::Value;
        yaml << YAML::BeginSeq;

        // Second pass: FromHost
        for( json::iterator it2 = egrps.begin(); it2 != egrps.end(); ++it2 )
          {
            json egrp = *it2;
            egroup = egrp[KEY_EGROUP];
            if( egroup > 5 ) continue;

            // Convert to integers (from a hexadecimal representation)
            enables = 0; modes = 0;
            iss.clear();
            iss.str( egrp[KEY_ENA_FH] );
            iss >> hex >> enables;
            iss.clear();
            iss.str( egrp[KEY_MODE_FH] );
            iss >> hex >> modes;
            //_gbtConfig[linknr].setEnablesFromHost( egroup, enables );
            //_gbtConfig[linknr].setModesFromHost( egroup, modes );

            // Translate from RM4 to RM5
            uint64_t width_fh = 0, ena_fh = 0, mode_fh = 0;
            if( egroup == 5 )
              {
                // 'Mini' egroup
                width_fh = 0; // 2-bit
                ena_fh   = enables;
                mode_fh  = modes;
              }
            else if( enables & 0x07F80 )
              {
                width_fh = 0; // 2-bit
                ena_fh   = enables >> 7;
                mode_fh  = modes >> 7*4;
              }
            else if( enables & 0x00078 )
              {
                width_fh = 1; // 4-bit
                // Enables
                if( enables & 0x00040 ) ena_fh |= 0x40;
                if( enables & 0x00020 ) ena_fh |= 0x10;
                if( enables & 0x00010 ) ena_fh |= 0x04;
                if( enables & 0x00008 ) ena_fh |= 0x01;
                // Modes
                if( enables & 0x00040 ) mode_fh |= ((modes >> 6*4) & 0xF)<<6*4;
                if( enables & 0x00020 ) mode_fh |= ((modes >> 5*4) & 0xF)<<4*4;
                if( enables & 0x00010 ) mode_fh |= ((modes >> 4*4) & 0xF)<<2*4;
                if( enables & 0x00008 ) mode_fh |= ((modes >> 3*4) & 0xF)<<0*4;
              }
            else if( enables & 0x00006 )
              {
                width_fh = 2; // 8-bit
                // Enables
                if( enables & 0x00004 ) ena_fh |= 0x10;
                if( enables & 0x00002 ) ena_fh |= 0x01;
                // Modes
                if( enables & 0x00004 ) mode_fh |= ((modes >> 2*4) & 0xF)<<4*4;
                if( enables & 0x00002 ) mode_fh |= ((modes >> 1*4) & 0xF)<<0*4;
              }
            else if( enables & 0x00001 )
              {
                width_fh = 3; // 16-bit
                // Enables
                if( enables & 0x00001 ) ena_fh |= 0x01;
                // Modes
                if( enables & 0x00001 ) mode_fh |= modes & 0xF;
              }

            // Convert items to string
            string ena_fh_str, mode_fh_str;
            ostringstream oss;
            oss << setfill('0') << hex;

            oss.clear(); oss.str( "" ); // Reset
            oss << "0x" << setw(4) << ena_fh;
            ena_fh_str = oss.str();

            oss.clear(); oss.str( "" ); // Reset
            oss << "0x" << setw(15) << mode_fh;
            mode_fh_str = oss.str();

            // Write FromHost egroup to YAML
            yaml << YAML::BeginMap;
            yaml << YAML::Key   << YML_EGROUP
                 << YAML::Value << egroup

                 << YAML::Key   << YML_WIDTH_FH
                 << YAML::Value << width_fh // Integer
                 << YAML::Key   << YML_ENA_FH
                 << YAML::Value << ena_fh_str
                 << YAML::Key   << YML_MODE_FH
                 << YAML::Value << mode_fh_str
                 << YAML::Key   << YML_TTCOPTION
                 << YAML::Value << 0;       // Integer
            yaml << YAML::EndMap;
          }

        // End "EgroupsFromHost"
        yaml << YAML::EndSeq;

        // End "Link"
        yaml << YAML::EndMap;

        // Any additional FELIX register/bitfield settings present ?
        // (save and add to file at the end..)
        if( lnk.find( KEY_SETTINGS ) != lnk.end() )
          {
            regsetting_t setting;
            json jregs = lnk[KEY_SETTINGS];
            json::iterator it;
            for( it = jregs.begin(); it != jregs.end(); ++it )
              {
                json jsetting = *it;
                setting.name = std::string( jsetting[KEY_FIELDNAME] );
                istringstream iss;
                iss.str( jsetting[KEY_FIELDVAL] );
                iss >> hex >> setting.value;

                // Add the setting to our list
                regsettings.push_back( setting );
              }
          }
      }

    // End "Links"
    yaml << YAML::EndSeq;
  }
  catch( std::exception &ex ) {
    cout << "### Error while reading JELC-file:" << endl
         << "    " << ex.what() << endl;
    return 1;
  }

  file.close();

  // Were any FELIX register settings present in the file? Output these too
  if( regsettings.size() > 0 )
    {
      yaml << YAML::Key << YML_SETTINGS
           << YAML::Value; // "RegisterSettings:"
      yaml << YAML::BeginSeq;
      for( regsetting_t r : regsettings )
        {
          yaml << YAML::BeginMap;
          yaml << YAML::Key << YML_FIELDNAME
               << YAML::Value << r.name
               << YAML::Key << YML_FIELDVAL
               << YAML::Value;
          ostringstream oss;
          oss << "0x" << hex << uppercase << r.value;
          yaml << oss.str();
          yaml << YAML::EndMap;
        }
      yaml << YAML::EndSeq;
    }

  yaml << YAML::EndMap;

  // As YAML output...
  cout << yaml.c_str() << endl;

  return 0;
}

// ----------------------------------------------------------------------------

void usage()
{
  cout << "fj2y version " << hex << VERSION_ID << dec << endl <<
    "Convert RM4 .jelc file to RM5 .yelc format:\n"
    "redirect to save the output to a new configuration file.\n"
    "Usage: fj2y [-h|V] <filename.jelc> [> somename.yelc]\n"
    "  -h          : Show this help text.\n"
    "  -V          : Show version.\n"
    " <filename>   : Name of .jelc file with FLX-device (E-)link configuration\n"
    "                for GBT or FULL-mode.\n";
}

// ----------------------------------------------------------------------------
