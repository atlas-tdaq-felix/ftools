#include <iostream>
#include <iomanip>
using namespace std;
#include <unistd.h>
#include <string.h>

#include "felixtag.h"
#include "FlxUpload.h"
#include "FlxReceiver.h"
#include "FlxParser.h"
#include "arg.h"
#include "ic.h"

// Version identifier: year, month, day, release number
const int VERSION_ID = 0x22101100; // Port of fscai2c-like functionality
//const int VERSION_ID = 0x22101100; // Port of fscai2c-like functionality

// Function declarations
bool writeLpgbtI2c( FlxUpload *fup, FlxReceiver *frecvr,
                    int linknr, int i2c_addr_lpgbt,
                    int i2c_master_index, bool i2c_configure,
                    int i2c_freq, int i2c_addr_slave,
                    int i2c_regaddr_sz, int i2c_regaddr,
                    bool lsb_first,
                    int i2c_data_sz, uint8_t *i2c_data,
                    bool debug, bool display );
bool readLpgbtI2c ( FlxUpload *fup, FlxReceiver *frecvr,
                    int linknr, int i2c_addr_lpgbt,
                    int i2c_master_index, bool i2c_configure,
                    int i2c_freq, int i2c_addr_slave,
                    int i2c_regaddr_sz, int i2c_regaddr,
                    bool lsb_first,
                    int i2c_data_sz, uint8_t *i2c_data,
                    bool debug, bool display );

// ----------------------------------------------------------------------------

int main( int argc, char *argv[] )
{
  int  opt;
  int  cardnr           = 0;
  int  dma_write        = -1;// Autoselect FromHost DMA controller index
  int  dma_read         = 0;
  int  linknr           = -1;
  bool receive          = true;
  bool use_intrpt       = false;
  int  i2c_addr_lpgbt   = -1;
  int  i2c_freq         = 400; // KHz
  int  i2c_master_index = 0;
  int  i2c_addr_slave   = -1;
  int  i2c_regaddr      = -1;
  int  i2c_regaddr_sz   = 0;
  int  i2c_data         = -1;
  int  i2c_data_sz      = 0;
  bool i2c_read         = true; // An I2C read (or write) operation
  bool lsb_first        = false;
  bool debug            = false;
  bool display          = false; // Display lpGBT (I2C) register operations

  // Parse the options
  unsigned int x;
  while( (opt = getopt(argc, argv, "a:A:d:Df:G:hI:i:Lm:r:VXZ")) != -1 )
    {
      switch( opt )
        {
        case 'a':
          // I2C register address (1-byte)
          if( optarg[0] == '0' && (optarg[1] == 'x' || optarg[1] == 'X') )
            {
              if( sscanf( optarg, "%x", &x ) != 1 )
                arg_error( 'a' );
              i2c_regaddr = x;
            }
          else
            {
              if( sscanf( optarg, "%d", &i2c_regaddr ) != 1 )
                arg_error( 'a' );
            }
          if( i2c_regaddr < 0 || i2c_regaddr > 255 ) // One byte
            arg_range( 'a', 0, 255 );
          i2c_regaddr_sz = 1;
          break;
        case 'A':
          // I2C register address (2-byte)
          if( optarg[0] == '0' && (optarg[1] == 'x' || optarg[1] == 'X') )
            {
              if( sscanf( optarg, "%x", &x ) != 1 )
                arg_error( 'A' );
              i2c_regaddr = x;
            }
          else
            {
              if( sscanf( optarg, "%d", &i2c_regaddr ) != 1 )
                arg_error( 'A' );
            }
          if( i2c_regaddr < 0 || i2c_regaddr > 65535 ) // Two bytes
            arg_range( 'A', 0, 65535 );
          i2c_regaddr_sz = 2;
          break;
        case 'd':
          if( sscanf( optarg, "%d", &cardnr ) != 1 )
            arg_error( 'd' );
          break;
        case 'f':
          // I2C bus frequency (in KHz)
          if( sscanf( optarg, "%d", &i2c_freq ) != 1 )
            arg_error( 'f' );
          if( !(i2c_freq == 100 || i2c_freq == 200 ||
                i2c_freq == 400 || i2c_freq == 1000) )
            {
              cout << "### -f: select one of 100,200,400,1000" << endl;
              return 1;
            }
          break;
        case 'G':
          // lpGBT link number
          if( sscanf( optarg, "%d", &linknr ) != 1 )
            arg_error( 'G' );
          if( linknr < 0 || linknr > FLX_LINKS-1 )
            arg_range( 'G', 0, FLX_LINKS-1 );
          break;
        case 'h':
          usage();
          return 0;
        case 'I':
          // lpGBT I2C address
          if( sscanf( optarg, "%x", &x ) != 1 )
            arg_error( 'I' );
          i2c_addr_lpgbt = x;
          if( i2c_addr_lpgbt < 0 || i2c_addr_lpgbt > 127 )
            arg_range_hex( 'I', 0, 127 );
          break;
        case 'i':
          // Slave I2C address
          if( sscanf( optarg, "%x", &x ) != 1 )
            arg_error( 'i' );
          i2c_addr_slave = x;
          if( i2c_addr_slave < 0 || i2c_addr_slave > 127 )
            arg_range_hex( 'i', 0, 127 );
          break;
        case 'L':
          lsb_first = true;
          break;
        case 'm':
          // lpGBT I2C Master index (0, 1 or 2) to use
          if( sscanf( optarg, "%d", &i2c_master_index ) != 1 )
            arg_error( 'm' );
          if( i2c_master_index < 0 || i2c_master_index > 2 )
            arg_range( 'm', 0, 2 );
          break;
        case 'r':
          // I2C number of bytes to read or write
          if( sscanf( optarg, "%d", &i2c_data_sz ) != 1 )
            arg_error( 'r' );
          if( i2c_data_sz < 1 || i2c_data_sz > 4 )
            arg_range( 'r', 1, 4 );
          // NB: lpGBT I2C hardware supports up to 16 bytes total..
          // (i.e. register address + data) ###TODO if needed
          //if( i2c_data_sz < 1 || i2c_data_sz > 16 )
          //  arg_range( 'r', 1, 16 );
          break;
        case 'V':
          cout << "Version " << hex << VERSION_ID << dec
               << " (tag: " << FELIX_TAG << ")" << endl;
          return 0;
        case 'X':
          debug = true;
          display = true;
          break;
        case 'Z':
          receive = false;
          break;
        default: // '?'
          usage();
          return 1;
        }
    }

  // Check for mandatory parameters:
  // lpGBT link number, I2C lpGBT and slave address
  // (not I2C register address: Can also read/write without register address)
  if( linknr < 0 || i2c_addr_lpgbt == -1 || i2c_addr_slave == -1 )
    {
      if( linknr < 0 )
        cout << "### Provide an lpGBT link number (-G)" << endl;
      if( i2c_addr_lpgbt == -1 )
        cout << "### Provide an lpGBT I2C address (-I)" << endl;
      if( i2c_addr_slave == -1 )
        cout << "### Provide a Slave I2C address (-i)" << endl;
      //if( i2c_regaddr == -1 )
      //  cout << "### Provide an I2C register address (-a or -A)" << endl;
      return 1;
    }

  // Value to write ? (currently a hex value up to 4 bytes is supported)
  if( optind < argc )
    {
      char *val = argv[optind];
      if( val[0] == '0' && (val[1] == 'x' || val[1] == 'X') )
        {
          if( sscanf( val, "%x", &x ) == 1 )
            {
              i2c_data = x;
              i2c_read = false;
            }
          else
            {
              cout << "### Invalid (hex) data write value" << endl;
              return 1;
            }
        }
      else
        {
          if( sscanf( val, "%d", &i2c_data ) == 1 )
            {
              i2c_read = false;
            }
          else
            {
              cout << "### Invalid data write value" << endl;
              return 1;
            }
        }
    }

  // -------------------------------------------------------------------------
  // FLX-device sender and receiver

  FlxUpload fup( cardnr, 0, dma_write );
  if( fup.hasError() )
    {
      cout << "### " << fup.errorString() << endl;
      return 1;
    }
  cout << "Opened FLX-device " << cardnr
       << ", firmw " << fup.firmwareVersion() << endl;

  // Check firmware version
  if( !fup.lpgbtType() )
    {
      // It's *not* a firmware for lpGBT
      cout << "### flpgbti2c tool is for RM5/lpGBT firmware only" << endl;
      return 1;
    }

  if( fup.fanOutLocked() )
    cout <<  "**NB**: FanOut-Select registers are locked!" << endl;
  fup.setFanOutForDaq();

  FlxReceiver *frecvr = 0;
  if( receive )
    {
      frecvr = new FlxReceiver( cardnr, 64*1024*1024, dma_read );
      if( frecvr->hasError() )
        {
          cout << "### " << frecvr->errorString() << endl;
          frecvr->stop();
          return 1;
        }
      /*
      cout << "Opened FLX-device " << cardnr
           << ", firmw " << frecvr->firmwareVersion()
           << ", " << frecvr->numberOfChans()
           <<  " chans (cmem bufsize="
           << frecvr->bufferSize() << ", receive: "
           << "DMA #" << dma_read;
      if( use_intrpt )
        cout << " interrupt-driven)" << endl;
      else
        cout << " polling)" << endl;
      */
      frecvr->setUseInterrupt( use_intrpt );
    }

  // -------------------------------------------------------------------------

  // lpGBTv0 or lpGBTv1 ?
  bool lpgbt_v1 = detectLpGbtV1( &fup, frecvr, linknr, i2c_addr_lpgbt );
  if( !lpgbt_v1 )
    {
      cout << "### flpgbti2c for lpGBTv1 only" << endl;
      return 1;
    }

  // -------------------------------------------------------------------------
  // The I2C operation

  // Display info about the requested operation
  cout << endl
       << "Operation: lpGBT@link" << linknr
       << hex << uppercase << setfill('0')
       << "-I2C=0x" << i2c_addr_lpgbt << dec
       << " I2C-master=" << i2c_master_index << hex << ":" << endl
       << "           I2C-addr=0x" << setw(2) << i2c_addr_slave;
  if( i2c_regaddr_sz > 0 )
    {
      cout << " reg=0x" << setw(i2c_regaddr_sz*2) << i2c_regaddr;
      if( lsb_first )
        cout << " (LSB first)";
      else
        cout << " (MSB first)";
    }
  else
    {
      cout << ", without register addr";
    }
  cout << dec;
  if( i2c_read )
    cout << ", read " << i2c_data_sz << " bytes" << endl;
  else
    cout << ", write " << i2c_data_sz << " bytes: 0x"
         << hex << setw(i2c_data_sz*2) << i2c_data << dec << endl;

  bool i2c_configure = true;

  if( i2c_read )
    {
      uint8_t bytes[16];
      if( readLpgbtI2c( &fup, frecvr,
                        linknr, i2c_addr_lpgbt,
                        i2c_master_index, i2c_configure,
                        i2c_freq, i2c_addr_slave,
                        i2c_regaddr_sz, i2c_regaddr,
                        lsb_first,
                        i2c_data_sz, bytes,
                        debug, display ) )
        {
          // Display register address (if any) and data
          if( i2c_regaddr_sz > 0 )
            {
              cout << hex << setfill('0')
                   << "Reg 0x" << (i2c_regaddr_sz == 1 ? setw(2) : setw(4))
                   << i2c_regaddr << " ("
                   << dec << setfill(' ')
                   << setw(3) << i2c_regaddr << "): ";
            }
          else
            {
              cout << "I2C data bytes: ";
            }
          cout << hex << setfill('0');
          for( int i=0; i<i2c_data_sz; ++i )
            cout << " " << setw(2) << (uint32_t) bytes[i];
          cout << endl;
        }
    }
  else
    {
      writeLpgbtI2c( &fup, frecvr,
                     linknr, i2c_addr_lpgbt,
                     i2c_master_index, i2c_configure,
                     i2c_freq, i2c_addr_slave,
                     i2c_regaddr_sz, i2c_regaddr,
                     lsb_first,
                     i2c_data_sz, (uint8_t *) &i2c_data,
                     debug, display );
    }

  // -------------------------------------------------------------------------
  // Finish up

  fup.stop();
  if( frecvr )
    frecvr->stop();
  cout << endl;
  return 0;
}

// ----------------------------------------------------------------------------

void usage()
{
  cout << "flpgbti2c version " << hex << VERSION_ID << dec << endl <<
    "Tool to read or write from and to an I2C slave device (register)\n"
    "through an lpGBT I2C Master connected to an FLX-device link.\n"
    "NB: suitable for lpGBT v1 only, for the time being.\n"
    "Usage:\n"
    " flpgbti2c [-h|V] [-d<devnr>] -G<link> -I<i2c> [-f<freq>]\n"
    "           -m<master> -i<iaddr> [-a|A<addr>] [-r<nbytes>] [-X] [-Z]\n"
    "           [<value-to-write>]\n"
    "  -h         : Show this help text.\n"
    "  -V         : Show version.\n"
    "  -d <devnr> : FLX-device to use (default: 0).\n"
    "  -G <link>  : lpGBT-link number.\n"
    "  -I <i2c>   : lpGBT I2C address (hex).\n"
    "  -m <master>: I2C Master index (0,1 or 2; default:0).\n"
    "  -f <freq>  : I2C bus frequency, in KHz\n"
    "               (100,200,400 or 1000, default: 400).\n"
    "  -i <iaddr> : I2C device address (hex).\n"
    "  -a|A <addr>: I2C register address ('a':1-byte, 'A':2-byte).\n"
    "               (decimal or '0x..' for hexadecimal).\n"
    "  -L         : In case of 2-byte address (-A) LSByte first "
    "(default: MSB first)\n"
    "  -r <nbytes>: Number of register/data bytes,\n"
    "               for now limited to 4 (default: 1).\n"
    "  -X         : Debug mode: display bytes of received frames;\n"
    "               also: continue, even when nothing is received\n"
    "               (e.g. in combination with option -Z).\n"
    "  -Z         : Do NOT receive and process/display replies.\n"
    " <value-to-write>: value to write (decimal or '0x..' for hexadecimal).\n"
    "\n"
    "=> Examples (with lpGBT I2C address 0x70):\n"
    "Read 2-byte register at address 6 (1-byte address)\n"
    "from I2C device with I2C address 0x30 using lpGBT I2C Master 1:\n"
    "  flpgbti2c -G3 -I70 -m1 -i30 -a6 -r2 \n\n"
    "Read 1-byte register at address 6 (2-byte address)\n"
    "from I2C device with I2C address 0x30 using lpGBT I2C Master 1:\n"
    "  flpgbti2c -G3 -I70 -m1 -i30 -A6 -r1 \n\n"
    "Write 0x1234 to 4-byte register at address 6 (2-byte address)\n"
    "to I2C device with I2C address 0x30 using lpGBT I2C Master 1:\n"
    "  flpgbti2c -G3 -I70 -m1 -i30 -A6 -r4 0x1234 \n";
}

// ----------------------------------------------------------------------------
// Slave I2C device register read/write via an lpGBT I2C Master
// ----------------------------------------------------------------------------

#include "lpgbt-regmap/lpgbt-regs-v1.h"

bool writeLpgbtI2c( FlxUpload *fup, FlxReceiver *frecvr,
                    int linknr, int i2c_addr_lpgbt,
                    int i2c_master_index, bool i2c_configure,
                    int i2c_freq, int i2c_addr_slave,
                    int i2c_regaddr_sz, int i2c_regaddr,
                    bool lsb_first,
                    int i2c_data_sz, uint8_t *i2c_data,
                    bool debug, bool display )
{
  // (This function for lpGBTv1 only)
  int addr;
  uint8_t val;
  bool use_ec = false;
  bool lpgbt_v1 = true;

  if( i2c_configure )
    {
      // Configure the I2CCtrl register to write the register address bytes
      // plus the register/data bytes
      if( i2c_regaddr_sz + i2c_data_sz > 16 )
        {
          cout << "### lpGBT I2C Master write op (16 bytes max): "
               << i2c_regaddr_sz << " + " << i2c_data_sz << " bytes" << endl;
          return false;
        }
      configI2c( fup, frecvr, linknr, i2c_addr_lpgbt,
                 i2c_master_index, i2c_freq, i2c_addr_slave,
                 i2c_regaddr_sz + i2c_data_sz,
                 use_ec, lpgbt_v1 );
    }

  // Assemble the I2C frame for the slave I2C device register write operation
  uint8_t i2c_frame[16];
  int index = 0;
  if( i2c_regaddr_sz > 0 )
    {
      if( i2c_regaddr_sz > 1 )
        {
          if( lsb_first )
            {
              i2c_frame[index++] = (uint8_t) (i2c_regaddr & 0xFF);
              i2c_frame[index++] = (uint8_t) ((i2c_regaddr>>8) & 0xFF);
            }
          else
            {
              i2c_frame[index++] = (uint8_t) ((i2c_regaddr>>8) & 0xFF);
              i2c_frame[index++] = (uint8_t) (i2c_regaddr & 0xFF);
            }
        }
      else
        {
          i2c_frame[index] = (uint8_t) (i2c_regaddr & 0xFF);
          ++index;
        }
    }
  for( int i=0; i<i2c_data_sz; ++i )
    i2c_frame[index+i] = i2c_data[i];
  index += i2c_data_sz;
  // Just in case, set remaining bytes to zero
  for( int i=index; i<16; ++i )
    i2c_frame[i] = (uint8_t) 0x00;

  // Write the assembled frame to the lpGBT I2C data registers
  for( int i=0; i<(i2c_regaddr_sz + i2c_data_sz); i+=4 )
    {
      // Write 4 bytes per operation
      addr = I2CM0DATA0_V1 + 7*i2c_master_index;
      fup->writeIcChannel( linknr, i2c_addr_lpgbt, addr,
                           4, &i2c_frame[i], use_ec, lpgbt_v1 );

      // Store in lpGBT I2C internal data registers
      addr = I2CM0CMD_V1 + 7*i2c_master_index;
      val = 8 + (i/4); // I2C_W_MULTI_4BYTE0,1,2,3
      fup->writeIcChannel( linknr, i2c_addr_lpgbt, addr,
                           1, &val, use_ec, lpgbt_v1 );
    }

  // Execute the I2C write operation
  addr = I2CM0CMD_V1 + 7*i2c_master_index;
  val  = 0xC; // I2C_MULTI_WRITE
  fup->writeIcChannel( linknr, i2c_addr_lpgbt, addr,
                       1, &val, use_ec, lpgbt_v1 );

  // Receive the replies of above operations and display on request
  //receiveIcReply( frecvr, linknr, 0, 0, 0, 8000,
  //                use_ec, lpgbt_v1, debug, display );

  // Wait for the write operation to complete
  // (also receiving/absorbing replies from the operations above)
  int cnt = 0;
  bool reply_received;
  addr = I2CM0STATUS_V1 + 0x15*i2c_master_index;
  uint8_t i2c_status = 0;
  while( (i2c_status & 4) == 0 && cnt < 20 )
    {
      reply_received = readIcRegs( fup, frecvr,
                                   linknr, i2c_addr_lpgbt,
                                   addr, 1, &i2c_status,
                                   use_ec, lpgbt_v1,
                                   debug, display );
      if( !reply_received )
        {
          cout << "### Reply (I2C status/done) not received "
               << "(slave I2C reg=0x" << hex << i2c_regaddr << ")" << endl;
          break;
        }
      ++cnt;
    }
  if( cnt >= 20 )
    cout << "### Timeout on I2C success: I2CMxStatus=0x"
         << hex << (uint32_t) i2c_status << dec << endl;
  /*
  // Read I2C status and transaction count ?
  addr = I2CM0STATUS_V1 + 0x15*i2c_master_index;
  fup->readIcChannel( linknr, i2c_addr, addr, 2, use_ec, lpgbt_v1 );

  // Receive the reply (containing 2 bytes) and display
  reply_received = receiveIcReply( frecvr, linknr,
                                   addr, 2, i2c_frame, 1000,
                                   use_ec, lpgbt_v1,
                                   debug, display );
  if( !reply_received && display )
    cout << "### Reply (I2C status,count) not received (addr=0x"
         << hex << addr << ")" << endl;
  */
  if( debug ) return true;
  return reply_received;
}

// ----------------------------------------------------------------------------

bool readLpgbtI2c( FlxUpload *fup, FlxReceiver *frecvr,
                   int linknr, int i2c_addr_lpgbt,
                   int i2c_master_index, bool i2c_configure,
                   int i2c_freq, int i2c_addr_slave,
                   int i2c_regaddr_sz, int i2c_regaddr,
                   bool lsb_first,
                   int i2c_data_sz, uint8_t *i2c_data,
                   bool debug, bool display )
{
  // (This function for lpGBTv1 only)
  int addr;
  uint8_t val;
  bool use_ec = false;
  bool lpgbt_v1 = true;

  if( i2c_regaddr_sz > 0 )
    {
      if( i2c_configure )
        {
          // Configure the I2CCtrl register to write the register address
          configI2c( fup, frecvr, linknr, i2c_addr_lpgbt,
                     i2c_master_index, i2c_freq,
                     i2c_addr_slave, i2c_regaddr_sz,
                     use_ec, lpgbt_v1 );
        }

      // Slave lpGBT register write I2C frame: the register address
      uint8_t i2c_frame[2];
      if( i2c_regaddr_sz > 1 )
        {
          if( lsb_first )
            {
              i2c_frame[0] = (uint8_t) ((i2c_regaddr>>8) & 0xFF);
              i2c_frame[1] = (uint8_t) (i2c_regaddr & 0xFF);
            }
          else
            {
              i2c_frame[0] = (uint8_t) (i2c_regaddr & 0xFF);
              i2c_frame[1] = (uint8_t) ((i2c_regaddr>>8) & 0xFF);
            }
        }
      else
        {
          i2c_frame[0] = (uint8_t) (i2c_regaddr & 0xFF);
        }

      // Write the frame to the master lpGBT I2C data registers
      addr = I2CM0DATA0_V1 + 7*i2c_master_index;
      fup->writeIcChannel( linknr, i2c_addr_lpgbt, addr,
                           i2c_regaddr_sz, i2c_frame, use_ec, lpgbt_v1 );

      // Store in lpGBT I2C internal data registers
      addr = I2CM0CMD_V1 + 7*i2c_master_index;
      val  = 8; // I2C_W_MULTI_4BYTE0
      fup->writeIcChannel( linknr, i2c_addr_lpgbt, addr,
                           1, &val, use_ec, lpgbt_v1 );

      // Execute the I2C (multi)write operation
      addr = I2CM0CMD_V1 + 7*i2c_master_index;
      val  = 0xC; // I2C_MULTI_WRITE
      fup->writeIcChannel( linknr, i2c_addr_lpgbt, addr,
                           1, &val, use_ec, lpgbt_v1 );

      // Receive the replies of above operations and display only on request
      //receiveIcReply( frecvr, linknr, 0, 0, 0, 8000,
      //                use_ec, lpgbt_v1, debug, display );
      // Wait for the write operation to complete
      // (also receiving/absorbing replies from the operations above)
      int cnt = 0;
      addr = I2CM0STATUS_V1 + 0x15*i2c_master_index;
      uint8_t i2c_status = 0;
      while( (i2c_status & 4) == 0 && cnt < 20 )
        {
          bool reply_received = readIcRegs( fup, frecvr,
                                            linknr, i2c_addr_lpgbt,
                                            addr, 1, &i2c_status,
                                            use_ec, lpgbt_v1,
                                            debug, display );
          if( !reply_received )
            {
              cout << "### Reply (I2C status/done) not received "
                   << "(slave I2C reg=0x" << hex << i2c_regaddr << ")" << endl;
              break;
            }
          ++cnt;
        }
      if( cnt >= 20 )
        cout << "### Timeout on I2C success: I2CMxStatus=0x"
             << hex << (uint32_t) i2c_status << dec << endl;
    }

  if( i2c_data_sz == 1 )
    {
      // Execute a 1-byte I2C read operation
      addr = I2CM0CMD_V1 + 7*i2c_master_index;
      val  = 0x3; // I2C_1BYTE_READ
      fup->writeIcChannel( linknr, i2c_addr_lpgbt, addr,
                           1, &val, use_ec, lpgbt_v1 );
    }
  else
    {
      // Reconfigure the I2CCtrl register to read the requested number of bytes
      configI2c( fup, frecvr, linknr, i2c_addr_lpgbt,
                 i2c_master_index, i2c_freq, i2c_addr_slave, i2c_data_sz,
                 use_ec, lpgbt_v1 );

      // Execute the I2C (multi)read operation
      addr = I2CM0CMD_V1 + 7*i2c_master_index;
      val  = 0xD; // I2C_MULTI_READ
      fup->writeIcChannel( linknr, i2c_addr_lpgbt, addr,
                           1, &val, use_ec, lpgbt_v1 );
    }

  // Receive the replies of above operations and display only on request
  //receiveIcReply( frecvr, linknr, 0, 0, 0, 8000,
  //                use_ec, lpgbt_v1, debug, display );

  // Wait for the read operation to complete
  // (also receiving/absorbing replies from above the operation above)
  int cnt = 0;
  addr = I2CM0STATUS_V1 + 0x15*i2c_master_index;
  uint8_t i2c_status = 0;
  while( (i2c_status & 4) == 0 && cnt < 20 )
    {
      bool reply_received = readIcRegs( fup, frecvr,
                                        linknr, i2c_addr_lpgbt,
                                        addr, 1, &i2c_status,
                                        use_ec, lpgbt_v1,
                                        debug, display );
      if( !reply_received )
        {
          cout << "### Reply (I2C status/done) not received "
               << "(slave I2C reg=0x" << hex << i2c_regaddr << ")" << endl;
          break;
        }
      ++cnt;
    }
  if( cnt >= 20 )
    cout << "### Timeout on I2C success: I2CMxStatus=0x"
         << hex << (uint32_t) i2c_status << dec << endl;

  // Read the slave I2C device register bytes from the lpGBT I2C data registers
  // (NB: Stored starting from I2CMxREAD15, in reversed order:
  //      misunderstanding between I2C Master and lpGBT designers)
  if( i2c_data_sz == 1 )
    addr = I2CM0READBYTE_V1 + 0x15*i2c_master_index;
  else
    addr = (I2CM0READ15_V1 - i2c_data_sz+1) + 0x15*i2c_master_index;
  fup->readIcChannel( linknr, i2c_addr_lpgbt, addr, i2c_data_sz,
                      use_ec, lpgbt_v1 );

  // Receive the reply (containing 'i2c_data_sz' bytes) and display
  bool reply_received = receiveIcReply( frecvr, linknr,
                                        addr, i2c_data_sz, i2c_data, 4000,
                                        use_ec, lpgbt_v1,
                                        debug, display );
  if( !reply_received && display )
    cout << "### Reply (I2C data bytes) not received (lpGBT addr=0x"
         << hex << addr << ")" << dec << endl;

  // Reverse the order of the bytes...
  for( int i=0; i<i2c_data_sz/2; ++i )
    {
      uint8_t tmp = i2c_data[i];
      i2c_data[i] = i2c_data[i2c_data_sz-1-i];
      i2c_data[i2c_data_sz-1-i] = tmp;
    }

  if( debug ) return true;
  return reply_received;
}

// ----------------------------------------------------------------------------
