#include <cstring>
#include <iostream>
#include <iomanip>
using namespace std;
#include <unistd.h>

#include "felixtag.h"
#include "FlxUpload.h"
#include "FlxReceiver.h"
#include "arg.h"
#include "gbtx-items.h"
#include "ic.h"

// Version identifier: year, month, day, release number
const int VERSION_ID = 0x23072500; // First version based on flpgbtconf

// Function declarations
const gbtx_item_t *selectField( const char *fieldname,
                                std::string &info_str );
void listAllFields( FlxUpload *fup = 0,
                    FlxReceiver *frecvr = 0,
                    int linknr = 0, int i2c_addr = 0,
                    bool debug = false );

// ----------------------------------------------------------------------------

int main( int argc, char *argv[] )
{
  int   opt;
  int   cardnr     = 0;
  int   dma_write  = -1; // Autoselect FromHost DMA controller index
  int   dma_read   = 0;
  int   linknr     = -1;
  int   i2c_addr   = -1;
  int   reg_addr   = -1;
  char *fieldname  = 0;
  bool  receive    = true;
  bool  use_intrpt = false;
  bool  write_it   = false;
  bool  debug      = false;

  // Parse the options
  unsigned int x;
  while( (opt = getopt(argc, argv, "d:D:G:hI:VXZ")) != -1 )
    {
      switch( opt )
        {
        case 'd':
          if( sscanf( optarg, "%d", &cardnr ) != 1 )
            arg_error( 'd' );
          break;
        case 'D':
          // DMA controller to use
          if( sscanf( optarg, "%d", &dma_read ) != 1 )
            arg_error( 'D' );
          if( dma_read < 0 || dma_read > 7 )
            arg_range( 'D', 0, 7 );
          break;
        case 'G':
          // GBT link number
          if( sscanf( optarg, "%d", &linknr ) != 1 )
            arg_error( 'G' );
          if( linknr < 0 || linknr > FLX_LINKS-1 )
            arg_range( 'G', 0, FLX_LINKS-1 );
          break;
        case 'h':
          usage();
          return 0;
        case 'I':
          // GBTx I2C address
          if( sscanf( optarg, "%x", &x ) != 1 )
            arg_error( 'I' );
          i2c_addr = x;
          if( i2c_addr < 0 || i2c_addr > 127 )
            arg_range_hex( 'I', 0, 127 );
          break;
        case 'V':
          cout << "Version " << hex << VERSION_ID << dec
               << " (tag: " << FELIX_TAG << ")" << endl;
          return 0;
        case 'X':
          debug = true;
          break;
        case 'Z':
          receive = false;
          break;
        default: // '?'
          return 1;
        }
    }

  if( optind < argc )
    {
      // Convert all chars to uppercase and change any '-' into '_'
      char *name = argv[optind];
      for( unsigned int i=0; i<strlen(name); ++i )
        {
          if( name[i] == '-' )
            name[i] = '_';
          else
            name[i] = toupper( name[i] );
        }

      // Check if a (decimal or hex) number is provided
      // which is to be taken as a register address,
      // else consider it to be a register or bit field name
      bool is_hex = true;
      bool is_dec = true;
      for( unsigned int i=0; i<strlen(name); ++i )
        {
          char ch = name[i];
          if( (i == 0 && ch != '0') ||
              (i == 1 && ch != 'x' && ch != 'X') ||
              (i >= 2 &&
               !((ch >= 'a' && ch <= 'f') ||
                 (ch >= 'A' && ch <= 'F') ||
                 (ch >= '0' && ch <= '9'))) )
            is_hex = false;
 
          if( ch < '0' || ch > '9' )
            is_dec = false;
        }

      if( is_dec )
        {
          // Not a register address after all?
          if( sscanf( name, "%d", &reg_addr ) != 1 )
            fieldname = name;
        }
      else if( is_hex )
        {
          // Not a register address after all?
          if( sscanf( name, "%x", &x ) != 1 )
            fieldname = name;
          else
            reg_addr = x;
        }
      else
        {
          fieldname = name;
        }
    }
  else
    {
      cout << "### Provide a bitfield or register name for read/write" << endl;
      usage();
      return 0;
    }

  FlxUpload *fup = 0;
  FlxReceiver *frecvr = 0;

  // Check for GBTx selection parameters
  if( linknr < 0 && i2c_addr < 0 )
    {
      // No GBTx involved:
      // going to display info about an item (or all items) only
    }
  else if( linknr < 0 || i2c_addr < 0 )
    {
      // Both parameters are required
      if( linknr < 0 )
        cout << "### Provide a GBTx link number (-G)" << endl;
      if( i2c_addr < 0 )
        cout << "### Provide a GBTx I2C address (-I)" << endl;
      return 1;
    }
  else
    {
      fup = new FlxUpload( cardnr, 0, dma_write );
      if( fup->hasError() )
        {
          cout << "### " << fup->errorString() << endl;
          return 1;
        }
      cout << "Opened FLX-device " << cardnr
           << ", firmw " << fup->firmwareVersion() << endl;

      if( fup->fanOutLocked() )
        cout <<  "**NB**: FanOut-Select registers are locked!" << endl;
      fup->setFanOutForDaq();

      if( fup->lpgbtType() || fup->fullmodeType() )
        {
          // It's *not* a firmware for GBTx
          cout << "### fgbtxconf tool is for GBT firmware only" << endl;
          return 1;
        }

      if( receive )
        {
          frecvr = new FlxReceiver( cardnr, 64*1024*1024, dma_read );
          if( frecvr->hasError() )
            {
              cout << "### " << frecvr->errorString() << endl;
              frecvr->stop();
              return 1;
            }
          /*
          cout << "Opened FLX-device " << cardnr
               << ", firmw " << frecvr->firmwareVersion()
               <<  " (cmem buffersize="
               << frecvr->bufferSize() << ", receive: "
               << "DMA #" << dma_read;
          if( use_intrpt )
            cout << " interrupt-driven)" << endl;
          else
            cout << " polling)" << endl;
          */
          frecvr->setUseInterrupt( use_intrpt );
        }
    }

  // SPECIAL: if name is "LIST", display the info of all known
  // registers and bitfields, e.g. for the user to 'grep' on
  if( fieldname && strcmp(fieldname, "LIST") == 0 )
    {
      if( linknr >= 0 && i2c_addr >= 0 )
        // Also display the item values read from the lpGBT device
        listAllFields( fup, frecvr,
                       linknr, i2c_addr, debug );
      else
        listAllFields( );
      return 0;
    }

  int max_addr;
  std::string info_str;
  const gbtx_item_t *item = 0;

  // Check for a valid name or address
  // GBTx: 436 8-bit registers
  max_addr = 436-1;
  if( fieldname )
    {
      item = selectField( fieldname, info_str );
    }
  else
    {
      if( reg_addr < 0 || reg_addr > max_addr )
        {
          cout << "### GBTx address (hex) not in range [0..0x"
               << hex << uppercase << max_addr << "]" << dec << endl;
          return 1;
        }

      // Find the first item corresponding to the given address
      // (which should be the full register)
      const gbtx_item_t *it = &GBTX_ITEM[0];
      while( strlen(it->name) != 0 )
        {
          if( it->addr == reg_addr )
            {
              // Found it
              item = it;
              break;
            }
          ++it;
        }
    }

  // Is the register or bitfield name valid?
  if( item == 0 && reg_addr < 0 )
    {
      if( info_str.empty() )
        {
          if( fieldname )
            cout << "### Bitfield \"" << fieldname
                 << "\" not found" << endl;
          else
            cout << "### Register 0x" << hex << setfill('0') << uppercase
                 << setw(4) << reg_addr << " not found" << endl;
        }
      else
        {
          // Info string is not empty, but item name is invalid,
          // and the string now contains the list of available options
          cout << "->Item not found, suggested options are:"
               << endl << info_str;
        }
      return 1;
    }

  if( item != 0 )
    {
      // Display some info about the item
      cout << item->name << ": addr=0x"
           << hex << uppercase << setfill('0') << setw(3) << item->addr
           << dec << " (" << item->addr << ")"
           << " size=" << item->nbits
           << "bits index=" << item->bitindex
           << (item->readonly ? " R" : " RW") << endl;
      if( strlen(item->desc) > 0 )
        cout << "\"" << item->desc << "\"" << endl;
      else
        cout << "<no description>" << endl;

      reg_addr = item->addr;
    }

  uint32_t val_to_write;
  if( optind < argc-1 )
    {
      // A value to be written is provided, apparently
      char *towrite = argv[optind+1];
      bool  ishex = false;
      if( strlen(towrite) > 2 &&
          towrite[0] == '0' && tolower(towrite[1]) == 'x' )
        ishex = true;

      int result;
      if( ishex )
        {
          // Parse as hexadecimal number
          result = sscanf( towrite, "%x", &val_to_write );
          if( result == 1 )
            arg_check_hex( &towrite[2], 0 );
        }
      else
        {
          // Parse as decimal number
          result = sscanf( towrite, "%u", &val_to_write );
          if( result == 1 )
            arg_check_dec( towrite, 0 );
        }

      if( result != 1 )
        {
          cout << "### Value to write is invalid" << endl;
          return 1;
        }

      //cout << "DEBUG: value = " << towrite << " = " << val_to_write
      //     << ", hex=" << ishex << endl;

      uint32_t max;
      if( item == 0 )
        max = 0xFF;
      else
        max = (1<<item->nbits) - 1;
      if( val_to_write > max )
        {
          cout << "### Value not in range, max=0x"
               << hex << max << dec << " (" << max << ")" << endl;
          return 1;
        }

      write_it = true;

      // Check whether item/register is writable
      if( item->readonly )
        {
          cout << "### Register or item is read-only" << endl;
          return 1;
        }
    }

  if( linknr < 0 && i2c_addr < 0 )
    // Only displayed info about an item (or all items)
    return 0;

  // -------------------------------------------------------------------------

  uint8_t reg_val;
  bool use_ec = false;
  bool lpgbt_v1 = false;

  // First read the register containing the item
  cout << "=> Read:" << endl;
  if( readIcRegs( fup, frecvr, linknr, i2c_addr, reg_addr, 1, &reg_val,
                  use_ec, lpgbt_v1, debug ) )
    {
      if( item )
        {
          uint32_t val;
          val = (reg_val >> item->bitindex) & ((1<<item->nbits)-1);
          cout << item->name << ": 0x" << hex << val
               << " (" << dec << val << ")" << endl;
        }
    }
  else
    {
      if( !debug )
        {
          if( fup )
            fup->stop();
          if( frecvr )
            frecvr->stop();
          return 1;
        }
    }

  if( !write_it )
    {
      if( fup )
        fup->stop();
      if( frecvr )
        frecvr->stop();
      return 0;
    }

  // Going to write: update the register value
  if( item )
    {
      reg_val &= ~(((1<<item->nbits)-1) << item->bitindex);
      reg_val |= (val_to_write << item->bitindex);
    }
  else
    {
      reg_val = val_to_write;
    }

  // Write the updated register value
  cout << "=> Write:" << endl;
  writeIcRegs( fup, frecvr, linknr, i2c_addr, reg_addr, 1, &reg_val,
               use_ec, lpgbt_v1, debug );

  // Re-read the register containing the item
  cout << "=> Reread:" << endl;
  if( readIcRegs( fup, frecvr, linknr, i2c_addr, reg_addr, 1, &reg_val,
                  use_ec, lpgbt_v1, debug ) )
    {
      if( item )
        {
          uint32_t val;
          val = (reg_val >> item->bitindex) & ((1<<item->nbits)-1);
          cout << item->name << ": 0x" << hex << val
               << " (" << dec << val << ")" << endl;
        }
    }

  // -------------------------------------------------------------------------

  if( fup )
    fup->stop();
  if( frecvr )
    frecvr->stop();

  return 0;
}

// ----------------------------------------------------------------------------

const gbtx_item_t *selectField( const char *fieldname,
                                std::string &info_str )
{
  // Searches for the GBTx register bit field name in the list and
  // returns a pointer if found; the name should be exact or a unique substring;
  // returns NULL if not found and returns a string with possible options
  // in 'info_str' (if any)
  bool found = false;
  const gbtx_item_t *item_list = GBTX_ITEM;

  const gbtx_item_t *item = &item_list[0];
  while( strlen(item->name) != 0 )
    {
      if( strcasecmp(item->name, fieldname) == 0 )
        {
          found = true;
          break;
        }
      ++item;
    }

  // Require at least 2 chars to compile a name options list
  size_t len = strlen( fieldname );
  if( !found && len >= 2 )
    {
      // Find out if the given name substring is unique
      // and compile a list of name options at the same time
      int cnt = 0;
      std::ostringstream oss;
      const gbtx_item_t *first_item = 0;
      item = &item_list[0];
      while( strlen(item->name) != 0 )
        {
          if( strncasecmp(item->name, fieldname, len) == 0 )
            {
              first_item = item;
              ++cnt;
              oss << item->name << std::endl;
            }
          ++item;
        }

      if( cnt == 1 )
        {
          // Name is unique
          found = true;
          item = first_item;
        }
      else
        {
          // Add items containing the substring avoiding duplicates (pos != 0)
          if( true ) //if( cnt == 0 )
            {
              // Look for field names with the substring contained
              std::string f;
              item = &item_list[0];
              while( strlen(item->name) != 0 )
                {
                  f = std::string( item->name );
                  size_t pos = f.find( fieldname );
                  // Compile a list of names, avoiding items already found
                  if( pos != std::string::npos && pos != 0 )
                    oss << item->name << std::endl;
                  ++item;
                }
            }
          // Provide the caller with the list of names
          info_str = oss.str();
        }
    }

  if( found )
    // Return string with information about the bit field
    info_str = std::string( item->desc );
  else
    // Nothing found
    item = 0;

  return item;
}

// ----------------------------------------------------------------------------

void listAllFields( FlxUpload *fup, FlxReceiver *frecvr,
                    int linknr, int i2c_addr, bool debug )
{
  cout << "GBTx items:" << endl
       << "-----------" << endl;

  uint8_t regs[512];
  if( fup && frecvr )
    {
      // Read all GBTx registers in one operation
      memset( regs, 0, 512 );
      // Highest addr 435 -> 436 regs; but 366 RW-regs + 68 RO-regs = 434 ?
      uint32_t regcount = 436;
      bool use_ec = false;
      bool lpgbt_v1 = false;
      if( !readIcRegs( fup, frecvr, linknr, i2c_addr, 0, regcount, regs,
                       use_ec, lpgbt_v1, debug, false ) )
        {
          cout << "### readIcRegs() failed" << endl;
          fup = 0; // Skip display of register values below
          return; // ...or return
        }
    }

  const gbtx_item_t *item = &GBTX_ITEM[0];

  while( strlen(item->name) != 0 )
    {
      cout << setw(50) << left
           << item->name << right << setfill( '0' )
           << " 0x" << hex << uppercase
           << setw(3) << item->addr << dec << setfill( ' ' )
           << " (" << setw(3) << item->addr << ")";
      if( item->readonly )
        cout << " R  ";
      else
        cout << " RW ";
      cout << setw(3) << item->nbits << "b";

      cout << " [";
      if( item->nbits > 1 )
        cout << item->bitindex + item->nbits - 1 << ":";
      cout << item->bitindex;
      cout << "]";

      if( fup && frecvr )
        {
          // Display the item's current value
          if( item->nbits == 1 )
            cout << "  ";
          cout << " :   ";

          uint32_t val, width;
          val = (regs[item->addr] >> item->bitindex) & ((1<<item->nbits)-1);
          width = (item->nbits + 3)/4;
          cout << "0x" << hex << setfill('0') << setw(width) << val
               << setfill(' ');
          if( item->nbits <= 4 )
            cout << " ";
          cout << "  (" << dec << val << ")";
        }
      cout << endl;

      ++item;
    }
}

// ----------------------------------------------------------------------------

void usage()
{
  cout << "fgbtxconf version " << hex << VERSION_ID << dec << endl <<
    "Read or write a GBTX register or register bitfield, by address or name.\n"
    "The value of the item is read and displayed, and if requested,\n"
    "written to and re-read and again displayed.\n"
    "Requires the IC channel of the FLX device to be enabled.\n"
    "Without options -G and -I some information about the selected register\n"
    "or bitfield is displayed.\n"
    "'fgbtxconf list' displays all known 'item' names plus additional info\n"
    "about each item, and in combination with options -d/G/I displays\n"
    "the current value of each item of the selected GBTX as well.\n"
    "(NB: for lpGBT devices use the flpgbtconf tool).\n"
    "\n"
    "Usage: fgbtxconf [-h|V] [-d<devnr>] [-D<dma>] [-G<link> -I<i2c>] "
    "[-X] [-Z]\n"
    "                 <name> [<value>]\n"
    "  -h         : Show this help text.\n"
    "  -V         : Show version.\n"
    "  -d <devnr> : FLX-device number (default: 0).\n"
    "  -D <dma>   : FLX-device DMA controller for receiving (default: 0).\n"
    "  -G <link>  : GBT link number.\n"
    "  -I <i2c>   : GBTX I2C address (hex).\n"
    "  -X         : Debug mode: display bytes of received frames;\n"
    "               also: continue, even when nothing is received "
    "(e.g. with -Z).\n"
    "  -Z         : Do NOT receive and process/display replies.\n"
    " <name>      : Name of register or bitfield, or hex (0x) or decimal address.\n"
    "               (Name \"list\" results in output of a list of all known "
    "items,\n"
    "                including their current values in a connected GBTX chip\n"
    "                when options -G/I are provided).\n"
    " <value>     : Value to write to register or bitfield (hex or decimal).\n";
}

// ----------------------------------------------------------------------------
