#include <iostream>
#include <iomanip>
using namespace std;
#include <unistd.h>

#include "felixtag.h"
#include "flxdefs.h"
#include "flxcard/FlxCard.h"
#include "flxcard/FlxException.h"
#include "arg.h"

// Version identifier: year, month, day, release number
const int   VERSION_ID = 0x22083100;

// ----------------------------------------------------------------------------

int main( int argc, char *argv[] )
{
  int  opt;
  int  cardnr    = 0;
  int  gbtnr     = -1;
  int  egroupnr  = -1;
  int  epathnr   = -1;
  int  elinknr   = -1;

  // Parse the options
  while( (opt = getopt(argc, argv, "d:e:g:G:hp:V")) != -1 )
    {
      switch( opt )
        {
        case 'd':
          if( sscanf( optarg, "%d", &cardnr ) != 1 )
            arg_error( 'd' );
          break;
        case 'e':
          // E-link number
          unsigned int x;
          if( sscanf( optarg, "%x", &x ) != 1 )
            arg_error( 'e' );
          elinknr = x;
          if( elinknr < 0 || elinknr > FLX_MAX_ELINK_NR )
            arg_range( 'e', 0, FLX_MAX_ELINK_NR );
          break;
        case 'G':
          // GBT link number
          if( sscanf( optarg, "%d", &gbtnr ) != 1 )
            arg_error( 'G' );
          if( gbtnr < 0 || gbtnr > FLX_LINKS-1 )
            arg_range( 'G', 0, FLX_LINKS-1 );
          break;
        case 'g':
          // E-group number
          if( sscanf( optarg, "%d", &egroupnr ) != 1 )
            arg_error( 'g' );
          if( egroupnr < 0 || egroupnr >= FLX_TOHOST_GROUPS-1 )
            arg_range( 'g', 0, FLX_TOHOST_GROUPS-1-1 );
          //if( egroupnr < 0 || egroupnr >= FLX_FROMHOST_GROUPS-1 )
          //  arg_range( 'g', 0, FLX_FROMHOST_GROUPS-1-1 );
          break;
        case 'h':
          usage();
          return 0;
        case 'p':
          // E-path number
          if( sscanf( optarg, "%d", &epathnr ) != 1 )
            arg_error( 'p' );
          if( epathnr < 0 || epathnr > FLX_ELINK_PATHS-1 )
            arg_range( 'p', 0, FLX_ELINK_PATHS-1 );
          break;
        case 'V':
          cout << "Version " << hex << VERSION_ID << dec
               << " (tag: " << FELIX_TAG << ")" << endl;
          return 0;
        default: // '?'
          usage();
          return 1;
        }
    }

  // Check for either a valid -e or valid -G/g/p options
  if( elinknr == -1 && gbtnr == -1 && egroupnr == -1 && epathnr == -1 )
    {
    }
  else if( (elinknr != -1 && (gbtnr != -1 || egroupnr != -1 || epathnr != -1)) )
    {
      cout << "### Provide either -e or -G/g/p options" << endl;
      return 1;
    }

#if REGMAP_VERSION < 0x500
  cout << "### fela tool is for RM5 firmware only" << endl;
  return 1;
#endif // REGMAP_VERSION

  // Open FELIX FLX-device
  FlxCard *flx = new FlxCard;
  try {
    flx->card_open( cardnr, LOCK_NONE );
  }
  catch( FlxException &ex ) {
    cout << "### FlxCard open: " << ex.what() << endl;
    return 1;
  }

  bool lpgbt = flx->lpgbt_type();
  flxcard_bar2_regs_t *bar2 = (flxcard_bar2_regs_t *) flx->bar2Address();
  uint64_t chans = bar2->NUM_OF_CHANNELS;
  if( chans > FLX_LINKS ) chans = FLX_LINKS;

  // Single E-link number given ?
  if( elinknr != -1 )
    {
      // Derive GBT, e-group and e-path numbers from the given e-link number
      if( lpgbt )
        {
          gbtnr    = (elinknr & BLOCK_LNK_MASK) >> BLOCK_LNK_SHIFT;
          egroupnr = ((elinknr & BLOCK_EGROUP_MASK_LPGBT) >>
                      BLOCK_EGROUP_SHIFT_LPGBT);
          epathnr  = (elinknr & BLOCK_EPATH_MASK_LPGBT);
        }
      else
        {
          gbtnr    = (elinknr & BLOCK_LNK_MASK) >> BLOCK_LNK_SHIFT;
          egroupnr = (elinknr & BLOCK_EGROUP_MASK) >> BLOCK_EGROUP_SHIFT;
          epathnr  = (elinknr & BLOCK_EPATH_MASK);
        }
    }

#if REGMAP_VERSION < 0x500
  uint64_t nstreams = 0;
#else
  uint64_t nstreams = bar2->AXI_STREAMS_TOHOST.NUMBER_OF_STREAMS;
#endif // REGMAP_VERSION

  int max_gbt_cnt;
  if( gbtnr == -1 )
    {
      gbtnr = 0;
      max_gbt_cnt = chans;
    }
  else if( gbtnr >= 0 && gbtnr < (int) chans )
    {
      max_gbt_cnt = gbtnr + 1;
    }
  else
    {
      cout << "### Invalid Link number [0.." << chans-1
           << "] for this FLX-card" << endl;
      flx->card_close();
      return 1;
    }

  int max_egroup_cnt;
  if( egroupnr == -1 )
    {
      egroupnr = 0;
      if( lpgbt )
        max_egroup_cnt = 7;
      else
        max_egroup_cnt = 5;
    }
  else
    {
      if( lpgbt && egroupnr < 7 )
        {
          max_egroup_cnt = egroupnr + 1;
        }
      else if( !lpgbt && egroupnr < 5 )
        {
          max_egroup_cnt = egroupnr + 1;
        }
      else if( !(elinknr != -1 &&
                 (elinknr & ~BLOCK_LNK_MASK) >= (int) nstreams) ) 
        {
          // Accept stream indices not associated with an E-group (EC,IC)
          max_egroup_cnt = egroupnr + 1;
        }
      else
        {
          cout << "### Invalid Egroup number [0.." << (lpgbt ? 6 : 4)
               << "] for this FLX-card" << endl;
          flx->card_close();
          return 1;
        }
    }

  int max_epath_cnt;
  if( epathnr == -1 )
    {
      epathnr = 0;
      if( lpgbt )
        max_epath_cnt = 4;
      else
        max_epath_cnt = 8;
    }
  else
    {
      if( lpgbt && epathnr < 4 )
        {
          max_epath_cnt = epathnr + 1;
        }
      else if( !lpgbt && epathnr < 8 )
        {
          max_epath_cnt = epathnr + 1;
        }
      else
        {
          cout << "### Invalid Epath number [0.." << (lpgbt ? 3 : 7)
               << "] for this FLX-card" << endl;
          flx->card_close();
          return 1;
        }
    }

  cout << setfill( '0' ) << uppercase;
  cout << "Streams-per-link: " << nstreams << " (ToHost)" << endl;
  for( int gbt=gbtnr; gbt<max_gbt_cnt; ++gbt )
    {
#if REGMAP_VERSION >= 0x500
      uint64_t alignment =
        bar2->DECODING_LINK_STATUS_ARR[gbt].DECODING_LINK_ALIGNED;
#else
      uint64_t alignment = 0;
#endif // REGMAP_VERSION

      cout << "Link " << setw(2) << gbt << ": DECODING_LINK_ALIGNED="
           << hex << setw((nstreams+3)/4) << alignment << dec << endl;

      // List the status per E-link when displaying (part of) a single link
      if( max_gbt_cnt-gbtnr == 1 )
        {
          // Stream start index
          uint32_t index;
          if( lpgbt )
            index = egroupnr * 4 + epathnr;
          else
            index = egroupnr * 8 + epathnr;
          cout << "E-link G-g-p Aligned" << endl << setfill('0');
          for( int grp=egroupnr; grp<max_egroup_cnt; ++grp )
            for( int path=epathnr; path<max_epath_cnt; ++path, ++index )
              {
                if( lpgbt )
                  elinknr = ((gbt  << BLOCK_LNK_SHIFT) |
                             (grp  << BLOCK_EGROUP_SHIFT_LPGBT) | path);
                else
                  elinknr = ((gbt  << BLOCK_LNK_SHIFT) |
                             (grp  << BLOCK_EGROUP_SHIFT) | path);
                uint64_t align = (alignment & (1 << index)) >> index;
                cout << "0x" << setfill('0') << setw(3) << hex << elinknr << " "
                     << dec << setfill(' ') << setw(2) << gbt << "-"
                     << grp << "-" << path << "  " << align << endl;
              }
          // The rest... (when looking at a complete link)
          if( max_egroup_cnt-egroupnr > 1 )
            for( ; index<nstreams; ++index )
              {
                elinknr = ((gbt << BLOCK_LNK_SHIFT) | index);
                uint64_t align = (alignment & (1 << index)) >> index;
                cout << "0x" << setfill('0') << setw(3) << hex << elinknr
                     << "         " << align << endl;
              }
        }
    }

  // Close the FLX-device
  try {
    flx->card_close();
  }
  catch( FlxException &ex ) {
    cout << "FlxCard close: " << ex.what() << endl;
  }

  return 0;
}

// ----------------------------------------------------------------------------

void usage()
{
  cout << "fela version " << hex << VERSION_ID << dec << endl <<
    "Display the E-link alignment status, one bit per e-path (e-link).\n"
    "Usage: fela [-h|V] [-d <devnr>] [-e <elink>] "
    "[-G <lnk> [-g <group>] [-p <path>]]\n"
    "  -h         : Show this help text.\n"
    "  -V         : Show version.\n"
    "  -d <devnr> : FLX-device to use (default: 0).\n"
    "  -e <elink> : E-link number (hex) or use -G/g/p options.\n"
    "  -G <lnk>   : GBT-link number.\n"
    "  -g <group> : Group number (default: all groups).\n"
    "  -p <path>  : E-path number (default: all paths).\n"
    "Examples:\n"
    "> fela -G1        (Show alignment of E-links of link 1)\n"
    "> fela -G1 -g2    (Show alignment of E-group 2 E-links of link 1)\n"
    "> fela            (Show alignment of E-links of all links "
    "(in a single hex word)\n";
}

// ----------------------------------------------------------------------------
