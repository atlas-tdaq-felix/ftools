#include <iostream>
#include <iomanip>
using namespace std;
#include <unistd.h>

#include "felixtag.h"
#include "flxdefs.h"
#include "flxcard/FlxCard.h"
#include "flxcard/FlxException.h"
#include "arg.h"

// Version identifier: year, month, day, release number
const int   VERSION_ID = 0x23052300; // Various fixes for EC/IC
//const int VERSION_ID = 0x23030300; // Settings are per device, not all via #0
//const int VERSION_ID = 0x23022400;

// ----------------------------------------------------------------------------

int main( int argc, char *argv[] )
{
  int  opt;
  int  devnr     = 0;
  int  lnknr     = -1;
  int  egroupnr  = -1;
  int  epathnr   = -1;
  int  elinknr   = -1;
  int  set       = -1;
  string operation;

  // Parse the options
  while( (opt = getopt(argc, argv, "d:e:g:G:hp:V")) != -1 )
    {
      switch( opt )
        {
        case 'd':
          if( sscanf( optarg, "%d", &devnr ) != 1 )
            arg_error( 'd' );
          break;
        case 'e':
          // E-link number
          unsigned int x;
          if( sscanf( optarg, "%x", &x ) != 1 )
            arg_error( 'e' );
          elinknr = x;
          if( elinknr < 0 || elinknr > FLX_MAX_ELINK_NR )
            arg_range( 'e', 0, FLX_MAX_ELINK_NR );
          break;
        case 'G':
          // (lp)GBT link number
          if( sscanf( optarg, "%d", &lnknr ) != 1 )
            arg_error( 'G' );
          if( lnknr < 0 || lnknr > FLX_LINKS-1 )
            arg_range( 'G', 0, FLX_LINKS-1 );
          break;
        case 'g':
          // E-group number
          if( sscanf( optarg, "%d", &egroupnr ) != 1 )
            arg_error( 'g' );
          if( egroupnr < 0 || egroupnr >= FLX_TOHOST_GROUPS-1 )
            arg_range( 'g', 0, FLX_TOHOST_GROUPS-1-1 );
          //if( egroupnr < 0 || egroupnr >= FLX_FROMHOST_GROUPS-1 )
          //  arg_range( 'g', 0, FLX_FROMHOST_GROUPS-1-1 );
          break;
        case 'h':
          usage();
          return 0;
        case 'p':
          // E-path number
          if( sscanf( optarg, "%d", &epathnr ) != 1 )
            arg_error( 'p' );
          if( epathnr < 0 || epathnr > FLX_ELINK_PATHS-1 )
            arg_range( 'p', 0, FLX_ELINK_PATHS-1 );
          break;
        case 'V':
          cout << "Version " << hex << VERSION_ID << dec
               << " (tag: " << FELIX_TAG << ")" << endl;
          return 0;
        default: // '?'
          return 1;
        }
    }

  // Check for either a valid -e or valid -G/g/p options
  if( (elinknr != -1 && (lnknr != -1 || egroupnr != -1 || epathnr != -1)) )
    {
      cout << "### Provide either -e or -G/g/p options" << endl;
      return 1;
    }

  // String defining the operation to perform?
  if( optind < argc )
    {
      operation = string( argv[optind] );
      if( operation == string("set") )
        set = 1;
      else if( operation == string("reset") )
        set = 0;
      else
        {
          cout << "### Unknown qualifier (use: set|reset): "
               << operation << endl;
          usage();
          return 1;
        }

      ++optind;
      if( optind < argc )
        {
          cout << "### Warning: unexpected parameter \""
               << argv[optind] << "\" (ignored)" << endl;
        }
    }

#if REGMAP_VERSION < 0x500
  cout << "### febrc tool is for RM5 firmware only" << endl;
  set = set;
  return 1;

#else

  // Open FELIX FLX-device
  FlxCard *flx = new FlxCard;
  try {
    flx->card_open( devnr, LOCK_NONE );
  }
  catch( FlxException &ex ) {
    cout << "### FlxCard open: " << ex.what() << endl;
    return 1;
  }
  cout << "FLX-device " << devnr
       << ", firmw " << flx->firmware_string() << endl;

  bool lpgbt = flx->lpgbt_type();
  flxcard_bar2_regs_t *bar2 = (flxcard_bar2_regs_t *) flx->bar2Address();
  uint64_t chans = bar2->NUM_OF_CHANNELS;
  if( chans > FLX_LINKS ) chans = FLX_LINKS;
  uint64_t fw_mode = bar2->FIRMWARE_MODE;

  // Single E-link number given ?
  if( elinknr != -1 )
    {
      // Extract link, e-group and e-path numbers from the given e-link number
      if( lpgbt )
        {
          lnknr    = (elinknr & BLOCK_LNK_MASK) >> BLOCK_LNK_SHIFT;
          egroupnr = ((elinknr & BLOCK_EGROUP_MASK_LPGBT) >>
                      BLOCK_EGROUP_SHIFT_LPGBT);
          epathnr  = (elinknr & BLOCK_EPATH_MASK_LPGBT);
        }
      else
        {
          lnknr    = (elinknr & BLOCK_LNK_MASK) >> BLOCK_LNK_SHIFT;
          egroupnr = (elinknr & BLOCK_EGROUP_MASK) >> BLOCK_EGROUP_SHIFT;
          epathnr  = (elinknr & BLOCK_EPATH_MASK);
        }
    }

  // Settings through device #0 only
  uint64_t nstreams  = bar2->AXI_STREAMS_FROMHOST.NUMBER_OF_STREAMS;
  bool config_ec = false, config_ic = false;

  int max_lnk_cnt;
  if( lnknr == -1 )
    {
      lnknr = 0;
      // Covering *all* links
      max_lnk_cnt = chans;
    }
  else if( lnknr >= 0 && lnknr < (int) chans )
    {
      max_lnk_cnt = lnknr + 1;
    }
  else
    {
      cout << "### Invalid Link number " << lnknr
           << " (valid=[0.." << chans-1
           << "]) for this FLX-device" << endl;
      flx->card_close();
      return 1;
    }

  int max_egroup_cnt;
  if( egroupnr == -1 )
    {
      egroupnr = 0;
      if( lpgbt )
        max_egroup_cnt = 4;
      else
        max_egroup_cnt = 5;

      if( epathnr == -1 )
        {
          // Do complete link, so include EC and IC
          config_ec = true;
          config_ic = true;
        }
    }
  else
    {
      if( lpgbt && egroupnr < 7 )
        {
          max_egroup_cnt = egroupnr + 1;
        }
      else if( !lpgbt && egroupnr < 5 )
        {
          max_egroup_cnt = egroupnr + 1;
        }
      else
        {
          cout << "### Invalid Egroup number " << egroupnr
               << " (valid=[0.." << (lpgbt ? 6 : 4)
               << "]) for this FLX-device" << endl;
          flx->card_close();
          return 1;
        }
    }

  int max_epath_cnt;
  if( epathnr == -1 )
    {
      epathnr = 0;
      if( lpgbt )
        {
          if( fw_mode == FIRMW_STRIP )
            max_epath_cnt = 5;
          else
            max_epath_cnt = 4;
        }
      else
        {
          max_epath_cnt = 8;
        }
    }
  else
    {
      if( lpgbt && fw_mode == FIRMW_STRIP && epathnr < 5 )
        {
          max_epath_cnt = epathnr + 1;
        }
      else if( lpgbt && epathnr < 4 )
        {
          max_epath_cnt = epathnr + 1;
        }
      else if( !lpgbt && epathnr < 8 )
        {
          max_epath_cnt = epathnr + 1;
        }
      else
        {
          cout << "### Invalid Epath number " << epathnr
               << " (valid=[0.." << (lpgbt ? 3 : 7)
               << "]) for this FLX-device" << endl;
          flx->card_close();
          return 1;
        }
    }

  cout << setfill( '0' ) << uppercase;
  uint32_t index;
  uint64_t enables = 0;
  cout << "Streams-per-link: " << nstreams << " (FromHost)" << endl;
  for( int lnk=lnknr; lnk<max_lnk_cnt; ++lnk )
    {
      // Broadcast-enable is a FromHost setting (on both devices of a card)
      enables = bar2->BROADCAST_ENABLE_GEN[lnk].BROADCAST_ENABLE;

      for( int grp=egroupnr; grp<max_egroup_cnt; ++grp )
        {
          // Stream start index
          if( lpgbt )
            index = grp * 4 + epathnr;
          else
            index = grp * 8 + epathnr;

          for( int path=epathnr; path<max_epath_cnt; ++path, ++index )
            {
              if( set == 1 )
                enables |= ((uint64_t) 1 << index);
              else if( set == 0 )
                enables &= ~((uint64_t) 1 << index);
            }
        }

      // EC and IC
      if( config_ec )
        {
          index = bar2->AXI_STREAMS_FROMHOST.EC_INDEX;
          if( set == 1 )
            enables |= ((uint64_t) 1 << index);
          else if( set == 0 )
            enables &= ~((uint64_t) 1 << index);
        }
      if( config_ic )
        {
          index = bar2->AXI_STREAMS_FROMHOST.IC_INDEX;
          if( set == 1 )
            enables |= ((uint64_t) 1 << index);
          else if( set == 0 )
            enables &= ~((uint64_t) 1 << index);
        }

      if( set == 1 || set == 0 )
        bar2->BROADCAST_ENABLE_GEN[lnk].BROADCAST_ENABLE = enables;

      cout << "Dev #" << devnr
           << " Link " << setw(2) << lnk-chans*(lnk/chans);
      cout << ": BROADCAST_ENA="
           << hex << setw((nstreams+3)/4) << enables << dec << endl;

      // Handling more than 1 link
      if( max_lnk_cnt-lnknr != 1 )
        continue;

      // List the setting per E-link when displaying (part of) a single link
      cout << "E-link G-g-p BroadCast" << endl << setfill('0');
      for( int grp=egroupnr; grp<max_egroup_cnt; ++grp )
        {
          // Stream start index
          if( lpgbt )
            index = grp * 4 + epathnr;
          else
            index = grp * 8 + epathnr;

          for( int path=epathnr; path<max_epath_cnt; ++path, ++index )
            {
              if( lpgbt )
                elinknr = ((lnk  << BLOCK_LNK_SHIFT) |
                           (grp  << BLOCK_EGROUP_SHIFT_LPGBT) | path);
              else
                elinknr = ((lnk  << BLOCK_LNK_SHIFT) |
                           (grp  << BLOCK_EGROUP_SHIFT) | path);
              uint64_t brc = (enables & ((uint64_t)1 << index)) >> index;
              cout << "0x" << setfill('0') << setw(3) << hex << elinknr
                   << " " << dec << setfill(' ') << setw(2) << lnk << "-"
                   << grp << "-" << path << "  " << (brc != 0 ? '1' : '-')
                   << endl;
            }
        }
      // The rest... (only when displaying the complete link)
      if( max_egroup_cnt-egroupnr > 1 && max_epath_cnt-epathnr > 1 )
        {
          uint32_t ec_index = bar2->AXI_STREAMS_FROMHOST.EC_INDEX;
          uint32_t ic_index = bar2->AXI_STREAMS_FROMHOST.IC_INDEX;
          for( ; index<nstreams; ++index )
            {
              elinknr = ((lnk << BLOCK_LNK_SHIFT) | index);
              uint64_t brc = (enables & ((uint64_t)1 << index)) >> index;
              cout << "0x" << setfill('0') << setw(3) << hex << elinknr;
              if( index == ec_index )
                cout << "  EC     ";
              else if( index == ic_index )
                cout << "  IC     ";
              else
                cout << "         ";
              cout << (brc != 0 ? '1' : '-') << endl;
            }
        }
    }

  // Close the FLX-device
  try {
    flx->card_close();
  }
  catch( FlxException &ex ) {
    cout << "FlxCard close: " << ex.what() << endl;
  }
#endif // REGMAP_VERSION

  return 0;
}

// ----------------------------------------------------------------------------

void usage()
{
  cout << "febrc version " << hex << VERSION_ID << dec << endl <<
    "NOTE: for RM5 firmware only.\n"
    "Enable, disable or display the broadcast enable setting,\n"
    "a setting per e-path (e-link)\n"
    "Without keyword '(re)set' the current setting of the requested\n"
    "(group of) broadcast enables is displayed (one complete link at most).\n"
    "\n"
    "Usage: febrc [-h|V] [-d <devnr>] [-e <elink>] "
    "[-G <lnk> [-g <group>] [-p <path>]]\n"
    "            [set|reset]\n"
    "  -h         : Show this help text.\n"
    "  -V         : Show version.\n"
    "  -d <devnr> : FLX-device to use (default: 0).\n"
    "  -e <elink> : E-link number (hex) or use -G/g/p options.\n"
    "  -G <lnk>   : (lp)GBT-link number.\n"
    "  -g <group> : Group number (default: all groups).\n"
    "  -p <path>  : E-path number (default: all paths).\n"
    "  set        : Enable time-out.\n"
    "  reset      : Disable time-out.\n"
    "Examples:\n"
    "> febrc -G4 set    (Enable broadcast on all E-links of link 4;\n"
    "                    broadcast e.g. on link 4 using E-link 0x13F)\n"
    "> febrc -g1 set    (Enable broadcast on E-links of E-group 1 of all links;\n"
    "                    broadcast e.g. on all links using E-link 0x7ff)\n"
    "> febrc -e42 set   (Enable broadcast on E-link 0x042 (on Link 1, E-group 0, path 2);"
    "                    broadcast e.g. on all Links E-group 0, path 2 using E-link 0x7c2)\n"
    "> febrc            (Show broadcast enable registers content)\n";
}

// ----------------------------------------------------------------------------
