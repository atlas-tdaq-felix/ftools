#include <iostream>
#include <iomanip>
using namespace std;
#include <unistd.h>

#include "felixtag.h"
#include "FlxUpload.h"
#include "FlxReceiver.h"
#include "FlxParser.h"
#include "arg.h"

// Version identifier: year, month, day, release number
const int   VERSION_ID = 0x23060100; // Support for HDLC 'delay packet'
//const int VERSION_ID = 0x22040800; // Add options -r/-t (pulses generation)
//const int VERSION_ID = 0x21011800;

uint32_t chunk2val( uint8_t *chunk );
void val2bytes( uint32_t val, uint8_t *bytes );

// ----------------------------------------------------------------------------

int main( int argc, char *argv[] )
{
  int   opt;
  int   cardnr       = 0;
  int   gbtnr        = -1;
  int   egroupnr     = 7; // EC-link
  int   epathnr      = 7; // EC-link
  int   elinknr      = -1;
  int   dma_write    = -1; // Autoselect FromHost DMA controller index
  int   dma_read     = 0;
  bool  receive      = true;
  bool  sca_connect  = false;
  bool  sca_reset    = false;
  bool  gpio_enable  = true;
  bool  gpio_disable = false;
  int   gpio_bit     = -1;
  int64_t gpio_val   = -1;
  int64_t gpio_setdir= -1;
  int   pulses       = 0;
  int   width_us     = 1;
  int   width_offs   = 2; // Approximate delay overhead in addScaFrame()
  bool  hdlc_delay   = true;

  // Parse the options
  unsigned int x;
  while( (opt = getopt(argc, argv, "Cd:DEe:G:g:hHi:o:p:Rr:t:VZ")) != -1 )
    {
      switch( opt )
        {
        case 'C':
          sca_connect = true;
          break;
        case 'd':
          if( sscanf( optarg, "%d", &cardnr ) != 1 )
            arg_error( 'd' );
          break;
        case 'D':
          gpio_disable = true;
          break;
        case 'E':
          gpio_enable = false;
          break;
        case 'e':
          // E-link number
          if( sscanf( optarg, "%x", &x ) != 1 )
            arg_error( 'e' );
          elinknr = x;
          if( elinknr < 0 || elinknr > FLX_MAX_ELINK_NR )
            arg_range( 'e', 0, FLX_MAX_ELINK_NR );
          break;
        case 'G':
          // GBT link number to upload to
          if( sscanf( optarg, "%d", &gbtnr ) != 1 )
            arg_error( 'G' );
          if( gbtnr < 0 || gbtnr > FLX_LINKS-1 )
            arg_range( 'G', 0, FLX_LINKS-1 );
          break;
        case 'g':
          // E-group number to upload to
          // (NB: Egroup 7 (or 5) + Epath 7 is EC link)
          if( sscanf( optarg, "%d", &egroupnr ) != 1 )
            arg_error( 'g' );
          if( egroupnr < 0 || (egroupnr > FLX_FROMHOST_GROUPS-1 &&
                               egroupnr != 5 && egroupnr != 7) )
            arg_range( 'g', 0, FLX_FROMHOST_GROUPS-1 );
          break;
        case 'h':
          usage();
          return 0;
        case 'H':
          hdlc_delay = false;
          break;
        case 'i':
          // GPIO bit index
          if( sscanf( optarg, "%d", &gpio_bit ) != 1 )
            arg_error( 'i' );
          if( gpio_bit < 0 || gpio_bit > 31 )
            arg_range( 'i', 0, 31 );
          break;
        case 'o':
          // GPIO DIR register value
          if( sscanf( optarg, "%x", &x ) != 1 )
            arg_error( 'o' );
          gpio_setdir = x;
          if( gpio_setdir < 0 || gpio_setdir > 0xFFFFFFFF )
            arg_range( 'o', 0, 0xFFFFFFFF );
          break;
        case 'p':
          // E-path number to upload to
          if( sscanf( optarg, "%d", &epathnr ) != 1 )
            arg_error( 'p' );
          if( epathnr < 0 || epathnr > FLX_ELINK_PATHS-1 )
            arg_range( 'p', 0, FLX_ELINK_PATHS-1 );
          break;
        case 'R':
          sca_reset = true;
          break;
        case 'r':
          // 'repeat' factor (number of pulses)
          // (generate a sequence of output pulses; for test purposes)
          // (when setting a single GPIO output only)
          if( sscanf( optarg, "%d", &pulses ) != 1 )
            arg_error( 'r' );
          if( pulses < 1 || pulses > 65535 )
            arg_range( 'r', 1, 65535 );
          break;
        case 't':
          // Delay in microseconds between repeated GPIO pulses (see option -r)
          // (when setting a single GPIO output only)
          if( sscanf( optarg, "%d", &width_us ) != 1 )
            arg_error( 't' );
          if( width_us < 2 || width_us > 1000 )
            arg_range( 't', 2, 1000 );
          break;
        case 'V':
          cout << "Version " << hex << VERSION_ID << dec
               << " (tag: " << FELIX_TAG << ")" << endl;
          return 0;
        case 'Z':
          receive = false;
          break;
        default: // '?'
          return 1;
        }
    }

  // Check for either a valid -e or valid -G/g/p options
  if( (elinknr != -1 && !(gbtnr == -1 || egroupnr == -1 || epathnr == -1)) ||
      (elinknr == -1 && !(gbtnr != -1 && egroupnr != -1 && epathnr != -1)) )
    {
      cout << "### Provide either -e or -G/g/p options" << endl;
      return 1;
    }
  if( elinknr != -1 )
    {
      // Derive GBT, e-group and e-path numbers from the given e-link number
      gbtnr    = (elinknr & BLOCK_LNK_MASK) >> BLOCK_LNK_SHIFT;
      egroupnr = (elinknr & BLOCK_EGROUP_MASK) >> BLOCK_EGROUP_SHIFT;
      epathnr  = (elinknr & BLOCK_EPATH_MASK);
    }
  else
    {
      // Derive e-link number from the given GBT, group and epath numbers
      elinknr = ((gbtnr    << BLOCK_LNK_SHIFT) |
                 (egroupnr << BLOCK_EGROUP_SHIFT) | epathnr);
   }

  // GPIO value to write
  if( optind < argc )
    {
      // Expect a value to write
      if( sscanf( argv[optind], "%x", &x ) == 1 )
        {
          gpio_val = x;
          if( gpio_bit != -1 )
            {
              // Single bit value
              if( gpio_val != 0 && gpio_val != 1 )
                {
                  cout << "### GPIO value out-of-range 0..1: "
                       << gpio_val << endl;
                }
            }
          else
            {
              // 32-bit value
              if( gpio_val < 0 || gpio_val > 0xFFFFFFFF )
                {
                  cout << "### GPIO value out-of-range 0..0xFFFFFFFF: "
                       << gpio_val << endl;
                  return 1;
                }
            }
        }
      else
        {
          cout << "### Invalid GPIO value provided" << endl;
          return 1;
        }
    }

  // -------------------------------------------------------------------------
  // FLX-card sender and receiver

  FlxUpload fup( cardnr, 0, dma_write );
  if( fup.hasError() )
    {
      cout << "### " << fup.errorString() << endl;
      return 1;
    }
  cout << "Opened FLX-device " << cardnr
       << ", firmw " << fup.firmwareVersion() << endl;

  if( fup.fanOutLocked() )
    cout <<  "**NB**: FanOut-Select registers are locked!" << endl;
  fup.setFanOutForDaq();
  fup.setDmaCircular( true );

  // Disable HDLC delay support explicitly? (enable is automatic)
  cout << "HDLC delay support";
  if( !hdlc_delay && fup.hdlcDelaySupport() )
    {
      // Force disabling of HDLC delay packets
      fup.setHdlcDelaySupport( false );
      cout << " (disabled)";
    }
  cout << ": " << (fup.hdlcDelaySupport() ? "YES" : "NO") << endl;

  FlxReceiver *frecv = 0;
  if( receive )
    {
      frecv = new FlxReceiver( cardnr, 64*1024*1024, dma_read );
      if( frecv->hasError() )
        {
          cout << "### " << frecv->errorString() << endl;
          frecv->stop();
          return 1;
        }
      /*
      cout << "Opened FLX-device " << cardnr
           << ", firmw " << frecv->firmwareVersion()
           << ", " << frecv->numberOfChans()
           <<  " chans (cmem bufsize="
           << frecv->bufferSize() << ", receive: "
           << "DMA #" << dma_read << ")" << endl;
      */
    }

  FlxParser fparser;
  uint8_t  *chunkdata = 0;
  uint32_t  size;
  fparser.setReceiver( frecv );

  // -------------------------------------------------------------------------
  // Initialize GBT-SCA

  if( sca_connect )
    {
      if( fup.scaConnect( elinknr ) )
        cout << "GBT-SCA connect" << endl;
      else
        cout << "###GBT-SCA connect: " << fup.errorString() << endl;
    }

  if( sca_reset )
    {
      if( fup.scaReset( elinknr ) )
        cout << "GBT-SCA reset" << endl;
      else
        cout << "###GBT-SCA reset: " << fup.errorString() << endl;
    }

  // -------------------------------------------------------------------------
  // Enable GPIO channel

  uint8_t data[4], dev_mask = 0;
  int     trid = 1;

  if( gpio_enable || gpio_disable )
    {
      // Read twice... first one possibly results in SREJ reply...
      int recv_trid = 254;
      fup.uploadScaFrame( elinknr, &recv_trid, SCA_DEV_CONFIG, 0,
                          SCA_CONFIG_RD_B, data );
      recv_trid = 254;
      fup.uploadScaFrame( elinknr, &recv_trid, SCA_DEV_CONFIG, 0,
                          SCA_CONFIG_RD_B, data );

      // Receive reply
      while( fparser.nextChunkRecvd( &chunkdata, &size, 5000, elinknr ) )
        {
          if( size == 0 ) continue;
          if( size <= 12 && chunkdata[2] == 0xFE ) // Transaction ID 254
            dev_mask = chunkdata[7];
        }
      //cout << "ENA=" << hex << (uint32_t) dev_mask << dec << endl;
    }

  if( gpio_enable )
    {
      // Enable GPIO
      data[0] = 0x00;
      data[1] = 1 << SCA_DEV_GPIO;
      fup.uploadScaFrame( elinknr, &trid, SCA_DEV_CONFIG, 2,
                          SCA_CONFIG_WR_B, data );
      cout << "GPIO enabled" << endl;
    }

  // -------------------------------------------------------------------------
  // Read/write GPIO direction

  // Write DIR register
  if( gpio_setdir != -1 )
    {
      uint32_t tmp = (uint64_t) (gpio_setdir & 0xFFFFFFFF);
      val2bytes( tmp, data );
      fup.uploadScaFrame( elinknr, &trid, SCA_DEV_GPIO, 4,
                          SCA_GPIO_WR_DIR, data );
    }

  // Read DIR register
  fup.uploadScaFrame( elinknr, &trid, SCA_DEV_GPIO, 4,
                      SCA_GPIO_RD_DIR, data );
  // Receive reply
  int64_t gpio_dir = -1;
  while( fparser.nextChunkRecvd( &chunkdata, &size, 5000, elinknr ) )
    {
      if( size == 12 && chunkdata[2] == trid-1 )
        gpio_dir = (int64_t) chunk2val( chunkdata );
    }
  if( gpio_dir == -1 )
    {
      cout << "### Failed to receive GBT-SCA reply (read DIR)" << endl;
      if( frecv )
        return 1;
    }

  if( gpio_bit != -1 && gpio_val != -1 )
    {
      // Set the *selected* GPIO bit to output, if not already
      if( (gpio_dir & (1 << gpio_bit)) == 0 )
        {
          // Write DIR
          gpio_dir |= (1 << gpio_bit);
          uint32_t tmp = (uint64_t) (gpio_dir & 0xFFFFFFFF);
          val2bytes( tmp, data );
          fup.uploadScaFrame( elinknr, &trid, SCA_DEV_GPIO, 4,
                              SCA_GPIO_WR_DIR, data );
        }
    }

  cout << "GPIO dir = 0x" << hex << setfill('0')
       << setw(8) << gpio_dir << endl;

  // -------------------------------------------------------------------------
  // Read current DATAOUT

  fup.uploadScaFrame( elinknr, &trid, SCA_DEV_GPIO, 4,
                      SCA_GPIO_RD_OUT, data );
  // Receive reply
  int64_t gpio_out = -1;
  while( fparser.nextChunkRecvd( &chunkdata, &size, 5000, elinknr ) )
    {
      if( size == 12 && chunkdata[2] == trid-1 )
        gpio_out = (int64_t) chunk2val( chunkdata );
    }
  if( gpio_out == -1 )
    {
      cout << "### Failed to receive GBT-SCA reply (read OUT)" << endl;
      if( frecv )
        return 1;
    }

  // -------------------------------------------------------------------------
  // Write selected GPIO output or all

  if( gpio_val != -1 )
    {
      if( pulses > 0 && gpio_bit != -1 )
        {
          // Special (test/demo):
          // toggle I/O up/down or down/up 'pulses' times
          // with configurable width

          // Write DATAOUT
          uint32_t out = gpio_out ^ (1 << gpio_bit);
          val2bytes( out, data );

          fup.resetScaFrames();
          for( int r=0; r<pulses; ++r )
            {
              // It takes ca. 1 us to send a command so
              // this output pulse has a minimum width of 1 us
              fup.addScaFrame( &trid, SCA_DEV_GPIO, 4, SCA_GPIO_WR_OUT, data );
              if( width_us > width_offs )// offset implicit in addScaFrame()
                fup.addScaDelayUs( width_us-width_offs );

              out ^= (1 << gpio_bit);
              val2bytes( out, data );

              fup.addScaFrame( &trid, SCA_DEV_GPIO, 4, SCA_GPIO_WR_OUT, data );
              if( width_us > width_offs ) // offset implicit in addScaFrame()
                fup.addScaDelayUs( width_us-width_offs );

              out ^= (1 << gpio_bit);
              val2bytes( out, data );
            }
          fup.uploadScaFrames( elinknr );
        }

      if( gpio_bit != -1 )
        {
          // Single bit
          if( gpio_val == 1 )
            gpio_out |= (1 << gpio_bit);
          else
            gpio_out &= ~(1 << gpio_bit);
        }
      else
        {
          // 32 bits
          gpio_out = gpio_val;
        }
      uint32_t tmp = (uint64_t) (gpio_out & 0xFFFFFFFF);
      val2bytes( tmp, data );
      fup.uploadScaFrame( elinknr, &trid, SCA_DEV_GPIO, 4,
                          SCA_GPIO_WR_OUT, data );

      // Re-read DATAOUT
      fup.uploadScaFrame( elinknr, &trid, SCA_DEV_GPIO, 4,
                          SCA_GPIO_RD_OUT, data );
      // Receive replies
      int timeout = 5000;
      if( pulses > 0 )
        timeout = 50000;
      gpio_out = -1;
      while( fparser.nextChunkRecvd( &chunkdata, &size, timeout, elinknr ) )
        {
          if( size == 12 && chunkdata[2] == trid-1 )
            gpio_out = (int64_t) chunk2val( chunkdata );
        }
      if( gpio_out == -1 )
        {
          cout << "### Failed to receive GBT-SCA reply (read OUT)" << endl;
          if( frecv )
            return 1;
        }
    }

  // -------------------------------------------------------------------------
  // Read current DATAIN

  fup.uploadScaFrame( elinknr, &trid, SCA_DEV_GPIO, 4,
                      SCA_GPIO_RD_IN, data );
  // Receive reply
  int64_t gpio_in = -1;
  while( fparser.nextChunkRecvd( &chunkdata, &size, 5000, elinknr ) )
    {
      if( size == 12 && chunkdata[2] == trid-1 )
        gpio_in = (int64_t) chunk2val( chunkdata );
    }
  if( gpio_in == -1 )
    {
      cout << "### Failed to receive GBT-SCA reply (read IN)" << endl;
      if( frecv )
        return 1;
    }

  // -------------------------------------------------------------------------
  // Display

  cout << hex << setfill('0')
       << "GPIO out = 0x" << setw(8) << gpio_out
       << " in = 0x" << setw(8) << gpio_in << endl;

  if( gpio_bit != -1 )
    {
      cout << "Bit " << dec << gpio_bit << ": ";
      if( (gpio_dir & (1 << gpio_bit)) != 0 )
        {
          // It's an output
          cout << ((gpio_out & (1 << gpio_bit)) ? "1" : "0");
          cout << " (OUT)";
        }
      else
        {
          // It's an input
          cout << ((gpio_in & (1 << gpio_bit)) ? "1" : "0");
          cout << " (IN)";
        }
      cout << endl;
    }

  // -------------------------------------------------------------------------
  // Disable

  if( gpio_disable )
    {
      data[0] = 0x00;
      data[1] = dev_mask;
      fup.uploadScaFrame( elinknr, &trid, SCA_DEV_CONFIG, 2,
                          SCA_CONFIG_WR_B, data );
      cout << "GPIO disabled" << endl;
    }

  // -------------------------------------------------------------------------
  // Finish up

  fup.stop();
  if( frecv )
    frecv->stop();
  cout << endl;
  return 0;
}

// ----------------------------------------------------------------------------

void usage()
{
  cout << "fscaio version " << hex << VERSION_ID << dec << endl <<
    "Tool to write and/or read the GBT-SCA GPIO bits and direction register.\n"
    "Usage:\n"
    " fscaio [-h|V] [-d <devnr>] [-e <elink>] [-G <lnk>] [-g <group>] "
    "[-p <path>]\n"
    "        [-i <bit>] [-o <dir>] [-C] [-R] [-D] [-E] [-Z] [-H] [<value>]\n"
    "  -h         : Show this help text.\n"
    "  -V         : Show version.\n"
    "  -d <devnr> : FLX-device to use (default: 0).\n"
    "  -e <elink> : E-link number (hex) or use -G/g/p options.\n"
    "  -G <lnk>   : GBT-link number (default: 0).\n"
    "  -g <group> : Group number (default matches GBT EC 'group' = 7).\n"
    "  -p <path>  : E-path number (default matches GBT EC 'path' = 7).\n"
    "  -i <bit>   : Read or write GPIO bit number <bit> (default: all).\n"
    "               NB: if a single I/O pin is written to, its direction bit\n"
    "                   is set to output (independent of option -o).\n"
    "  -o <dir>   : Set GPIO direction register to value <dir> (hex).\n"
    "  -C         : Send GBT-SCA connect (HDLC control).\n"
    "  -R         : Send GBT-SCA reset (HDLC control).\n"
    "  -r <rep>   : Generate <rep> pulses up/down or down/up before setting the (single) output\n"
    "               to the requested value (for debug/demo purposes).\n"
    "  -t <us>    : Width of the -r pulses, in microseconds [2..1000] (default: 2).\n"
    "  -Z         : Do not receive and display the GBT-SCA replies.\n"
    "  -D         : Disable GBT-SCA GPIO channel after operation "
    "(default: leave enabled)\n"
    "  -E         : Do *not* enable GBT-SCA GPIO channel at start of operation,\n"
    "               assume it already is.\n"
    "  -H         : Do not use HDLC delay packet, even if firmware supports it,\n"
    "               use zero-byte packets (for output pulse width, options -r and -t).\n"
    " <value>     : Value to write (0 or 1 for a single GPIO bit,\n"
    "               or up to 0xFFFFFFFF otherwise, hexadecimal);\n"
    "               if no value is provided a read operation is performed.\n";
}

// ----------------------------------------------------------------------------

uint32_t chunk2val( uint8_t *chunk )
{
  uint32_t val = ((((uint32_t) chunk[8]) << 0) |
                  (((uint32_t) chunk[9]) << 8) |
                  (((uint32_t) chunk[6]) << 16) |
                  (((uint32_t) chunk[7]) << 24));
  return val;
}

// ----------------------------------------------------------------------------

void val2bytes( uint32_t val, uint8_t *bytes )
{
  bytes[0] = (uint8_t) ((val & 0x00FF0000) >> 16);
  bytes[1] = (uint8_t) ((val & 0xFF000000) >> 24);
  bytes[2] = (uint8_t) ((val & 0x000000FF) >>  0);
  bytes[3] = (uint8_t) ((val & 0x0000FF00) >>  8);
}

// ----------------------------------------------------------------------------
