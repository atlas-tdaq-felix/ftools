#include <iostream>
#include <iomanip>
using namespace std;
#include <unistd.h>

#include "felixtag.h"
#include "flxcard/FlxCard.h"
#include "flxcard/FlxException.h"
#include "FlxConfig.h"
#if REGMAP_VERSION < 0x500
#include "GbtConfig.h"
#else
#include "GbtConfig5.h"
#endif // REGMAP_VERSION
#include "arg.h"

void listGbtElinks( int linknr, int egroupnr, int epathnr );
void listLpgbtElinks( int linknr, int egroupnr, int epathnr );

// Version identifier: year, month, day, release number
const int   VERSION_ID = 0x25011700; // Optionally list enabled e-links
//const int VERSION_ID = 0x23052400; // Indicate RM5 FromHost broadcast e-links
//const int VERSION_ID = 0x23011600; // RM5 (lp)GBT e-link numbers: -E and -L;
                                     // FlxConfig class; remove readLinkConfig()
//const int VERSION_ID = 0x23010400; // Remove -I and -w options ('bit index')
#if REGMAP_VERSION >= 0x500
//const int VERSION_ID = 0x21021100; // GbtConfig5.h
#else
//const int VERSION_ID = 0x19101400; // Allow E-link nr with offset of n*2048
#endif // REGMAP_VERSION
//const int VERSION_ID = 0x18080700; // Added IC-link
//const int VERSION_ID = 0x16092800;

// ----------------------------------------------------------------------------

int main( int argc, char *argv[] )
{
  int  cardnr    = -1;
  int  elinknr   = -1;
  int  devnr     = -1;
  int  linknr    = -1;
  int  egroupnr  = -1;
  int  epathnr   = -1;
  int  dma_index = -1;
  bool listGbt   = false;
  bool listLpgbt = false;
  bool isLpgbt   = false;

  // Parse the options
  int  opt;
  while( (opt = getopt(argc, argv, "d:e:E:g:G:hi:lLp:V")) != -1 )
    {
      switch( opt )
        {
        case 'd':
          if( sscanf( optarg, "%d", &cardnr ) != 1 )
            arg_error( 'd' );
          if( elinknr != -1 )
            {
              if( elinknr < 0 || elinknr > FLX_MAX_ELINK_NR )
                arg_range( 'e', 0, FLX_MAX_ELINK_NR );
              break;
            }
          break;
        case 'e':
        case 'E':
          {
            // E-link number (may have an offset of a multiple of
            // FLX_MAX_ELINK_NR, to indicate the FLX-device number
            // the E-link is assigned to, unless a device number,
            // i.e. 'cardnr', is provided using option -d)
            unsigned int x;
            if( sscanf( optarg, "%x", &x ) != 1 )
              arg_error( 'e' );
            elinknr = x;
            int elinknr_max;
            if( cardnr != -1 )
              elinknr_max = FLX_MAX_ELINK_NR;
            else
              // ### Don't allow 'multi-device' elink numbers (6 Mar 2023)
              //elinknr_max = FLX_MAX_ELINK_NR*4; // Let's allow up to 4 devs
              elinknr_max = FLX_MAX_ELINK_NR;
            if( elinknr < 0 || elinknr > elinknr_max )
              arg_range( 'e', 0, elinknr_max );
            // The 'device number' as part of the E-link number given
            //devnr = elinknr / FLX_MAX_ELINK_NR;
            if( opt == 'E' )
              isLpgbt = true;
            break;
          }
        case 'G':
          // (lp)GBT link number
          if( sscanf( optarg, "%d", &linknr ) != 1 )
            arg_error( 'G' );
#if REGMAP_VERSION < 0x500
          if( linknr < 0 || linknr > FLX_LINKS-1 )
#else
          if( (linknr < 0 || linknr > FLX_LINKS-1) &&
              linknr != 31 ) // RM5: Broadcast
#endif // REGMAP_VERSION
            arg_range( 'G', 0, FLX_LINKS-1 );
          break;
        case 'g':
          // E-group number
          if( sscanf( optarg, "%d", &egroupnr ) != 1 )
            arg_error( 'g' );
          if( egroupnr < 0 || egroupnr >= FLX_TOHOST_GROUPS )
            arg_range( 'g', 0, FLX_TOHOST_GROUPS-1 );
          break;
        case 'h':
          usage();
          return 0;
        case 'i':
          // DMA controller to use
          if( sscanf( optarg, "%d", &dma_index ) != 1 )
            arg_error( 'i' );
          if( dma_index < 0 || dma_index > 7 )
            arg_range( 'i', 0, 7 );
          break;
        case 'l':
          listGbt = true;
          break;
        case 'L':
          listLpgbt = true;
          break;
        case 'p':
          // E-path number
          if( sscanf( optarg, "%d", &epathnr ) != 1 )
            arg_error( 'p' );
          if( epathnr < 0 || epathnr > FLX_ELINK_PATHS-1 )
            arg_range( 'p', 0, FLX_ELINK_PATHS-1 );
          break;
        case 'V':
          cout << "Version " << hex << VERSION_ID << dec
               << " (tag: " << FELIX_TAG << ")" << endl;
          return 0;
        default: // '?'
          usage();
          return 1;
        }
    }

  if( listGbt || listLpgbt )
    {
      if( listGbt )
        {
          if( elinknr != -1 )
            {
              linknr   = (elinknr & BLOCK_LNK_MASK) >> BLOCK_LNK_SHIFT;
              egroupnr = (elinknr & BLOCK_EGROUP_MASK) >> BLOCK_EGROUP_SHIFT;
              epathnr  = (elinknr & BLOCK_EPATH_MASK);
            }
          listGbtElinks( linknr, egroupnr, epathnr );
        }
      else
        {
          if( elinknr != -1 )
            {
              linknr   = (elinknr & BLOCK_LNK_MASK) >> BLOCK_LNK_SHIFT;
              egroupnr = ((elinknr & BLOCK_EGROUP_MASK_LPGBT) >>
                          BLOCK_EGROUP_SHIFT_LPGBT);
              epathnr  = (elinknr & BLOCK_EPATH_MASK_LPGBT);
            }
          listLpgbtElinks( linknr, egroupnr, epathnr );
        }
      return 0;
    }

  // Do we want to list enabled e-links on a certain device?
  if( cardnr != -1 && elinknr == -1 &&
      linknr == -1 && egroupnr == -1 && epathnr == -1 )
    {
      // Open FELIX FLX-device
      FlxCard *flx = new FlxCard;
      try {
        flx->card_open( cardnr, LOCK_NONE, true );
      }
      catch( FlxException &ex ) {
        cout << "### FlxCard open: " << ex.what() << endl;
        return 1;
      }
      cout << "FLX-device " << cardnr
           << ", " << flx->firmware_string() << endl;

      std::vector<flxcard::elink_descr_t> elinks;

      // Enabled ToHost e-links
      // ----------------------
      elinks = flx->elinks_tohost( dma_index );

      if( dma_index == -1 )
        cout << endl;
      cout << "=> ToHost e-links";
      if( dma_index >= 0 )
        cout << " (DMA index " << dma_index << ")";
      cout << ": " << elinks.size();

      uint32_t dma_count = flx->number_of_dma_tohost();
      cout << " (Total: " << flx->number_of_elinks_tohost()
           << ", DMA 0-" << dma_count-1 << ": ";
      for( int dma_i=0; dma_i<dma_count-1; ++dma_i )
        cout << flx->number_of_elinks_tohost( dma_i ) << ", ";
      cout << flx->number_of_elinks_tohost( dma_count-1 );
      cout << ")" << endl;

      if( elinks.size() == 0 )
        cout << " <none>" << endl;

      // Display the e-link numbers
      cout << hex << setfill('0');
      int count = 0;
      for( flxcard::elink_descr_t &e : elinks )
        {
          cout << " " << setw(3) << e.id;
          ++count;
          if( (count & 0xF) == 0 )
            cout << endl;
        }
      cout << dec;
      if( (count & 0xF) != 0 )
        cout << endl;

      if( dma_index == -1 )
        {
          // Enabled FromHost e-links
          // ------------------------
          elinks = flx->elinks_fromhost();

          cout << "\n=> FromHost e-links: ";
          if( elinks.size() > 0 )
            cout << elinks.size();
          else
            cout << "<none>";

          uint32_t total_elinks = flx->number_of_elinks_fromhost();
          cout << " (Total: " << total_elinks;
          if( total_elinks != elinks.size() )
            cout << ", incl. TTC";
          cout << ")" << endl;

          // Display the e-link numbers
          cout << hex << setfill('0');
          count = 0;
          for( flxcard::elink_descr_t &e : elinks )
            {
              cout << " " << setw(3) << e.id;
              ++count;
              if( (count & 0xF) == 0 )
                cout << endl;
            }
          cout << dec;
          if( (count & 0xF) != 0 )
            cout << endl;
        }

      // Close the FLX-device
      try {
        flx->card_close();
      }
      catch( FlxException &ex ) {
        cout << "FlxCard close: " << ex.what() << endl;
      }
      return 0;
    }

  // Check for either a valid -e option or valid -G/g/p options
  if( (elinknr != -1 && (linknr != -1 || egroupnr != -1 || epathnr != -1)) ||
      (elinknr == -1 && linknr == -1) ||
      (elinknr == -1 && !(egroupnr != -1 && epathnr != -1)) )
    {
      cout << "### Provide either -e or -G/g/p options, "
           << "or -d<n> to list enabled e-links" << endl;
      return 1;
    }

  if( elinknr != -1 )
    {
      // Derive GBT, e-group and e-path numbers from the given e-link number
      // (dependency on GBT or lpGBT!)
      if( isLpgbt )
        {
          linknr   = (elinknr & BLOCK_LNK_MASK) >> BLOCK_LNK_SHIFT;
          egroupnr = (elinknr & BLOCK_EGROUP_MASK_LPGBT) >> BLOCK_EGROUP_SHIFT_LPGBT;
          epathnr  = (elinknr & BLOCK_EPATH_MASK_LPGBT);
        }
      else
        {
          linknr   = (elinknr & BLOCK_LNK_MASK) >> BLOCK_LNK_SHIFT;
          egroupnr = (elinknr & BLOCK_EGROUP_MASK) >> BLOCK_EGROUP_SHIFT;
          epathnr  = (elinknr & BLOCK_EPATH_MASK);
        }
    }

  if( elinknr == -1 )
    {
      if( isLpgbt )
        {
          if( epathnr > 3 )
            {
              cout << "### E-path number not in range for lpGBT [0,3]" << endl;
              return 1;
            }
          elinknr = ((linknr << BLOCK_LNK_SHIFT) |
                     (egroupnr << BLOCK_EGROUP_SHIFT_LPGBT) | epathnr);
        }
      else
        {
          elinknr = ((linknr << BLOCK_LNK_SHIFT) |
                     (egroupnr << BLOCK_EGROUP_SHIFT) | epathnr);
        }
    }

#if REGMAP_VERSION < 0x500
  if( !(egroupnr == 7 && (epathnr == 7 || epathnr == 6 ||   // RM4: EC, IC
                          epathnr == 5 || epathnr == 3)) && // RM4: AUX, TTC2H
#else
  if( !(!isLpgbt &&
        egroupnr == 5 && (epathnr == 0 || epathnr == 1)) && // RM5: GBT EC,IC
      !(isLpgbt &&
        egroupnr == 7 && (epathnr == 0 || epathnr == 1)) && // RM5: lpGBT EC,IC
      elinknr != 0x600 &&   // RM5: TTC2Host
      // RM5: Broadcast
      !(elinknr == 0x7FF ||
       (elinknr & 0x7C0) == 0x7C0 || (elinknr & 0x03F) == 0x03F) &&
#endif // REGMAP_VERSION
      (linknr >= FLX_LINKS || egroupnr >= 7) )
    {
      cout << "### ";
      if( isLpgbt )
        cout << "lp";
      cout << "GBT E-link " << hex << setw(3) << setfill('0') << uppercase
           << elinknr << dec << " is INVALID (";
      if( linknr >= FLX_LINKS )
        cout << "Link=" << linknr << ", max=" << FLX_LINKS-1;
      if( egroupnr > 7 )
        cout << " Egroup=" << egroupnr << ", max=" << 6;
      else if( egroupnr == 7 )
#if REGMAP_VERSION < 0x500
        cout << " Egroup=7: path=7,6,5 or 3 only";
#else
        cout << " Egroup=" << egroupnr << ", max=" << 6;
#endif // REGMAP_VERSION
      cout << ")" << endl;
      return 1;
    }

  if( devnr > 0 )
    cout << "= ";
  if( isLpgbt )
    cout << "lp";
  cout << "GBT E-link " << hex << setw(3) << setfill('0') << uppercase
       << elinknr << dec;
  if( devnr > 0 )
    cout << " (FLX-device #" <<devnr << ")";
  cout << " = Link #" << linknr << " group #" << egroupnr
       << " path #" << epathnr;

  // Skip showing bitnumber and width for lpGBT, broadcast e-links, TTC2Host
#if REGMAP_VERSION >= 0x500
  if( !isLpgbt
      && !(elinknr == 0x600 || // RM5: TTC2Host
           // RM5: Broadcast
           elinknr == 0x7FF ||
           (elinknr & 0x7C0) == 0x7C0 || (elinknr & 0x03F) == 0x03F) )
#endif // REGMAP_VERSION
    switch( epathnr )
      {
      case 0:
        cout << ", bit#" << egroupnr*16 << " width=2|4";
#if REGMAP_VERSION >= 0x500
        cout << "|8|16";
#endif // REGMAP_VERSION
        break;
      case 1:
        cout << ", bit#"  << egroupnr*16+2 << " width=2";
#if REGMAP_VERSION < 0x500
        cout << " OR bit#" << egroupnr*16   << " width=8";
#endif // REGMAP_VERSION
        break;
      case 2:
        cout << ", bit#"  << egroupnr*16+4 << " width=2|4";
        break;
      case 3:
        if( egroupnr == 7 )
          {
            cout << ", TTC-to-Host link";
          }
        else
          {
            cout << ", bit#"  << egroupnr*16+6 << " width=2";
#if REGMAP_VERSION < 0x500
            cout << " OR bit#" << egroupnr*16   << " width=16";
#endif // REGMAP_VERSION
          }
        break;
      case 4:
        cout << ", bit#"  << egroupnr*16+8 << " width=2|4";
#if REGMAP_VERSION >= 0x500
        cout << "|8";
#endif // REGMAP_VERSION
        break;
      case 5:
        if( egroupnr == 7 )
        {
          cout << ", AUX-link width=2";
        }
        else
          {
            cout << ", bit#"  << egroupnr*16+10 << " width=2";
#if REGMAP_VERSION < 0x500
            cout << " OR bit#" << egroupnr*16+ 8 << " width=8";
#endif // REGMAP_VERSION
          }
        break;
      case 6:
        if( egroupnr == 7 )
          cout << ", IC-link width=2";
        else
          cout << ", bit#" << egroupnr*16+12 << " width=2|4";
        break;
      case 7:
        if( egroupnr == 7 )
          cout << ", EC-link width=2";
        else
          cout << ", bit#" << egroupnr*16+14 << " width=2";
        break;
      }
#if REGMAP_VERSION >= 0x500
  // Account for RM5 broadcast elinks
  if( elinknr == 0x7FF ||
      (elinknr & 0x7C0) == 0x7C0 || (elinknr & 0x03F) == 0x03F )
    {
      cout << " (from-host broadcast (RM5))" << endl;
      return 0;
    }
#endif // REGMAP_VERSION
  cout << endl;

  // Check for elink on installed FLX-card ?
  if( cardnr < 0 ) return 0;

  // Check it, using an FlxConfig object
  try {
    // Reads the FLX-device's configuration
    FlxConfig fconf( cardnr );

    // Check if the given elink is configured on the given FLX-device (if any)
    cout << "Checking this e-link on FLX-device #" << cardnr << "..." << endl;

    // If necessary adjust the derived egroup/epath number
    // (only now we know whether we're dealing with GBT or lpGBT)
    if( !isLpgbt && fconf.isLpgbt() )
      {
        egroupnr = ((elinknr & BLOCK_EGROUP_MASK_LPGBT) >>
                    BLOCK_EGROUP_SHIFT_LPGBT);
        epathnr  = (elinknr & BLOCK_EPATH_MASK_LPGBT);
        cout << "NB: firmware is not GBT" << endl;
      }
    else if( isLpgbt && !fconf.isLpgbt() )
      {
        egroupnr = (elinknr & BLOCK_EGROUP_MASK) >> BLOCK_EGROUP_SHIFT;
        epathnr  = (elinknr & BLOCK_EPATH_MASK);
        cout << "NB: firmware is not lpGBT" << endl;
      }

    int bits;
    cout << "From-GBT/To-Host FLX #" << cardnr;
    if( fconf.isElinkEnabled( linknr, egroupnr, epathnr, &bits, false ) )
      cout << ": link enabled, width=" << bits
           << ", mode=" << fconf.elinkMode( linknr, egroupnr, epathnr, false )
           << endl;
    else
      cout << ": ----" << endl;

    cout << "To-GBT/From-Host FLX #" << cardnr;
    if( fconf.isElinkEnabled( linknr, egroupnr, epathnr, &bits, true ) )
      cout << ": link enabled, width=" << bits
           << ", mode=" << fconf.elinkMode( linknr, egroupnr, epathnr, true )
           << endl;
    else
      cout << ": ----" << endl;
  }
  catch( FlxException &ex ) {
    cout << "### " << ex.what() << endl;
  }

  return 0;
}

// ----------------------------------------------------------------------------

void listGbtElinks( int linknr, int egroupnr, int epathnr )
{
  int linkstart  = 0, linkend  = FLX_LINKS;
  int groupstart = 0, groupend = FLX_TOHOST_GROUPS;
  int pathstart  = 0, pathend  = 8;

  cout << "==> GBT E-link list <==" << endl;

  // List elinks from all or a single GBT
  if( linknr >= 0 )
    {
      linkstart = linknr;
      linkend   = linknr + 1;
    }
  // List elinks from all or a single egroup
  if( egroupnr >= 0 )
    {
      groupstart = egroupnr;
      groupend   = egroupnr + 1;
    }
  // List elinks from all or a single epath
  if( epathnr >= 0 )
    {
      pathstart = epathnr;
      pathend   = epathnr + 1;
    }

  //cout << "Link " << linkstart << "-" << linkend << ", "
  //     << "Egroup " << groupstart << "-" << groupend
  //     << "Epath " << pathstart << "-" << pathend << endl;

  cout << setfill('0') << uppercase;

  int lnk, egroup, epath, elinknr;
  egroup = groupstart; // Prevent compiler warning
  for( lnk=linkstart; lnk<linkend; ++lnk )
    {
      for( egroup=groupstart; egroup<groupend; ++egroup )
        {
          cout << "Link-" << lnk
               << " Egroup " << egroup << ":" << endl;
          for( epath=pathstart; epath<pathend; ++epath )
            {
              elinknr = ((lnk << BLOCK_LNK_SHIFT) |
                         (egroup << BLOCK_EGROUP_SHIFT) | epath);
              if( egroup == FLX_TOHOST_GROUPS-1 )
                {
                  if( epath == 7 || epath == 6 || epath == 5 )
                    cout << " " << hex << setw(3) << elinknr << " = "
                         << dec << lnk << "-" << egroup << "-" << epath;

                  if( epath == 7 )
                    cout << " to/from-host: EC (RM4)";
                  else if( epath == 6 )
                    cout << " to/from-host: IC (RM4)";
                  else if( epath == 5 )
                    cout << " to/from-host: AUX(RM4)";
                  //else if( epath == 3 )
                  //  cout << " to-host:TTC" << endl;
                  //else
                  //cout << " ----" << endl;

#if REGMAP_VERSION >= 0x500
                  // Account for RM5 broadcast elinks
                  if( elinknr == 0x7FF ||
                      (elinknr & 0x7C0) == 0x7C0 || (elinknr & 0x03F) == 0x03F )
                    cout << " from-host: broadcast(RM5)";
#endif // REGMAP_VERSION
                  if( epath == 7 || epath == 6 || epath == 5 )
                    cout << endl;
                }
              else if( egroup >= FLX_FROMHOST_GROUPS-1 )
                {
                  cout << " " << hex << setw(3) << elinknr << " = "
                       << dec << lnk << "-" << egroup << "-" << epath;

                  cout << " to-host:wide";

                  if( egroup == FLX_FROMHOST_GROUPS-1 && epath == 7 )
                    cout << " from-host: EC (RM4)";
                  else if( egroup == FLX_FROMHOST_GROUPS-1 && epath == 6 )
                    cout << " from-host: IC (RM4)";
                  else if( egroup == FLX_FROMHOST_GROUPS-1 && epath == 5 )
                    cout << " from-host: AUX(RM4)";
                  else
                    cout << " from-host: ---";

                  cout << endl;
                }
              else
                {
                  cout << " " << hex << setw(3) << elinknr << " = "
                       << dec << lnk << "-" << egroup << "-" << epath << endl;
                }
            }
        }
    }
  if( egroup == FLX_TOHOST_GROUPS )
    {
      lnk    = 12;
      egroup = FLX_TOHOST_GROUPS-1;
      epath  = 3;
      elinknr = ((lnk << BLOCK_LNK_SHIFT) |
                 (egroup << BLOCK_EGROUP_SHIFT) | epath);
      cout << endl << " " << hex << setw(3) << elinknr << " = "
           << dec << lnk << "-" << egroup << "-" << epath;
      cout << " to-host: TTC(RM4)" << endl;
    }
}

// ----------------------------------------------------------------------------

void listLpgbtElinks( int linknr, int egroupnr, int epathnr )
{
  int linkstart  = 0, linkend  = FLX_LINKS;
  int groupstart = 0, groupend = FLX_TOHOST_GROUPS-1;
  int pathstart  = 0, pathend  = 4;

  cout << "==> lpGBT E-link list <==" << endl;

  // List elinks from all or a single GBT
  if( linknr >= 0 )
    {
      linkstart = linknr;
      linkend   = linknr + 1;
    }
  // List elinks from all or a single egroup
  if( egroupnr >= 0 )
    {
      groupstart = egroupnr;
      groupend   = egroupnr + 1;
    }
  // List elinks from all or a single epath
  if( epathnr >= 0 )
    {
      pathstart = epathnr;
      pathend   = epathnr + 1;
    }

  cout << setfill('0') << uppercase;

  int lnk, egroup, epath, elinknr;
  for( lnk=linkstart; lnk<linkend; ++lnk )
    {
      for( egroup=groupstart; egroup<groupend; ++egroup )
        {
          cout << "Link-" << lnk
               << " Egroup " << egroup << ":" << endl;
          for( epath=pathstart; epath<pathend; ++epath )
            {
              elinknr = ((lnk << BLOCK_LNK_SHIFT) |
                         (egroup << BLOCK_EGROUP_SHIFT_LPGBT) | epath);
              cout << " " << hex << setw(3) << elinknr << " = "
                   << dec << lnk << "-" << egroup << "-" << epath << endl;
            }
        }
    }
}

// ----------------------------------------------------------------------------

void usage()
{
  cout << "felink version " << hex << VERSION_ID << dec << endl <<
    "Convert a given E-link number into (lp)GBT, egroup and epath numbers, "
    "or vice versa.\n"
    "The E-link number is provided as a (hex) number directly (-e option)\n"
    "or as a set of -G/g/p options.\n"
    "Optionally checks if this E-link is valid and configured "
    "on a given FLX-device (option -d),\n"
    "in to-host and from-host direction, taking into account GBT or lpGBT.\n\n"
    "Using option -d without providing an e-link number can be used to list\n"
    "*enabled* e-links in both ToHost and FromHost directions on the FLX-device.\n\n"
    "Use option -l or -L to display a list of valid ((lp)GBT) E-link numbers,\n"
    "optionally in combination with -G or -g options to restrict the output\n"
    "to a particular link and/or egroup.\n"
    "(Note that E-link numbers are also shown by the elinkconfig GUI).\n\n"

    "Usage: felink [-h|V] [-d <devnr>] (-e|E <elink>\n"
    "               | (-G <lnk> (-g <group> -p <path>))\n"
    "  -h         : Show this help text.\n"
    "  -V         : Show version.\n"
    "  -d <devnr> : FLX-device to use (default: 0).\n"
    "  -e <elink> : GBT E-link number (hex) or use -G/g/p options.\n"
    "  -E <elink> : lpGBT E-link number (hex) or use -G/g/p options.\n"
    "  -G <lnk>   : (lp)GBT-link number.\n"
    "  -g <group> : Group number.\n"
    "  -i <dma>   : In combination with -d lists enabled (ToHost) e-links\n"
    "               assigned to the given DMA descriptor index only.\n"
    "  -l|L       : Show a list of valid GBT or lpGBT E-link numbers\n"
    "               (use options -G/g/p to restrict the list).\n"
    "  -p <path>  : E-path number.\n";
}

// ----------------------------------------------------------------------------
