#include <iostream>
#include <iomanip>
using namespace std;
#include <unistd.h>

#include "felixtag.h"
#include "flxcard/FlxCard.h"
#include "flxcard/FlxException.h"
#include "arg.h"

// Version identifier: year, month, day, release number
const   int VERSION_ID = 0x22030800; // Options -T/c: L1A-triggered emulator,
                                     // options -t and -i for untriggered
//const int VERSION_ID = 0x20042100; // Check for firmware mode 8 (MROD)
//const int VERSION_ID = 0x18112800; // Wait before touching fan-out at disable
//const int VERSION_ID = 0x17091300;

//const uint64_t FO_SEL_LOCKBIT = 0x80000000;

// ----------------------------------------------------------------------------

int main( int argc, char *argv[] )
{
  int  opt;
  int  cardnr               = 0;
  bool enable_emulator      = false;
  bool disable_emulator     = false;
  bool external_emulator    = false;
  bool unlock_fanout        = false;
  bool lock_fanout          = false;
  bool tohost_fanout_to_emu = false;
  bool get_status           = true;
  bool fe_emu               = false;
  bool fe_emu_triggered     = false;
  int  fe_emu_size          = -1;
  int  fe_emu_idles         = -1;

  // Parse the options
  while( (opt = getopt(argc, argv, "d:eEfhi:lLns:TtV")) != -1 )
    {
      switch( opt )
        {
        case 'd':
          if( sscanf( optarg, "%d", &cardnr ) != 1 )
            arg_error( 'd' );
          break;
        case 'E':
          if( enable_emulator || disable_emulator )
            {
              cout << "### Select one of -e, -E or -n\n";
              return 1;
            }
          external_emulator = true;
          enable_emulator = true;
          //get_status = false;
          break;
        case 'e':
          if( enable_emulator || disable_emulator )
            {
              cout << "### Select one of -e, -E or -n\n";
              return 1;
            }
          enable_emulator = true;
          //get_status = false;
          break;
        case 'f':
          tohost_fanout_to_emu = true;
          break;
        case 'h':
          usage();
          return 0;
        case 'i':
          if( sscanf( optarg, "%d", &fe_emu_idles ) != 1 )
            arg_error( 'i' );
          if( fe_emu_idles < 4 || fe_emu_idles > 65532 )
            arg_range( 'i', 4, 65532 );
          fe_emu_idles &= 0xFFFC; // Multiple of 4
          break;
        case 'l':
          unlock_fanout = true;
          break;
        case 'L':
          lock_fanout = true;
          break;
        case 'n':
          if( enable_emulator )
            {
              cout << "### Select one of -e, -E or -n\n";
              return 1;
            }
          disable_emulator = true;
          //get_status = false;
          break;
        case 's':
          if( sscanf( optarg, "%d", &fe_emu_size ) != 1 )
            arg_error( 's' );
          if( fe_emu_size < 4 || fe_emu_size > 65532 )
            arg_range( 's', 4, 65532 );
          fe_emu_size &= 0xFFFC; // Multiple of 4
          break;
        case 'T':
          fe_emu = true;
          fe_emu_triggered = true;
          break;
        case 't':
          fe_emu = true;
          break;
        case 'V':
          cout << "Version " << hex << VERSION_ID << dec
               << " (tag: " << FELIX_TAG << ")" << endl;
          return 0;
        default: // '?'
          usage();
          return 1;
        }
    }

  if( lock_fanout && unlock_fanout )
    {
      cout << "### Choose lock (-l) OR unlock (-L) FanOut-Select" << endl;
      lock_fanout = false;
      unlock_fanout = false;
    }

  if( (fe_emu && fe_emu_size == -1) || (!fe_emu && fe_emu_size > 0) )
    {
      cout << "### Use both -T/t and -s for the 'logic' emulator" << endl;
      return 1;
    }
  if( fe_emu && !fe_emu_triggered && fe_emu_idles == -1 )
    {
      cout << "### Use -t with -s and -i for the 'logic' emulator" << endl;
      return 1;
    }
  if( fe_emu && !(enable_emulator || disable_emulator) )
    {
      cout << "### Use option -e or -E in combination with -T or -t" << endl;
      return 1;
    }

  // Open FELIX FLX-card
  FlxCard *flx = new FlxCard;
  try {
    flx->card_open( cardnr, LOCK_NONE );
  }
  catch( FlxException &ex ) {
    cout << "### FlxCard open: " << ex.what() << endl;
    return 1;
  }

  flxcard_bar2_regs_t *bar2 = (flxcard_bar2_regs_t *) flx->bar2Address();

  if( lock_fanout || unlock_fanout )
    {
      if( lock_fanout )
        {
          bar2->GBT_TOFRONTEND_FANOUT.LOCK = 1;
          bar2->GBT_TOHOST_FANOUT.LOCK     = 1;
          cout << "FanOut-Select locked\n";
        }
      else
        {
          bar2->GBT_TOFRONTEND_FANOUT.LOCK = 0;
          bar2->GBT_TOHOST_FANOUT.LOCK     = 0;
          cout << "FanOut-Select unlocked\n";
        }
    }
  uint64_t lockbit = ((bar2->GBT_TOFRONTEND_FANOUT.LOCK == 1) |
                      (bar2->GBT_TOHOST_FANOUT.LOCK == 1));

  uint64_t fw_mode = bar2->FIRMWARE_MODE;

  cout << hex << setfill('0');

  if( enable_emulator )
    {
      // Use the alternative 'logic' based emulator?
      // (instead of the RAM based one)
      if( fe_emu )
        {
          if( fe_emu_idles > -1 )
            {
              bar2->FE_EMU_LOGIC.IDLES = fe_emu_idles;
            }
          else
            {
              // Must be at least 4 to prevent 'funny' behaviour
              // (malformed packets)
              if( bar2->FE_EMU_LOGIC.IDLES < 4 )
                bar2->FE_EMU_LOGIC.IDLES = 4;
            }
          bar2->FE_EMU_LOGIC.CHUNK_LENGTH = fe_emu_size;
          if( fe_emu_triggered )
            bar2->FE_EMU_LOGIC.L1A_TRIGGERED = 1;
          bar2->FE_EMU_LOGIC.ENA = 1;
        }
      else
        {
          bar2->FE_EMU_LOGIC.IDLES = 0;
          bar2->FE_EMU_LOGIC.CHUNK_LENGTH = 0;
          bar2->FE_EMU_LOGIC.L1A_TRIGGERED = 0;
          bar2->FE_EMU_LOGIC.ENA = 0;
        }

      if( external_emulator )
        {
          // Enable external emulator

          // FanOut-Select registers
          bar2->GBT_TOFRONTEND_FANOUT.SEL = 0xFFFFFF; // From emulator
          bar2->GBT_TOHOST_FANOUT.SEL     = 0x000000;

          // Emulator
#if REGMAP_VERSION < 0x500
          bar2->GBT_EMU_ENA.TOFRONTEND = 1;
          bar2->GBT_EMU_ENA.TOHOST     = 0;
          bar2->GBT_FM_EMU_ENA_TOHOST  = 0;
#else
          bar2->FE_EMU_ENA.EMU_TOFRONTEND = 1;
          bar2->FE_EMU_ENA.EMU_TOHOST     = 0;
#endif
          cout << "=> Enabled TOFRONTEND ";
          if( fe_emu )
            cout << "L1A-triggered ";
          cout << "Emulator";
        }
      else
        {
          // Enable internal emulator

          // FanOut-Select registers
          bar2->GBT_TOFRONTEND_FANOUT.SEL = 0x000000;
          bar2->GBT_TOHOST_FANOUT.SEL     = 0xFFFFFF; // From emulator

          // Emulator type: FULLMODE or GBT?
          if( fw_mode == FIRMW_FULL )
            {
#if REGMAP_VERSION < 0x500
              bar2->GBT_FM_EMU_ENA_TOHOST = 1;
#else
              bar2->FE_EMU_ENA.EMU_TOHOST = 1;
#endif
              cout << "=> Enabled FullMode ";
              if( fe_emu )
                {
                  if( fe_emu_triggered )
                    cout << "L1A-triggered ";
                  else
                    cout << "'logic' ";
                }
              cout << "Emulator";
            }
          else
            {
#if REGMAP_VERSION < 0x500
              bar2->GBT_EMU_ENA.TOFRONTEND = 0;
              bar2->GBT_EMU_ENA.TOHOST     = 1;
#else
              bar2->FE_EMU_ENA.EMU_TOFRONTEND = 0;
              bar2->FE_EMU_ENA.EMU_TOHOST  = 1;
#endif
              cout << "=> Enabled TOHOST ";
              if( fe_emu )
                {
                  if( fe_emu_triggered )
                    cout << "L1A-triggered ";
                  else
                    cout << "'logic' ";
                }
              cout << "Emulator";
            }
        }
      cout << endl;
    }
  else if( disable_emulator )
    {
      // Disable emulators
#if REGMAP_VERSION < 0x500
      bar2->GBT_EMU_ENA.TOFRONTEND = 0;
      bar2->GBT_EMU_ENA.TOHOST     = 0;
      bar2->GBT_FM_EMU_ENA_TOHOST  = 0;
#else
      bar2->FE_EMU_ENA.EMU_TOFRONTEND = 0;
      bar2->FE_EMU_ENA.EMU_TOHOST  = 0;
#endif
      // The alternative 'logic' based emulator
      bar2->FE_EMU_LOGIC.ENA = 0;
      bar2->FE_EMU_LOGIC.L1A_TRIGGERED = 0;
      bar2->FE_EMU_LOGIC.IDLES = 0;
      bar2->FE_EMU_LOGIC.CHUNK_LENGTH = 0;

      cout << "=> Disabled emulator" << endl;

      // Touching fan-out with data still flowing is a bad idea:
      // give it some time...
      usleep( 250000 );

      // FanOut-Select registers
      bar2->GBT_TOFRONTEND_FANOUT.SEL = 0x000000;
      if( tohost_fanout_to_emu )
        bar2->GBT_TOHOST_FANOUT.SEL = 0xFFFFFF;
      else
        bar2->GBT_TOHOST_FANOUT.SEL = 0x000000;
    }

  bool firmw_with_emu = !(fw_mode == FIRMW_FELIG_GBT ||
                          fw_mode == FIRMW_FMEMU ||
                          fw_mode == FIRMW_MROD);
  if( get_status )
    {
      // Display the current content of all the relevant registers
      cout << "Firmware : " << flx->firmware_type_string() << endl;
      if( fw_mode == FIRMW_FULL )
        {
          cout << "RAM emu  : ";
#if REGMAP_VERSION < 0x500
          cout << "FM_EMU_ENA_TOHOST=" << bar2->GBT_FM_EMU_ENA_TOHOST;
#else
          cout << "FM_EMU_ENA_TOHOST=" << bar2->FE_EMU_ENA.EMU_TOHOST;
#endif
          cout << endl;
        }
      else if( firmw_with_emu )
        {
          cout << "RAM emu  : ";
#if REGMAP_VERSION < 0x500
          cout << "EMU_ENA_TOFE=" << bar2->GBT_EMU_ENA.TOFRONTEND
               << ", EMU_ENA_TOHOST=" << bar2->GBT_EMU_ENA.TOHOST;
#else
          cout << "EMU_ENA_TOFE=" << bar2->FE_EMU_ENA.EMU_TOFRONTEND
               << ", EMU_ENA_TOHOST=" << bar2->FE_EMU_ENA.EMU_TOHOST;
#endif
          cout << endl;
        }

      if( firmw_with_emu )
        {
          // 'Logic' based emulator status
          cout << "Logic emu: " << "FE_EMU_ENA=" << bar2->FE_EMU_LOGIC.ENA
               << ", FE_EMU_L1A_TRIG=" << bar2->FE_EMU_LOGIC.L1A_TRIGGERED
               << dec
               << ", FE_EMU_CHUNKSZ=" << bar2->FE_EMU_LOGIC.CHUNK_LENGTH
               << ", FE_EMU_IDLES=" << bar2->FE_EMU_LOGIC.IDLES
               << hex << endl;
        }
    }

  if( firmw_with_emu )
    {
      cout << "Fanout   : "
           << "FROMHOST_FANOUT=" << setw(8) << bar2->GBT_TOFRONTEND_FANOUT.SEL
           << " LOCK=" << bar2->GBT_TOFRONTEND_FANOUT.LOCK
           << ", TOHOST_FANOUT=" << setw(8) << bar2->GBT_TOHOST_FANOUT.SEL
           << " LOCK=" << bar2->GBT_TOHOST_FANOUT.LOCK;
      if( lockbit != 0 )
        cout << " (Locked)";
      cout << endl;
    }

  // Close the FLX-card
  try {
    flx->card_close();
  }
  catch( FlxException &ex ) {
    cout << "FlxCard close: " << ex.what() << endl;
  }

  return 0;
}

// ----------------------------------------------------------------------------

void usage()
{
  cout << "femu version " << hex << VERSION_ID << dec << endl <<
    "Show or configure 'FanOut-Select' registers and start or stop\n"
    "an emulator on a FELIX device.\n"
    "Usage: femu -h|V -d<devnr> -e|E|n -l|L [-T|t -s<size> -i<idles>]\n"
    "  -h         : Show this help text.\n"
    "  -V         : Show version.\n"
    "  -d<devnr>  : FLX-device to use (default: 0).\n"
    "  -e|E|n     : Enable FLX-device data emulator, internal (e) or "
    "external (E) or disable emulator (n).\n"
    "               When no option is given the current status is displayed.\n"
    "  -f         : When disabling emulator set TOHOST_FANOUT to emulator "
    "(default: to external).\n"
    "  -l         : 'Unlock' FanOut-Select registers.\n"
    "  -L         : 'Lock' FanOut-Select registers.\n"
    "  -i<idles>  : Number of idles between chunks, in bytes (multiple of 4)\n"
    "  -s<size>   : L1A-triggered emu chunk size, in bytes (multiple of 4)\n"
    "               (in combination with option -t or -T).\n"
    "  -T         : Select the alternative ('logic') L1A-triggered emulator\n"
    "               (also option -s required; FULL firmware only).\n"
    "  -t         : Select the alternative ('logic') emulator, untriggered\n"
    "               (also options -i and -s required; FULL firmware only).\n";
}

// ----------------------------------------------------------------------------
