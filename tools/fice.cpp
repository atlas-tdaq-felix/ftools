#include <iostream>
#include <iomanip>
using namespace std;
#include <unistd.h>
#include <cstring> // For memset(), memcpy()

#include "felixtag.h"
#include "FlxUpload.h"
#include "FlxReceiver.h"
#include "FlxParser.h"
#include "arg.h"
#include "ic.h"

// Version identifier: year, month, day, release number
const   int VERSION_ID = 0x24032200; // Option -S<i2c> (one DMA per I2C op)
//const int VERSION_ID = 0x23072800; // Add file format
                                     // "<addr> <byte> <comment>"; option -o
                                     // also applies to writing range of regs
//const int VERSION_ID = 0x22072200; // Option -C in combination with filename:
                                     // compare (lp)GBT(X) registers to file contents
//const int VERSION_ID = 0x22070800; // Secondary lpGBT via I2C Master of
                                     // a primary lpGBT: options -f, -m and -s.
//const int VERSION_ID = 0x22060300; // Handle "<addr> <byte>" file format too;
                                     // option -T (.txt output with address)
//const int VERSION_ID = 0x22040500; // Autodetect lpGBTv0/GBTX or lpGBTv1
//const int VERSION_ID = 0x21122800; // Data value hex must have x or 0x
//const int VERSION_ID = 0x21122300; // Option -1 (lpGBTv1) and -e (EC-link),
//                                      i.e. connecting to a secondary lpGBT.
//const int VERSION_ID = 0x21092100; // Option -i (select ToHost DMA controller)
//const int VERSION_ID = 0x21091300; // Option -e (use EC-link, for lpGBT)
//const int VERSION_ID = 0x21012200; // Option -D (debug)
//const int VERSION_ID = 0x21012000; // Check for lpGBT firmware
//const int VERSION_ID = 0x18102400;

// ----------------------------------------------------------------------------

int main( int argc, char *argv[] )
{
  int    opt;
  int    cardnr      = 0;
  int    dma_write   = -1; // Autoselect FromHost DMA controller index
  int    dma_read    = 0;
  int    linknr      = -1;
  int    i2c_addr    = -1;
  int    reg_addr    = -1;
  int    databyte    = -1;
  bool   write_reg   = false;
  bool   one_by_one  = false;
  bool   receive     = true;
  bool   receive_any = false;
  bool   use_intrpt  = false;
  bool   txt_output  = false;
  bool   addr_output = false;
  //bool file_is_xml = false;
  bool   lpgbt       = false;
  bool   debug       = false;
  bool   use_ec      = false;
  bool   lpgbt_v0_f  = false; // Forced v0 selection
  bool   lpgbt_v1_f  = false; // Forced v1 selection
  bool   i2c_op      = false; // Redundant but more readable
  int    i2c_master  = 0;
  int    i2c_freq    = 100;
  int    i2c_addr_slave = -1;
  bool   i2c_op_per_dma = false;
  bool   compare_file = false;
  string filename;

  // Parse the options
  unsigned int x;
  while( (opt = getopt(argc, argv, "01a:Cd:Def:G:hi:I:m:oRs:tTVZ")) != -1 )
    {
      switch( opt )
        {
        case '0':
          lpgbt_v0_f = true;
          break;
        case '1':
          lpgbt_v1_f = true;
          break;
        case 'a':
          // GBTX/lpGBT register address
          {
            // Accept hex values "x12" and "0x12", as well as decimal "18"
            int result, i = 0;
            if( optarg[0] == 'x' || optarg[0] == 'X' )
              i = 1;
            else if( optarg[0] == '0' &&
                     (optarg[1] == 'x' || optarg[1] == 'X') )
              i = 2;

            if( i != 0 )
              {
                unsigned int x;
                result = sscanf( &optarg[i], "%x", &x );
                if( result == 1 )
                  {
                    arg_check_hex( &optarg[i], opt );
                    reg_addr = (int) x;
                  }
              }
            else
              {
                result = sscanf( &optarg[i], "%d", &reg_addr );
                if( result == 1 )
                  arg_check_dec( optarg, opt );
              }

            if( result != 1 )
              arg_error( 'a' );

            if( reg_addr < 0 || reg_addr > 511 )
              arg_range( 'a', 0, 511 );
          }
          break;
        case 'C':
          compare_file = true;
          break;
        case 'd':
          if( sscanf( optarg, "%d", &cardnr ) != 1 )
            arg_error( 'd' );
          break;
        case 'D':
          debug = true;
          break;
        case 'e':
          use_ec = true;
          break;
        case 'f':
          // I2C bus frequency (in KHz)
          if( sscanf( optarg, "%d", &i2c_freq ) != 1 )
            arg_error( 'f' );
          if( i2c_freq != 100 && i2c_freq != 200 &&
              i2c_freq != 400 && i2c_freq != 1000 )
            {
              cout << "### -f: select one of 100,200,400,1000" << endl;
              return 1;
            }
          break;
        case 'G':
          // (lp)GBT link number
          if( sscanf( optarg, "%d", &linknr ) != 1 )
            arg_error( 'G' );
          if( linknr < 0 || linknr > FLX_LINKS-1 )
            arg_range( 'G', 0, FLX_LINKS-1 );
          break;
        case 'h':
          usage();
          return 0;
        case 'i':
          // DMA controller to use
          if( sscanf( optarg, "%d", &dma_read ) != 1 )
            arg_error( 'i' );
          if( dma_read < 0 || dma_read > 7 )
            arg_range( 'i', 0, 7 );
          break;
        case 'I':
          // GBTX/lpGBT I2C address
          if( sscanf( optarg, "%x", &x ) != 1 )
            arg_error( 'I' );
          i2c_addr = x;
          if( i2c_addr < 0 || i2c_addr > 127 )
            arg_range_hex( 'I', 0, 127 );
          break;
        case 'm':
          // lpGBT I2C Master index (0, 1 or 2)
          // through which to access a secondary lpGBT
          if( sscanf( optarg, "%d", &i2c_master ) != 1 )
            arg_error( 'm' );
          if( i2c_master < 0 || i2c_master > 2 )
            arg_range( 'm', 0, 2 );
          break;
        case 'o':
          one_by_one = true;
          break;
        case 'R':
          receive_any = true;
          break;
        case 's':
        case 'S':
          // Secondary lpGBT I2C address
          if( sscanf( optarg, "%x", &x ) != 1 )
            arg_error( opt );
          i2c_addr_slave = x;
          if( i2c_addr_slave < 0 || i2c_addr_slave > 127 )
            arg_range_hex( opt, 0, 127 );
          i2c_op = true;
          if( opt == 'S' )
            i2c_op_per_dma = true;
          break;
        case 't':
          txt_output = true;
          break;
        case 'T':
          txt_output = true;
          addr_output = true;
          break;
        case 'V':
          cout << "Version " << hex << VERSION_ID << dec
               << " (tag: " << FELIX_TAG << ")" << endl;
          return 0;
        /*
          case 'X':
          file_is_xml = true;
          break;
        */
        case 'Z':
          receive = false;
          break;
        default: // '?'
          return 1;
        }
    }

  if( lpgbt_v0_f && lpgbt_v1_f )
    {
      cout << "### WARNING: select only one of -0 or -1" << endl;
      lpgbt_v0_f = false;
      lpgbt_v1_f = false;
    }

  if( use_ec && i2c_op )
    {
      cout << "### Options -e and -s can not be used together" << endl;
      return 1;
    }

  // Check for mandatory parameters
  if( linknr < 0 || i2c_addr < 0 )
    {
      if( linknr < 0 )
        cout << "### Provide a (lp)GBT link number (-G)" << endl;
      if( i2c_addr < 0 )
        cout << "### Provide a (lp)GBT I2C address (-I)" << endl;
      return 1;
    }

  // Data byte value to write or name of file with GBTX/lpGBT register values
  if( optind < argc )
    {
      // There is an additional parameter:
      // it must be a byte value or a file name
      if( reg_addr != -1 )
        {
          // Expect a byte value to write;
          // accept hex values "x12" and "0x12", as well as decimal "18"
          char *str = argv[optind];
          int result, i = 0;
          if( str[0] == 'x' || str[0] == 'X' )
            i = 1;
          else if( str[0] == '0' && (str[1] == 'x' || str[1] == 'X') )
            i = 2;

          if( i != 0 )
            {
              unsigned int x;
              result = sscanf( &str[i], "%x", &x );
              if( result == 1 )
                {
                  arg_check_hex( &str[i], 0 );
                  databyte = (int) x;
                }
            }
          else
            {
              result = sscanf( &str[i], "%d", &databyte );
              if( result == 1 )
                arg_check_dec( str, 0 );
            }

          if( result == 1 )
            {
              if( databyte < 0 || databyte > 255 )
                {
                  cout << "### Data byte value out-of-range 0..0xFF: 0x"
                       << hex << setfill('0') << setw(2)
                       << (u32) databyte << dec << endl;
                  return 1;
                }
              write_reg = true;
            }
          else
            {
              cout << "### Invalid data byte value to write provided" << endl;
              return 1;
            }
        }
      else
        {
          filename = string( argv[optind] );
        }
    }

  if( compare_file && filename.empty() )
    {
      cout << "### WARNING: option -C (compare) requires a file name"
           << endl;
      compare_file = false;
    }

  // -------------------------------------------------------------------------
  // FLX-card sender and receiver

  FlxUpload fup( cardnr, 0, dma_write );
  if( fup.hasError() )
    {
      cout << "### " << fup.errorString() << endl;
      return 1;
    }
  cout << "Opened FLX-device " << cardnr
       << ", firmw " << fup.firmwareVersion() << endl;

  if( fup.fanOutLocked() )
    cout <<  "**NB**: FanOut-Select registers are locked!" << endl;
  fup.setFanOutForDaq();

  // Is it firmware for lpGBT ?
  lpgbt = fup.lpgbtType();

  if( use_ec && !lpgbt )
    cout << "### WARNING: option -e applies to lpGBT only" << endl;

  if( i2c_op && !lpgbt )
    {
      cout << "### Option -s only used in combination with lpGBT (lpGBT_I2C->lpGBT)\n"
           << "    (for GBTX|lpGBT->GBTSCA_I2C->GBTX use fscagbtxconf).\n";
      fup.stop();
      return 1;
    }

  FlxReceiver *frecvr = 0;
  if( receive )
    {
      frecvr = new FlxReceiver( cardnr, 64*1024*1024, dma_read );
      if( frecvr->hasError() )
        {
          cout << "### " << frecvr->errorString() << endl;
          fup.stop();
          frecvr->stop();
          return 1;
        }
      /*cout << "Opened FLX-device " << cardnr
           << ", firmw " << frecvr->firmwareVersion()
           <<  " (cmem buffersize="
           << frecvr->bufferSize() << ", receive: "
           << "DMA #" << dma_read;
      if( use_intrpt )
        cout << " interrupt-driven)" << endl;
      else
        cout << " polling)" << endl;
      */
      frecvr->setUseInterrupt( use_intrpt );
    }

  // Auto-detect chip/protocol version if no forced selection
  bool lpgbt_v1 = false;
  if( lpgbt_v0_f )
    {
      lpgbt_v1 = false;
    }
  else if( lpgbt_v1_f )
    {
      lpgbt_v1 = true;
    }
  else
    {
      // GBTX/lpGBTv0 or lpGBTv1 ?
      if( receive )
        lpgbt_v1 = detectLpGbtV1( &fup, frecvr, linknr, i2c_addr, use_ec );
      else
        // Not receiving: type selection should be forced...
        detectLpGbtV1( &fup, frecvr, linknr, i2c_addr, use_ec );
    }

  // -------------------------------------------------------------------------

  // Describe the requested operation:
  if( lpgbt )
    cout << ">>> lpGBTv" << (lpgbt_v1 ? "1" : "0");
  else
    cout << ">>> GBTX";
  cout << "@Lnk" << linknr;
  if( use_ec )
    cout << "-via-EC";
  cout << " I2C-addr=0x" << hex << i2c_addr << dec;
  if( i2c_op )
    cout << " I2C Master" << i2c_master << " " << i2c_freq << "KHz" << endl
         << ">>> to 2nd lpGBT I2C-addr=0x" << hex << i2c_addr_slave << dec;
  cout << ": ";
  if( !filename.empty() && !compare_file )
    cout << "APPLY file contents: " << filename << endl;
  else if( !filename.empty() )
    cout << "COMPARE to file contents: " << filename << endl;
  else if( write_reg )
    cout << "WRITE 0x" << hex << setfill('0') << setw(2) << databyte
         << dec << " (" << databyte << ")"
         << " to reg 0x" << hex << setw(3) << reg_addr
         << dec << " (" << reg_addr << ")" << endl;
  else if( reg_addr != -1 )
    cout << "READ reg 0x" << hex << setfill('0') << setw(3) << reg_addr
         << dec << " (" << reg_addr << ")" << endl;
  else
    cout << "READ all registers" << endl;

  uint8_t regarray[512];
  std::memset( regarray, 0, 512 );
  int max_regs = 0;

  if( filename.empty() || compare_file )
    {
      bool i2c_configure = true;
      int nbytes = 1;
      uint8_t databyte_u8 = (uint8_t) (databyte & 0xFF);
      if( write_reg )
        {
          // Write a single register
          if( !i2c_op )
            {
              fup.writeIcChannel( linknr, i2c_addr, reg_addr,
                                  nbytes, &databyte_u8, use_ec, lpgbt_v1 );
              //cout << "Write " << nbytes << " bytes: "
              //     << hex << setfill('0') << setw(2) << (u32) databyte_u8
              //     << dec << endl;
            }
          else
            {
              writeIcRegsViaLpgbtI2c( &fup, frecvr, linknr, i2c_addr,
                                      i2c_master, i2c_freq, i2c_addr_slave,
                                      i2c_configure,
                                      reg_addr, nbytes, &databyte_u8,
                                      use_ec, i2c_op_per_dma,
                                      debug, true );
            }
        }
      else
        {
          // Read either a single register or all registers
          // (the latter also if there is going to be a comparison)
          if( reg_addr != -1 )
            {
              if( !i2c_op )
                {
                  fup.readIcChannel( linknr, i2c_addr, reg_addr,
                                     nbytes, use_ec, lpgbt_v1 );
                  //cout << "Read " << nbytes << " bytes" << endl;
                }
              else
                {
                  uint8_t byt;
                  if( readIcRegsViaLpgbtI2c( &fup, frecvr, linknr, i2c_addr,
                                             i2c_master, i2c_freq, i2c_addr_slave,
                                             i2c_configure,
                                             reg_addr, nbytes, &byt,
                                             use_ec, i2c_op_per_dma,
                                             debug, false ) )
                    {
                      // Display register value
                      cout << hex << setfill('0')
                           << "Reg 0x" << setw(3) << reg_addr << " ("
                           << dec << setfill(' ')
                           << setw(3) << reg_addr << "): "
                           << hex << setfill('0') << " "
                           << setw(2) << (uint32_t) byt << endl;
                    }
                }
            }
          else
            {
              // Read all registers:
              // GBTX   : 436 8-bit registers; first 366 R/W, rest read-only
              // lpGBTv0: 463 8-bit registers; first 320 R/W,  "    "
              // lpGBTv1: 494 8-bit registers; first 326 R/W,  "    "
              max_regs = 436;
              if( lpgbt )
                max_regs = (lpgbt_v1 ? 494 : 463);
              if( one_by_one )
                {
                  // Read the registers one-by-one
                  for( reg_addr=0; reg_addr<max_regs; ++reg_addr )
                    {
                      nbytes = 1;
                      if( !i2c_op )
                        {
                          fup.readIcChannel( linknr, i2c_addr,
                                             reg_addr, nbytes,
                                             use_ec, lpgbt_v1 );
                        }
                      else
                        {
                          uint8_t byt;
                          if( readIcRegsViaLpgbtI2c( &fup, frecvr, linknr, i2c_addr,
                                                     i2c_master, i2c_freq,
                                                     i2c_addr_slave, i2c_configure,
                                                     reg_addr, nbytes, &byt,
                                                     use_ec, i2c_op_per_dma,
                                                     debug, false ) )
                            {
                              // Display register value and/or save in array
                              cout << hex << setfill('0')
                                   << "Reg 0x" << setw(3) << reg_addr << " ("
                                   << dec << setfill(' ')
                                   << setw(3) << reg_addr << "): "
                                   << hex << setfill('0') << " "
                                   << setw(2) << (uint32_t) byt << endl;
                              regarray[reg_addr] = byt;
                            }
                          i2c_configure = false;
                        }
                    }
                 }
              else
                {
                  if( !i2c_op )
                    {
                      // Read all registers in one operation
                      reg_addr = 0;
                      nbytes   = max_regs;
                      fup.readIcChannel( linknr, i2c_addr, reg_addr,
                                         nbytes, use_ec, lpgbt_v1 );
                    }
                  else
                    {
                      // Read all registers in as few operations as possible,
                      // i.e. l6 bytes per I2C read operation
                      uint8_t bytes[16];
                      nbytes = 16;
                      for( int reg_addr=0; reg_addr<max_regs; reg_addr+=nbytes )
                        {
                          if( readIcRegsViaLpgbtI2c( &fup, frecvr, linknr, i2c_addr,
                                                     i2c_master, i2c_freq,
                                                     i2c_addr_slave, i2c_configure,
                                                     reg_addr, nbytes, bytes,
                                                     use_ec, i2c_op_per_dma,
                                                     debug, true ) )
                                                     //debug, false ) )
                            // Save copy of register bytes read in array
                            std::memcpy( &regarray[reg_addr], bytes, nbytes );

                          i2c_configure = false;
                        }
                    }
                }
            }
        }
    }
  else
    {
      // Use the contents of the configuration file
      // to configure GBTx or lpGBT
      int sz_file;
      if( !i2c_op )
        {
          if( fup.writeIcConfigFile( linknr, i2c_addr, filename, &sz_file,
                                     use_ec, lpgbt_v1, one_by_one ) )
            cout << "Wrote " << sz_file << " values" << endl;
          else
            cout << "### Config file: " << fup.errorString() << endl;
        }
      else
        {
          writeIcConfigFileViaLpgbtI2c( &fup, frecvr,
                                        linknr, i2c_addr,
                                        i2c_master, i2c_freq, i2c_addr_slave,
                                        filename, &sz_file, use_ec );
          cout << "Wrote " << sz_file << " values" << endl;
        }
    }

  // -------------------------------------------------------------------------

  if( i2c_op && !write_reg && (filename.empty() || compare_file) )
    {
      // Display the compiled list of register bytes in a compact overview
      cout << "All registers:" << endl;
      cout << hex << setfill('0') << uppercase;
      for( int i=0; i<max_regs; ++i )
        {
          if( (i & 0xF) == 0 )
            cout << hex << setfill('0')
                 << "Reg 0x" << setw(3) << i << " ("
                 << dec << setfill(' ')
                 << setw(3) << i << "): "
                 << hex << setfill('0');
          cout << " " << hex << setw(2) << (uint32_t) regarray[i];
          if( (i & 0xF) == 0xF )
            cout << endl;
        }
      cout << endl;
    }

  // Do not receive replies? In that case we're done
  // (or in case of using an I2C Master: all replies have been received;
  //  but we may want to do a comparison (see further down))
  if( frecvr == 0  || (i2c_op && !compare_file) )
    {
      fup.stop();
      return 0;
    }

  // The IC (or EC) elink number (will be mapped to the real number internally)
  int egroup = 7;
  int epath  = 6;
  if( use_ec )
    epath = 7;
  int elinknr = ((linknr << BLOCK_LNK_SHIFT) |
                 (egroup << BLOCK_EGROUP_SHIFT) | epath);
  if( debug )
    {
      int e_send = fup.mapElink( elinknr );
      FlxParser fparser;
      fparser.setReceiver( frecvr );
      int e_recv = fparser.mapElink( elinknr );
      if( use_ec )
        cout << hex << "DEBUG: EC-send=0x" << e_send
             << " EC-recv=0x" << e_recv << endl;
      else
        cout << hex << "DEBUG: IC-send=0x" << e_send
             << " IC-recv=0x" << e_recv << endl;
    }
  if( receive_any )
    elinknr = -1; // Receive on any E-link...

  // Collect multiple registers and display when appropriate
  uint32_t min_addr = 1000;
  uint32_t max_addr = 0;

  // Receive (a) chunk(s) from IC (or EC)
  FlxParser fparser;
  fparser.setReceiver( frecvr );
  uint8_t *chunkdata = 0;
  uint32_t size, chunkcnt = 0;
  uint32_t parity_start_index = (lpgbt_v1 ? 0 : 2);
  int offset = (lpgbt_v1 ? 0 : 1);
  while( fparser.nextChunkRecvd( &chunkdata, &size, 10000, elinknr ) )
    {
      if( size == 0 ) continue;

      // Process chunk from the IC (or EC) channel
      uint32_t frame_len = size;
      uint8_t *frame     = chunkdata;
      // Frame payload:
      // (I2C address, zero-byte (lpGBTv0), command, #bytes, #address, parity:
      //  7 (v1) or 8 (v0) bytes in total) (###TBD: define v0 and v1 structs?)
      int data_len = frame_len - (offset+1+1+2+2+1);
      // Check reply frame CRC
      // (do not include I2C-address and zero-byte, in case of GBT or lpGBTv0)
      if( frame_len > 2 )
        {
          if( !txt_output )
            cout << "Reply (size=" << frame_len << "): ";
          uint8_t parity = 0;
          for( uint32_t i=parity_start_index; i<frame_len-1; ++i )
            parity ^= frame[i];
          if( parity != frame[frame_len-1] )
            {
              cout << hex << setfill('0')
                   << "### Parity "
                   << setw(2) << (uint32_t) frame[frame_len-1]
                   << ", expected "
                   << setw(2) << (uint32_t) parity
                   << endl << dec << setfill(' ');
              //continue;
            }
          else if( !txt_output && !one_by_one )
            {
              cout << "Parity OK";
              // One register: display on the same line
              if( data_len > 1 )
                cout << endl;
              else
                cout << "  ";
            }
        }

      // Display returned data words (bytes) in frame
      cout << uppercase;
      if( data_len > 0 )
        {
          uint32_t len  = ((uint32_t) frame[offset+2] +
                           ((uint32_t) frame[offset+3]<<8));
          uint32_t addr = ((uint32_t) frame[offset+4] +
                           ((uint32_t) frame[offset+5]<<8));

          // NB! bug in GBTX:
          // reading address 255 (0xFF) returns 511 (0x1FF) as address
          // (see CERN JIRA ticket GBTSUPPORT-486)
          if( addr == 0x1FF ) // Non-existent in lpGBT...
            addr = 0xFF;

          cout << hex << setfill('0');
          for( int i=0; i<data_len; ++i )
            {
              if( (i & 0xF) == 0 && !txt_output )
                cout << hex << setfill('0')
                     << "Reg 0x" << setw(3) << (addr+i) << " ("
                     << dec << setfill(' ')
                     << setw(3) << (addr+i) << "): "
                     << hex << setfill('0');
              if( !txt_output ) cout << " ";
              if( txt_output && addr_output )
                cout << "0x" << setw(3) << addr+i << " 0x"
                     << setw(2) << (uint32_t) frame[offset+6+i];
              else
                cout << hex << setw(2)
                     << (uint32_t) frame[offset+6+i];
              if( (i & 0xF) == 0xF || (txt_output && len > 1) )
                cout << endl;

              // Store a copy in regarray[] for possible later display
              regarray[addr+i] = frame[offset+6+i];
            }
          cout << dec << endl;
          if( data_len != (int) len )
            {
              cout << "### Mismatched frame data size / length word: "
                   << data_len << ", " << len << endl;
            }

          // Keep track of addresses received in case of multiple chunks
          if( len == 1 && addr < min_addr )
            min_addr = addr;
          if( len == 1 && addr > max_addr )
            max_addr = addr;
        }
      ++chunkcnt;
    }

  if( min_addr == 0 && max_addr > 0 && !txt_output )
    {
      // Display the compiled list of register bytes in a compact overview
      // (i.e. having received them in multiple single register byte chunks)
      cout << "=> Overview of received register values up to max address "
           << max_addr
           << hex << setfill('0') << " (0x" << max_addr << ")" << endl
           << "   (NB: regs not received are also shown having value 00):"
           << endl;
      for( uint32_t i=0; i<=max_addr; ++i )
        {
          if( (i & 0xF) == 0 )
            cout << hex << setfill('0')
                 << "Reg 0x" << setw(3) << i << " ("
                 << dec << setfill(' ')
                 << setw(3) << i << "): "
                 << hex << setfill('0');
          cout << " " << setw(2) << (uint32_t) regarray[i];
          if( (i & 0xF) == 0xF )
            cout << endl;
        }
      cout << dec << endl;
    }

  if( debug )
    cout << "(chunks received: " << chunkcnt << ")" << endl;

  if( chunkcnt == 0 && !i2c_op )
    {
      cout << "### Nothing received" << endl;
    }
  else if( compare_file )
    {
      // Compare lpGBT or GBTX register contents to file contents
      cout << endl << "=> Comparison:" << endl;
      int nr_of_diffs, nregs;
      nr_of_diffs = compareIcConfigFile( regarray, max_addr,
                                         lpgbt, lpgbt_v1,
                                         filename, &nregs );
      cout << "Compared " << nregs << " values, found "
           << nr_of_diffs << " differences" << endl;
    }

  fup.stop();
  if( frecvr )
    frecvr->stop();

  return 0;
}

// ----------------------------------------------------------------------------

void usage()
{
  cout << "fice version " << hex << VERSION_ID << dec << endl <<
    "Tool to read or write GBTX or lpGBT registers via the IC-channel\n"
    "of an FLX-device (lp)GBT link,\n"
    "and for lpGBT optionally from/to a secondary lpGBT via the primary lpGBT's "
    "EC-channel,\n"
    "or from/to a secondary lpGBT via an I2C Master of the primary lpGBT.\n\n"
    "Read or write a single byte from or to the given GBTX/lpGBT register address\n"
    "or write to multiple consecutive GBTX/lpGBT registers using the contents\n"
    "of a file (i.e. ASCII file: 1 (register) byte value (hex) per line of text,\n"
    "e.g. the 'TXT' file generated by the GBTXProgrammer tool,\n"
    "or alternatively per line of text an address followed by a byte value,\n"
    "optionally followed by a comment text.\n"
    "NB: in the latter case registers are always written one-by-one,\n"
    "    as the registers may be listed in any order).\n"
    "Using option -C the file contents is not used to *configure* but *compared*\n"
    "to the current GBTX or lpGBT register contents.\n\n"
    "Provide a file name *or* use option -a with an address and "
    "an optional additional byte value\n"
    "to read resp. write a single GBTX or lpGBT register or, without option -a,\n"
    "to read (and display) *all* registers of the GBTX or lpGBT (v0 or v1).\n\n"
    "Without both option -a and file name all registers are read out and displayed\n"
    "either in one IC read operation or optionally one-by-one (option -o).\n"
    "Reading via another lpGBT I2C Master (option -s) is either one-by-one or in groups of 16\n"
    "(the maximum number of bytes that can be read in a single I2C Master operation).\n\n"
    "Option -t displays the register values in a format that could be used\n"
    "as a 'TXT' file for this tool or the I2C-dongle GBTX programmer.\n"
    "Usage:\n"
    " fice -h|V -d<devnr> -G<lnk> -0|1 -e -i<dma> -I<i2c> -Z|R -t|T\n"
    "      -m<master> -f<khz> -s<i2c> -a<addr> <byte>|<filename>\n"
    "  -h         : Show this help text.\n"
    "  -V         : Show version.\n"
    "  -0         : If lpGBT, assume v0 (default: auto-detect).\n"
    "  -1         : If lpGBT, assume v1 (default: auto-detect).\n"
    "  -a <addr>  : GBTX/lpGBT register address (decimal or hex, 0x.. or x..)\n"
    "               to read or write.\n"
    "  -C         : In combination with <filename>: compare GBTX/lpGBT register\n"
    "               contents to file contents and display the differences.\n"
    "  -d <devnr> : FLX-device to use (default: 0).\n"
    "  -e         : Use the lpGBT EC-channel to access a secondary lpGBT.\n"
    "  -f <freq>  : I2C Master bus frequency, in KHz\n"
    "               (only with -s; 100,200,400 or 1000; default:100).\n"
    "  -G <lnk>   : GBT-link number.\n"
    "  -i <dma>   : FLX-device DMA controller for receiving (default: 0).\n"
    "  -I <i2c>   : GBTX/lpGBT I2C address (hex).\n"
    "  -m <master>: I2C Master index (only with -s; 0,1 or 2; default:0).\n"
    "  -o         : When reading or writing all (consecutive) registers, do it one-by-one\n"
    "               (default: single multi-register read/write operation when possible).\n"
    "  -R         : Receive replies on any E-link.\n"
    "  -s <i2c>   : I2C address (hex) of a secondary lpGBT accessed via an lpGBT\n"
    "               I2C Master (NB: feature available for lpGBT v1 only, for now).\n"
    "  -t         : Display one register value per line in output\n"
    "               (i.e. 'TXT'-format like).\n"
    "  -T         : Display one address + register value per line in output\n"
    "               (i.e. similar to 'TXT'-format, but different..).\n"
    "  -Z         : Do NOT receive and process/display replies.\n"
    " <byte>      : Byte value (decimal or hex, 0x.. or x..) to write to\n"
    "               a GBTX/lpGBT register (option -a).\n"
    " <filename>  : Name of text file with GBTX/lpGBT (hex) register data to compare against,\n"
    "               or to write to consecutive registers (if one value per line;\n"
    "               also accepts files with address+value (both hex) per line,\n"
    "               separated by a space, in which case registers are written one-by-one).\n"
    "=> Examples:\n"
    "Read all registers of GBTX/lpGBT (I2C address 0x71)\n"
    "connected to FLX-device GBT link 3:\n"
    "  fice -G3 -I71\n"
    "Read GBTX/lpGBT register 32 (0x20):\n"
    "  fice -G3 -I71 -a 32 (or: fice -G 1 -I 3 -a 0x20)\n"
    "Write 0xA5 to GBTX/lpGBT register 32 (0x20):\n"
    "  fice -G3 -I71 -a 32 0xA5\n"
    "Write contents of GBT-conf.txt to GBTX/lpGBT registers:\n"
    "  fice -G3 -I71 GBT-conf.txt\n"
    "Compare contents of GBT-conf.txt to GBTX/lpGBT registers:\n"
    "  fice -G3 -I71 -C GBT-conf.txt\n"
    "Read all registers of a secondary lpGBT (I2C address 0x72)\n"
    "connected to the EC-link of an lpGBT connected to FLX-device GBT link 3:\n"
    "  fice -G3 -I72 -e\n"
    "Read all registers of a secondary lpGBT (I2C address 0x72)\n"
    "connected to I2C Master 1 (@400KHz) of an lpGBT (I2C address 0x71)\n"
    "connected to FLX-device GBT link 3:\n"
    "  fice -G3 -I71 -s72 -m1 -f400 \n";
}

// ----------------------------------------------------------------------------
