#include <iostream>
#include <iomanip>
using namespace std;
#include <unistd.h>

#include "felixtag.h"
#include "flxdefs.h"
#include "flxcard/FlxCard.h"
#include "flxcard/FlxException.h"
#include "arg.h"

// Version identifier: year, month, day, release number
const int   VERSION_ID = 0x23030300; // Settings are per device, not all via #0
//const int VERSION_ID = 0x22083100; // Extend RM5 instant time-out output
//const int VERSION_ID = 0x22071500; // RM5 support; -G optional;
//                                      more input checks
//const int VERSION_ID = 0x17121300; // Fix bug in TTC time-out counter set
//const int VERSION_ID = 0x17072600;

// ----------------------------------------------------------------------------

int main( int argc, char *argv[] )
{
  int  opt;
  int  devnr    = 0;
  int  lnknr     = -1;
  int  egroupnr  = -1;
  int  epathnr   = -1;
  int  elinknr   = -1;
  int  set       = -1;
  bool global    = false;
  int  global_to = -1;
  bool ttc       = false;
  int  ttc_to    = -1;
  string operation;

  // Parse the options
  while( (opt = getopt(argc, argv, "d:e:g:G:hp:TV")) != -1 )
    {
      switch( opt )
        {
        case 'd':
          if( sscanf( optarg, "%d", &devnr ) != 1 )
            arg_error( 'd' );
          break;
        case 'e':
          // E-link number
          unsigned int x;
          if( sscanf( optarg, "%x", &x ) != 1 )
            arg_error( 'e' );
          elinknr = x;
          if( elinknr < 0 || elinknr > FLX_MAX_ELINK_NR )
            arg_range( 'e', 0, FLX_MAX_ELINK_NR );
          break;
        case 'G':
          // (lp)GBT link number
          if( sscanf( optarg, "%d", &lnknr ) != 1 )
            arg_error( 'G' );
          if( lnknr < 0 || lnknr > FLX_LINKS-1 )
            arg_range( 'G', 0, FLX_LINKS-1 );
          break;
        case 'g':
          // E-group number
          if( sscanf( optarg, "%d", &egroupnr ) != 1 )
            arg_error( 'g' );
          if( egroupnr < 0 || egroupnr >= FLX_TOHOST_GROUPS-1 )
            arg_range( 'g', 0, FLX_TOHOST_GROUPS-1-1 );
          //if( egroupnr < 0 || egroupnr >= FLX_FROMHOST_GROUPS-1 )
          //  arg_range( 'g', 0, FLX_FROMHOST_GROUPS-1-1 );
          break;
        case 'h':
          usage();
          return 0;
        case 'p':
          // E-path number
          if( sscanf( optarg, "%d", &epathnr ) != 1 )
            arg_error( 'p' );
          if( epathnr < 0 || epathnr > FLX_ELINK_PATHS-1 )
            arg_range( 'p', 0, FLX_ELINK_PATHS-1 );
          break;
        case 'T':
          ttc = true;
          break;
        case 'V':
          cout << "Version " << hex << VERSION_ID << dec
               << " (tag: " << FELIX_TAG << ")" << endl;
          return 0;
        default: // '?'
          usage();
          return 1;
        }
    }

  // Check for either a valid -e or valid -G/g/p options
  if( elinknr == -1 && lnknr == -1 && egroupnr == -1 && epathnr == -1 )
    {
      // Going to read or configure TTC time-out or Global time-out
      if( !ttc ) global = true;
    }
  else if( (elinknr != -1 && (lnknr != -1 || egroupnr != -1 || epathnr != -1)) )
    {
      cout << "### Provide either -e or -G/g/p options" << endl;
      return 1;
    }
  else if( ttc )
    {
      cout << "(ignoring option -T)" << endl;
      ttc = false;
    }

  // String defining the operation to perform?
  if( optind < argc )
    {
      operation = string( argv[optind] );
      if( operation == string("set") )
        set = 1;
      else if( operation == string("reset") )
        set = 0;
      else
        {
          cout << "### Unknown qualifier (use: set|reset): "
               << operation << endl;
          usage();
          return 1;
        }

      ++optind;
      if( global || ttc )
        {
          if( optind < argc )
            {
              // Accept hex values "x12" and "0x12", as well as decimal "12"
              int result, i = 0;
              if( argv[optind][0] == 'x' || argv[optind][0] == 'X' )
                i = 1;
              else if( argv[optind][0] == '0' &&
                       (argv[optind][1] == 'x' || argv[optind][1] == 'X') )
                i = 2;
              if( i != 0 )
                {
                  unsigned int g;
                  result = sscanf( &argv[optind][i], "%x", &g );
                  if( result == 1 )
                    global_to = (int) g;
                }
              else
                {
                  result = sscanf( &argv[optind][i], "%d", &global_to );
                }
              if( result != 1 )
                {
                  cout << "### Invalid counter value" << endl;
                  return 1;
                }
            }
          if( ttc )
            ttc_to = global_to;
        }
      else
        {
          if( optind < argc )
            {
              cout << "### Warning: unexpected parameter \""
                   << argv[optind] << "\" (ignored)" << endl;
            }
        }
    }

  // Open FELIX FLX-device
  FlxCard *flx = new FlxCard;
  try {
    flx->card_open( devnr, LOCK_NONE );
  }
  catch( FlxException &ex ) {
    cout << "### FlxCard open: " << ex.what() << endl;
    return 1;
  }
  cout << "FLX-device " << devnr
       << ", firmw " << flx->firmware_string() << endl;

  bool lpgbt = flx->lpgbt_type();
  flxcard_bar2_regs_t *bar2 = (flxcard_bar2_regs_t *) flx->bar2Address();
  uint64_t chans = bar2->NUM_OF_CHANNELS;
  if( chans > FLX_LINKS ) chans = FLX_LINKS;

  if( global || ttc )
    {
      if( global )
        {
          // Read or configure the global time-out and counter
          if( global_to >= 0 )
            bar2->TIMEOUT_CTRL.TIMEOUT = global_to;

          if( set == 1 )
            bar2->TIMEOUT_CTRL.ENABLE = 1;
          else if( set == 0 )
            bar2->TIMEOUT_CTRL.ENABLE = 0;

          cout << "Global Timeout: "
               << (bar2->TIMEOUT_CTRL.ENABLE == 1 ? "ON" : "OFF")
               << ", counter=" << bar2->TIMEOUT_CTRL.TIMEOUT << endl;
        }
      else
        {
          // Read or configure the TTC time-out and counter
#if REGMAP_VERSION < 0x500
          if( ttc_to >= 0 )
            bar2->CR_TTC_TOHOST.TIMEOUT_VALUE = global_to;

          if( set == 1 )
            bar2->CR_TTC_TOHOST.TIMEOUT_ENABLE = 1;
          else if( set == 0 )
            bar2->CR_TTC_TOHOST.TIMEOUT_ENABLE = 0;

          cout << "TTC Timeout: " << bar2->CR_TTC_TOHOST.TIMEOUT_ENABLE
               << ", counter=" << bar2->CR_TTC_TOHOST.TIMEOUT_VALUE << endl;
#else
          cout << "### TTC Timeout not implemented (for RM5)" << endl;
          ttc_to = ttc_to;
#endif // REGMAP_VERSION
        }

      if( ttc
#if REGMAP_VERSION < 0x500
          || global
#endif // REGMAP_VERSION
          )
        {
          // Close the FLX-device
          try {
            flx->card_close();
          }
          catch( FlxException &ex ) {
            cout << "FlxCard close: " << ex.what() << endl;
          }
          return 0;
        }
      set = -1; // Global setting done: below just display instant time-out
    }

  // Single E-link number given ?
  if( elinknr != -1 )
    {
      // Extract link, e-group and e-path numbers from the given e-link number
      if( lpgbt )
        {
          lnknr    = (elinknr & BLOCK_LNK_MASK) >> BLOCK_LNK_SHIFT;
          egroupnr = ((elinknr & BLOCK_EGROUP_MASK_LPGBT) >>
                      BLOCK_EGROUP_SHIFT_LPGBT);
          epathnr  = (elinknr & BLOCK_EPATH_MASK_LPGBT);
        }
      else
        {
          lnknr    = (elinknr & BLOCK_LNK_MASK) >> BLOCK_LNK_SHIFT;
          egroupnr = (elinknr & BLOCK_EGROUP_MASK) >> BLOCK_EGROUP_SHIFT;
          epathnr  = (elinknr & BLOCK_EPATH_MASK);
        }
    }

#if REGMAP_VERSION >= 0x500
  uint64_t nstreams  = bar2->AXI_STREAMS_TOHOST.NUMBER_OF_STREAMS;
#endif // REGMAP_VERSION
  bool config_ec = false, config_ic = false;

  int max_lnk_cnt;
  if( lnknr == -1 )
    {
      lnknr = 0;
      // Covering *all* links
      max_lnk_cnt = chans;
    }
  else if( lnknr >= 0 && lnknr < (int) chans )
    {
      max_lnk_cnt = lnknr + 1;
    }
  else
    {
      cout << "### Invalid Link number " << lnknr
           << " (valid=[0.." << chans-1
           << "]) for this FLX-device" << endl;
      flx->card_close();
      return 1;
    }

  int max_egroup_cnt;
  if( egroupnr == -1 )
    {
      egroupnr = 0;
      if( lpgbt )
        max_egroup_cnt = 7;
      else
        max_egroup_cnt = 5;

      if( epathnr == -1 )
        {
          // Do complete link, so include EC and IC
          config_ec = true;
          config_ic = true;
        }
    }
  else
    {
      if( lpgbt && egroupnr < 7 )
        {
          max_egroup_cnt = egroupnr + 1;
        }
      else if( !lpgbt && egroupnr < 5 )
        {
          max_egroup_cnt = egroupnr + 1;
        }
      else
#if REGMAP_VERSION >= 0x500
        if( !(elinknr != -1 && (elinknr & ~BLOCK_LNK_MASK) >= (int) nstreams) ) 
          {
            // Accept stream indices not associated with an E-group (EC,IC)
            max_egroup_cnt = egroupnr + 1;
          }
        else
#endif // REGMAP_VERSION
          {
            cout << "### Invalid Egroup number " << egroupnr
                 << " (valid=[0.." << (lpgbt ? 6 : 4)
                 << "]) for this FLX-device" << endl;
            flx->card_close();
            return 1;
          }
    }

  int max_epath_cnt;
  if( epathnr == -1 )
    {
      epathnr = 0;
      if( lpgbt )
        max_epath_cnt = 4;
      else
        max_epath_cnt = 8;
    }
  else
    {
      if( lpgbt && epathnr < 4 )
        {
          max_epath_cnt = epathnr + 1;
        }
      else if( !lpgbt && epathnr < 8 )
        {
          max_epath_cnt = epathnr + 1;
        }
      else
        {
          cout << "### Invalid Epath number " << epathnr
               << " (valid=[0.." << (lpgbt ? 3 : 7)
               << "]) for this FLX-device" << endl;
          flx->card_close();
          return 1;
        }
    }

#if REGMAP_VERSION < 0x500
  uint64_t enables;
  for( int lnk=lnknr; lnk<max_lnk_cnt; ++lnk )
    for( int grp=egroupnr; grp<max_egroup_cnt; ++grp )
      {
        // Time-out is a ToHost setting (on both devices of a card)
        flxcard_cr_tohost_egroup_ctrl_t *c =
          &bar2->CR_GBT_CTRL[lnk].EGROUP_TOHOST[grp].TOHOST;
        enables = c->INSTANT_TIMEOUT_ENA;

        for( int path=epathnr; path<max_epath_cnt; ++path )
          {
            if( set == 1 )
              {
                enables |= ((uint64_t) 1 << path);
                c->INSTANT_TIMEOUT_ENA = enables;
              }
            else if( set == 0 )
              {
                enables &= ~((uint64_t) 1 << path);
                c->INSTANT_TIMEOUT_ENA = enables;
              }

            cout << "Dev #" << devnr
                 << " Link " << setw(2) << lnk
                 << " egroup " << grp << " epath " << path << ": ";
            if( enables & ((uint64_t) 1 << path) )
              cout << "ENABLED";
            else
              cout << "disabled";
            cout << endl;
          }
      }
  if( config_ec || config_ic )
    { } // Not used for RM4
#else
  cout << setfill( '0' ) << uppercase;
  uint32_t index;
  uint64_t enables = 0;
  flxcard_crtohost_instant_timeout_ena_gen_t *t;
  cout << "Streams-per-link: " << nstreams << " (ToHost)" << endl;
  for( int lnk=lnknr; lnk<max_lnk_cnt; ++lnk )
    {
      // Time-out is a ToHost setting (on device #0 of a card)
      t = &bar2->CRTOHOST_INSTANT_TIMEOUT_ENA_GEN[lnk];
      enables = t->CRTOHOST_INSTANT_TIMEOUT_ENA;

      for( int grp=egroupnr; grp<max_egroup_cnt; ++grp )
        {
          // Stream start index
          if( lpgbt )
            index = grp * 4 + epathnr;
          else
            index = grp * 8 + epathnr;

          for( int path=epathnr; path<max_epath_cnt; ++path, ++index )
            {
              if( set == 1 )
                enables |= ((uint64_t) 1 << index);
              else if( set == 0 )
                enables &= ~((uint64_t) 1 << index);
            }
        }

      // EC and IC
      if( config_ec )
        {
          index = bar2->AXI_STREAMS_TOHOST.EC_INDEX;
          if( set == 1 )
            enables |= ((uint64_t) 1 << index);
          else if( set == 0 )
            enables &= ~((uint64_t) 1 << index);
        }
      if( config_ic )
        {
          index = bar2->AXI_STREAMS_TOHOST.IC_INDEX;
          if( set == 1 )
            enables |= ((uint64_t) 1 << index);
          else if( set == 0 )
            enables &= ~((uint64_t) 1 << index);
        }

      if( set == 1 || set == 0 )
        t->CRTOHOST_INSTANT_TIMEOUT_ENA = enables;

      cout << "Dev #" << devnr << " Link " << setw(2) << lnk
           << ": TOHOST_INSTANT_TIMEOUT_ENA="
           << hex << setw((nstreams+3)/4) << enables << dec << endl;

      // Handling more than 1 link
      if( max_lnk_cnt-lnknr != 1 )
        continue;

      // List the setting per E-link when displaying (part of) a single link
      cout << "E-link G-g-p TimeOut" << endl << setfill('0');
      for( int grp=egroupnr; grp<max_egroup_cnt; ++grp )
        {
          // Stream start index
          if( lpgbt )
            index = grp * 4 + epathnr;
          else
            index = grp * 8 + epathnr;

          for( int path=epathnr; path<max_epath_cnt; ++path, ++index )
            {
              if( lpgbt )
                elinknr = ((lnk  << BLOCK_LNK_SHIFT) |
                           (grp  << BLOCK_EGROUP_SHIFT_LPGBT) | path);
              else
                elinknr = ((lnk  << BLOCK_LNK_SHIFT) |
                           (grp  << BLOCK_EGROUP_SHIFT) | path);
              uint64_t ito = (enables & ((uint64_t)1 << index)) >> index;
              cout << "0x" << setfill('0') << setw(3) << hex << elinknr
                   << " " << dec << setfill(' ') << setw(2) << lnk << "-"
                   << grp << "-" << path << "  " << (ito != 0 ? '1' : '-')
                   << endl;
            }
        }
      // The rest... (only when displaying the complete link)
      if( max_egroup_cnt-egroupnr > 1 && max_epath_cnt-epathnr > 1 )
        {
          uint32_t ec_index = bar2->AXI_STREAMS_TOHOST.EC_INDEX;
          uint32_t ic_index = bar2->AXI_STREAMS_TOHOST.IC_INDEX;
          for( ; index<nstreams; ++index )
            {
              elinknr = ((lnk << BLOCK_LNK_SHIFT) | index);
              uint64_t ito = (enables & ((uint64_t)1 << index)) >> index;
              cout << "0x" << setfill('0') << setw(3) << hex << elinknr;
              if( index == ec_index )
                cout << "  EC     ";
              else if( index == ic_index )
                cout << "  IC     ";
              else
                cout << "         ";
              cout << (ito != 0 ? '1' : '-') << endl;
            }
        }
    }
#endif // REGMAP_VERSION

  // Close the FLX-device
  try {
    flx->card_close();
  }
  catch( FlxException &ex ) {
    cout << "FlxCard close: " << ex.what() << endl;
  }

  return 0;
}

// ----------------------------------------------------------------------------

void usage()
{
  cout << "feto version " << hex << VERSION_ID << dec << endl <<
    "Enable, disable or display either the instant time-out setting,\n"
    "a setting per e-path (e-link), or the so-called global time-out\n"
    "and associated time-out counter (number of clocks until time-out),\n"
    "or the TTC time-out and associated counter (RM4 only).\n"
    "Without keyword '(re)set' the current setting of the requested\n"
    "(group of) time-outs is displayed (one complete link at most).\n"
    "\n"
    "Usage: feto [-h|V] [-d <devnr>] [-e <elink>] "
    "[-G <lnk> [-g <group>] [-p <path>]]\n"
    "            [-T] [set|reset] [<globcntr>]\n"
    "  -h         : Show this help text.\n"
    "  -V         : Show version.\n"
    "  -d <devnr> : FLX-device to use (default: 0).\n"
    "  -e <elink> : E-link number (hex) or use -G/g/p options.\n"
    "  -G <lnk>   : (lp)GBT-link number.\n"
    "  -g <group> : Group number (default: all groups).\n"
    "  -p <path>  : E-path number (default: all paths).\n"
    "  -T         : Read or configure TTC time-out (RM4 only).\n"
    "  set        : Enable time-out.\n"
    "  reset      : Disable time-out.\n"
    "  <globcntr> : Global or TTC time-out counter value to set.\n"
    "Examples:\n"
    "> feto set        (Enable global time-out)\n"
    "> feto reset 1000 (Disable global time-out, set counter to 1000)\n"
    "> feto -G1 set    (Enable instant time-out on all E-links of link 1)\n"
#if REGMAP_VERSION >= 0x500
    "> feto            (Show global time-out and counter and "
    "instant time-out registers)\n";
#else
    "> feto            (Show global time-out and counter)\n"
    "> feto -T         (Show TTC time-out and counter)\n";
#endif // REGMAP_VERSION
}

// ----------------------------------------------------------------------------
