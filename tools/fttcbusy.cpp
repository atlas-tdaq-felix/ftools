#include <iostream>
#include <iomanip>
using namespace std;
#include <unistd.h>

#include "felixtag.h"
#include "flxdefs.h"
#include "flxcard/FlxCard.h"
#include "flxcard/FlxException.h"
#include "arg.h"

// Version identifier: year, month, day, release number
const int   VERSION_ID = 0x24129000; // Remove option -L: moved to TTC
//const int VERSION_ID = 0x23083000; // Added option -L (LTITTC E-link BUSY)
//const int VERSION_ID = 0x22071800; // Improved input check and output
//const int VERSION_ID = 0x22063000; // Added options -e,E,n,N
                                     // (E-link BUSY enable set/reset)
//const int VERSION_ID = 0x22051700; // Added options -A (debug)
                                     // -D -F (DMA/FIFO BUSY sources)
//const int VERSION_ID = 0x22030200; // Added option -Y/y (FIFO thresholds)
//const int VERSION_ID = 0x22013100; // Some items Endpoint 0 only
//const int VERSION_ID = 0x20101300; // Added option -X/x (DMA thresholds)
                                     // and invert meaning of -T
//const int VERSION_ID = 0x20100700; // Added option -R (reset TTC decoder)
                                     // and -B (B-channel disable)
//const int VERSION_ID = 0x20093000; // BUSY-by-DMA thresholds displayed
//const int VERSION_ID = 0x20062600; // BUSY-by-OUTPUT-FIFO info
//const int VERSION_ID = 0x20061700; // BUSY-output info and config options
//const int VERSION_ID = 0x18013000;

/* Excerpt from JIRA FLX-1795 on BUSY:
   items per endpoint, or global ('card-wide') via endpoint 0 only:

   BUSY related registers that are configured per endpoint
   (in both endpoints):

   * BUSY_MAIN_OUTPUT_FIFO_THRESH.LOW
   * DMA_BUSY_STATUS_CLEAR_LATCH
   * DMA_BUSY_STATUS_TOHOST_BUSY_LATCHED
   * DMA_BUSY_STATUS_TOHOST_BUSY
   * BUSY_MAIN_OUTPUT_FIFO_THRESH_BUSY_ENABLE
   * BUSY_MAIN_OUTPUT_FIFO_THRESH_LOW
   * BUSY_MAIN_OUTPUT_FIFO_THRESH_HIGH
   * BUSY_MAIN_OUTPUT_FIFO_STATUS_CLEAR_LATCHED
   * BUSY_MAIN_OUTPUT_FIFO_STATUS_HIGH_THRESH_CROSSED_LATCHED
   * BUSY_MAIN_OUTPUT_FIFO_STATUS_HIGH_THRESH_CROSSED
   * BUSY_MAIN_OUTPUT_FIFO_STATUS_LOW_THRESH_CROSSED
   * All the addresses and status regarding DMA BUSY in BAR0

   BUSY related registers that are configured only through endpoint 0
   and are global for the card:

   * TTC_BUSY_CLEAR
   * TTC_DEC_CTRL_BUSY_OUTPUT_INHIBIT
   * TTC_DEC_CTRL_MASTER_BUSY
   * DMA_BUSY_STATUS_ENABLE
   * TTC_BUSY_ACCEPTED00 - TTC_BUSY_ACCEPTED23
   * TTC_DEC_CTRL_BUSY_OUTPUT_STATUS
   * ELINK_BUSY_ENABLE00,01,02,...
   * TTC_BUSY_TIMING_...
*/

// ----------------------------------------------------------------------------

int main( int argc, char *argv[] )
{
  int  opt;
  int  cardnr        = 0;
  int  gbtnr         = -1;
  int  width         = -1;
  int  prescale      = -1;
  int  limit         = -1;
  bool clear         = false;
  bool debug         = false;
  bool skip_ttc      = true;
  bool reset         = false;
  // BUSY output
  int  master        = -1;
  int  inhibit       = -1;
  int  busy_dma_ena  = -1;
  int  busy_fifo_ena = -1;
  int  bchan_ena     = -1;
  int  thresh_on     = -1; // BUSY-by-DMA assert value
  int  thresh_diff   = -1; // BUSY-by-DMA deassert difference with assert value
  int  thresh_f_on   = -1; // BUSY-by-FIFO assert value
  int  thresh_f_diff = -1; // BUSY-by-FIFO deassert difference with assert value
  bool do_setting    = false;
  // E-link BUSY enable
  int  elink_busy_set   = -1;
  int  elink_busy_reset = -1;
  int  link_busy_set    = -1;
  int  link_busy_reset  = -1;

  // Parse the options
  unsigned int x;
  while( (opt = getopt(argc, argv, "Ab:B:Cd:e:E:D:F:G:"
                       "hi:l:m:n:N:p:RTVw:X:x:Y:y:")) != -1 )
    {
      switch( opt )
        {
        case 'A':
          debug = true;
          break;
        case 'b':
          // Enable/disable BUSY sources (DMA and FIFO)
          if( sscanf( optarg, "%d", &busy_dma_ena ) != 1 )
            arg_error( 'b' );
          if( busy_dma_ena < 0 || busy_dma_ena > 1 )
            arg_range( 'b', 0, 1 );
          busy_fifo_ena = busy_dma_ena;
          do_setting = true;
          break;
        case 'B':
          // Enable/disable B-channel (limits L1A rate to ca. 200KHz)
          if( sscanf( optarg, "%d", &bchan_ena ) != 1 )
            arg_error( 'B' );
          if( bchan_ena < 0 || bchan_ena > 1 )
            arg_range( 'B', 0, 1 );
          do_setting = true;
          break;
        case 'C':
          clear = true;
          break;
        case 'd':
          if( sscanf( optarg, "%d", &cardnr ) != 1 )
            arg_error( 'd' );
          break;
        case 'D':
          // Enable/disable BUSY source (DMA)
          if( sscanf( optarg, "%d", &busy_dma_ena ) != 1 )
            arg_error( 'D' );
          if( busy_dma_ena < 0 || busy_dma_ena > 1 )
            arg_range( 'D', 0, 1 );
          do_setting = true;
          break;
        case 'e':
          // Elink number for which to set BUSY enable
          if( sscanf( optarg, "%x", &x ) != 1 )
            arg_error( 'e' );
          elink_busy_set = x;
          // 2x12 links (for 2 devices)
          if( elink_busy_set < 0 || elink_busy_set > (24<<BLOCK_LNK_SHIFT)-1 )
            arg_range( 'e', 0, (24<<BLOCK_LNK_SHIFT)-1 );
          break;
        case 'E':
          // Link number for which to set BUSY enables
          if( sscanf( optarg, "%d", &link_busy_set ) != 1 )
            arg_error( 'E' );
          // 2x12 links (for 2 devices)
          if( link_busy_set < 0 || link_busy_set > 23 )
            arg_range( 'E', 0, 23 );
          break;
        case 'F':
          // Enable/disable BUSY source (FIFO)
          if( sscanf( optarg, "%d", &busy_fifo_ena ) != 1 )
            arg_error( 'F' );
          if( busy_fifo_ena < 0 || busy_fifo_ena > 1 )
            arg_range( 'F', 0, 1 );
          do_setting = true;
          break;
        case 'G':
          // GBT link number
          if( sscanf( optarg, "%d", &gbtnr ) != 1 )
            arg_error( 'G' );
          if( gbtnr < 0 || gbtnr > 23 )
            arg_range( 'G', 0, 23 );
          skip_ttc = false; // Option -T is made implicit
          break;
        case 'h':
          usage();
          return 0;
        case 'i':
          // Set/clear BUSY inhibit
          if( sscanf( optarg, "%d", &inhibit ) != 1 )
            arg_error( 'i' );
          if( inhibit < 0 || inhibit > 1 )
            arg_range( 'i', 0, 1 );
          do_setting = true;
          break;
        case 'l':
          // Minimum required BUSY width
          if( sscanf( optarg, "%d", &limit ) != 1 )
            arg_error( 'l' );
          if( limit < 0 || limit > 65535 ) // 16 bits
            arg_range( 'l', 0, 0xFFFF );
          do_setting = true;
          break;
        case 'm':
          // Set/clear Master BUSY
          if( sscanf( optarg, "%d", &master ) != 1 )
            arg_error( 'm' );
          //if( master < 0 || master > 1 )
          //  arg_range( 'm', 0, 1 );
          // For Master BUSY pulse test, allow larger numbers:
          if( master < 0 )
            arg_range( 'm', 0, 0x7FFFFFFF );
          do_setting = true;
          break;
        case 'n':
          // Elink number for which to reset BUSY enable
          if( sscanf( optarg, "%x", &x ) != 1 )
            arg_error( 'n' );
          elink_busy_reset = x;
          // 2x12 links (for 2 devices)
          if( elink_busy_reset < 0 || elink_busy_reset>(24<<BLOCK_LNK_SHIFT)-1 )
            arg_range( 'n', 0, (24<<BLOCK_LNK_SHIFT)-1 );
          break;
        case 'N':
          // Link number for which to reset BUSY enables
          if( sscanf( optarg, "%d", &link_busy_reset ) != 1 )
            arg_error( 'N' );
          // 2x12 links (for 2 devices)
          if( link_busy_reset < 0 || link_busy_reset > 23 )
            arg_range( 'N', 0, 23 );
          break;
        case 'p':
          // 40MHz clock prescale
          if( sscanf( optarg, "%d", &prescale ) != 1 )
            arg_error( 'p' );
          if( prescale < 0 || prescale > 1048575 ) // 20 bits
            arg_range( 'p', 0, 0xFFFFF );
          do_setting = true;
          break;
        case 'R':
          reset = true;
          do_setting = true;
          break;
        case 'T':
          skip_ttc = false;
          break;
        case 'V':
          cout << "Version " << hex << VERSION_ID << dec
               << " (tag: " << FELIX_TAG << ")" << endl;
          return 0;
        case 'w':
          // Minimum asserted BUSY width
          if( sscanf( optarg, "%d", &width ) != 1 )
            arg_error( 'w' );
          if( width < 0 || width > 65535 ) // 16 bits
            arg_range( 'w', 0, 0xFFFF );
          do_setting = true;
          break;
        case 'X':
          // DMA BUSY assert value (in MB)
          if( sscanf( optarg, "%d", &thresh_on ) != 1 )
            arg_error( 'X' );
          if( thresh_on < 0 || thresh_on > 4096 )
            arg_range( 'X', 0, 4096 );
          do_setting = true;
          break;
        case 'x':
          // DMA BUSY difference assert/deassert value (in MB)
          if( sscanf( optarg, "%d", &thresh_diff ) != 1 )
            arg_error( 'x' );
          if( thresh_diff < 0 || thresh_diff > 4096 )
            arg_range( 'x', 0, 4096 );
          do_setting = true;
          break;
        case 'Y':
          {
            // FIFO BUSY assert value (in units of ...?)
            uint32_t val;
            if( sscanf( optarg, "%x", &val ) != 1 )
              arg_error( 'Y' );
            thresh_f_on = val;
            if( thresh_f_on < 0 || thresh_f_on > 4096 )
              arg_range( 'Y', 0, 4096 );
            do_setting = true;
            break;
          }
        case 'y':
          {
            // FIFO BUSY difference assert/deassert value (in units of ...?)
            uint32_t val;
            if( sscanf( optarg, "%x", &val ) != 1 )
              arg_error( 'y' );
            thresh_f_diff = val;
            if( thresh_f_diff < 0 || thresh_f_diff > 4096 )
              arg_range( 'y', 0, 4096 );
            do_setting = true;
            break;
          }
        default: // '?'
          usage();
          return 1;
        }
    }

  // If just displaying the settings never mind the firmware version
  bool ignore_version = (!do_setting && clear == false &&
                         elink_busy_set == -1 && elink_busy_reset == -1 &&
                         link_busy_set == -1 && link_busy_reset == -1);

  // Open FELIX FLX-device
  FlxCard *flx = new FlxCard;
  try {
    flx->card_open( cardnr, LOCK_NONE, false, ignore_version );
  }
  catch( FlxException &ex ) {
    cout << "### FlxCard open: " << ex.what() << endl;
    return 1;
  }

  flxcard_bar2_regs_t *bar2 = (flxcard_bar2_regs_t *) flx->bar2Address();
  uint64_t *bar0 = (uint64_t *) flx->bar0Address();

  uint64_t chans = bar2->NUM_OF_CHANNELS;
  if( chans > FLX_LINKS ) chans = FLX_LINKS;
  uint64_t endpoints = bar2->NUMBER_OF_PCIE_ENDPOINTS;

  int max_gbtnr;
  if( gbtnr == -1 )
    {
      gbtnr = 0;
      // Used for global settings via endpoint 0, so covers *all* links
      max_gbtnr = chans * endpoints;
    }
  else if( gbtnr >= 0 && gbtnr < (int) (chans*endpoints) )
    {
      max_gbtnr = gbtnr + 1;
    }
  else
    {
      cout << "### -G: invalid link number [0.." << chans*endpoints-1
           << "] for this FLX-card" << endl;
      flx->card_close();
      return 1;
    }

   if( elink_busy_set != -1 &&
       ((elink_busy_set & BLOCK_LNK_MASK)>>BLOCK_LNK_SHIFT) >= (int) chans*endpoints )
     {
      cout << "### -e: invalid link number [0.." << chans*endpoints-1
           << "] in e-link number for this FLX-card" << endl;
      flx->card_close();
      return 1;
     }

   if( elink_busy_reset != -1 &&
       ((elink_busy_reset & BLOCK_LNK_MASK)>>BLOCK_LNK_SHIFT) >= (int) chans*endpoints )
     {
      cout << "### -n: invalid link number [0.." << chans*endpoints-1
           << "] in e-link number for this FLX-card" << endl;
      flx->card_close();
      return 1;
     }

  if( link_busy_set != -1 && link_busy_set >= (int) (chans*endpoints) )
    {
      cout << "### -E: invalid link number [0.." << chans*endpoints-1
           << "] for this FLX-card" << endl;
      flx->card_close();
      return 1;
    }

  if( link_busy_reset != -1 && link_busy_reset >= (int) (chans*endpoints) )
    {
      cout << "### -N: invalid link number [0.." << chans*endpoints-1
           << "] for this FLX-card" << endl;
      flx->card_close();
      return 1;
    }

  // ###TEST: byte-aligned multi-byte bitfields writing overwrites other bytes
  /*
  if( debug )
    {
      cout << " PACKAGE_COUNT = " << bar2->GEN_FM_CONTROL1.PACKAGE_COUNT;
      if( limit != -1 )
        {
          bar2->GEN_FM_CONTROL1.PACKAGE_COUNT = limit;
          //flx->cfg_set_option( BF_GEN_FM_CONTROL1_PACKAGE_COUNT, limit );
          cout << " (set to: " << limit << ") ";
        }
      cout << " PACKAGE_LENGTH = " << bar2->GEN_FM_CONTROL1.PACKAGE_LENGTH;
      if( width != -1 )
        {
          bar2->GEN_FM_CONTROL1.PACKAGE_LENGTH = width;
          //flx->cfg_set_option( BF_GEN_FM_CONTROL1_PACKAGE_LENGTH, width );
          cout << " (set to: " << width << ") ";
        }
      cout << endl;
    }
  */
  /*
  // ###TEST
  typedef struct test_it
  {
    uint32_t part1 : 16;
    uint32_t part2 : 16;
    uint32_t part3 : 16;
    uint32_t part4 : 16;
  } test_it_t;
  test_it_t tmp = { 12345,12345,12345,12345 };
  if( limit != -1 )
    {
      tmp.part1 = limit;
      cout << " (tmp1 set to: " << limit << ") ";
    }
  if( width != -1 )
    {
      tmp.part2 = width;
      cout << " (tmp2 set to: " << limit << ") ";
    }
  cout << tmp.part1 << " " << tmp.part2 << " "
       << tmp.part3 << " " << tmp.part4;
  cout << endl;
  */

  if( elink_busy_set != -1 )
    {
      int lnk = (elink_busy_set & BLOCK_LNK_MASK) >> BLOCK_LNK_SHIFT;
      uint64_t bsy_ena = bar2->ELINK_BUSY_ENABLE[lnk].ELINK_BUSY_ENABLE;
      bsy_ena |= ((uint64_t) 1 << (elink_busy_set & 0x3F));
      bar2->ELINK_BUSY_ENABLE[lnk].ELINK_BUSY_ENABLE = bsy_ena;
      cout << "=> BUSY_ENABLE elink " << hex << setfill('0') << setw(3)
           << elink_busy_set << dec
           << " (link=" << lnk << ",index=" << (elink_busy_set & 0x3F)
           << "): *set*" << endl;
    }

  if( elink_busy_reset != -1 )
    {
      int lnk = (elink_busy_reset & BLOCK_LNK_MASK) >> BLOCK_LNK_SHIFT;
      uint64_t bsy_ena = bar2->ELINK_BUSY_ENABLE[lnk].ELINK_BUSY_ENABLE;
      bsy_ena &= ~((uint64_t) 1 << (elink_busy_reset & 0x3F));
      bar2->ELINK_BUSY_ENABLE[lnk].ELINK_BUSY_ENABLE = bsy_ena;
      cout << "=> BUSY_ENABLE elink " << hex << setfill('0') << setw(3)
           << elink_busy_reset << dec
           << " (link=" << lnk << ",index=" << (elink_busy_reset & 0x3F)
           << dec << "): *reset*" << endl;
    }

  if( link_busy_set != -1 )
    {
      int lnk = link_busy_set;
      bar2->ELINK_BUSY_ENABLE[lnk].ELINK_BUSY_ENABLE = 0xFFFFFFFFFFFFFFFF;
      cout << "=> BUSY_ENABLE link " << link_busy_set << ": *set*" << endl;
    }

  if( link_busy_reset != -1 )
    {
      int lnk = link_busy_reset;
      bar2->ELINK_BUSY_ENABLE[lnk].ELINK_BUSY_ENABLE = 0;
      cout << "=> BUSY_ENABLE link " << link_busy_reset << ": *reset*" << endl;
    }

  cout << setfill( '0' ) << uppercase;
  if( !debug && !skip_ttc )
    {
      // Display E-link BUSY-accepted and BUSY-enable registers and E-links
      uint64_t bsy_acc, bsy_ena;
      bool lpgbt = flx->lpgbt_type();
      cout << "*E-link ";
      cout << "(LTI)TTC-BUSY status (latched BUSY requests and "
           << "enables for BUSY output):" << endl;

      int devnr, devgbt;
      for( int gbt=gbtnr; gbt<max_gbtnr; ++gbt )
        {
          bsy_acc = bar2->TTC_BUSY_ACCEPTED_G[gbt].TTC_BUSY_ACCEPTED;
          bsy_ena = bar2->ELINK_BUSY_ENABLE[gbt].ELINK_BUSY_ENABLE;
          devnr = gbt/chans;
          devgbt = gbt - chans*(gbt/chans);
          cout << "Dev #" << devnr
               << " Link " << setw(2) << devgbt;
          if( endpoints > 1 )
            cout << " (" << setfill(' ') << setw(2) << gbt << setfill('0') << ")";
          cout << ": ";
          cout << "(LTI)TTC-BUSY=" << hex << setw((57+3)/4) << bsy_acc
               << "  BUSY-ENABLE="
               << hex << setw((57+3)/4) << bsy_ena
               << dec << endl;

          int elinknr, egroup, epath;
          uint64_t bsy_mask = 1;
          for( int i=0; i<57; ++i, bsy_mask<<=1 )
            if( bsy_acc & bsy_mask )
              {
                if( lpgbt )
                  {
                    egroup  = i/4;
                    epath   = (i & 3);
                    elinknr = ((devgbt << BLOCK_LNK_SHIFT) |
                               (egroup << BLOCK_EGROUP_SHIFT_LPGBT) | epath);
                  }
                else
                  {
                    egroup  = i/8;
                    epath   = (i & 7);
                    elinknr = ((devgbt << BLOCK_LNK_SHIFT) |
                               (egroup << BLOCK_EGROUP_SHIFT) | epath);
                  }
                cout << hex << "  Dev #" << devnr
                     << " E = " << setw(3) << elinknr << " = "
                     << dec << devgbt << "-" << egroup << "-" << epath;
                if( (bsy_ena & bsy_mask) == 0 )
                  cout << " (not enabled)";
                cout << endl;
              }
        }
    }

  // Optionally clear BUSY-accepted bits
  if( clear )
    {
      bar2->TTC_BUSY_CLEAR = 1;
      cout << "=> TTC-BUSYs CLEARED <=" << endl;
    }

  // If just making an E-link busy setting, leave it at that...
  if( (elink_busy_set != -1 || elink_busy_reset != -1 ||
      link_busy_set != -1 || link_busy_reset != -1) && !do_setting )
    {
      flx->card_close();
      return 1;
    }

  // Display and optionally set BUSY parameters
  // (NB: ### do NOT use the struct, because of byte-aligned bitfields
  //      and associated overwriting other bytes (within 32-bit word?;
  //      firmware fixed in the meantime?)
  if( limit != -1 || !skip_ttc )
    cout << "TTC-BUSY timing: "
         << "*Prescale = " << bar2->TTC_BUSY_TIMING_CTRL.PRESCALE;
  if( prescale != -1 )
    {
      if( debug )
        bar2->TTC_BUSY_TIMING_CTRL.PRESCALE = prescale;
      else
        flx->cfg_set_option( BF_TTC_BUSY_TIMING_CTRL_PRESCALE, prescale );
      cout << " (set to: " << prescale << ") ";
    }
  if( limit != -1 || !skip_ttc )
    cout << " *Width = " << bar2->TTC_BUSY_TIMING_CTRL.BUSY_WIDTH;
  if( width != -1 )
    {
      if( debug )
        bar2->TTC_BUSY_TIMING_CTRL.BUSY_WIDTH = width;
      else
        flx->cfg_set_option( BF_TTC_BUSY_TIMING_CTRL_BUSY_WIDTH, width );
      cout << " (set to: " << width << ") ";
    }
  if( limit != -1 || !skip_ttc )
    cout << " *Limit-time = " << bar2->TTC_BUSY_TIMING_CTRL.LIMIT_TIME;
  if( limit != -1 )
    {
      if( debug )
        bar2->TTC_BUSY_TIMING_CTRL.LIMIT_TIME = limit;
      else
        flx->cfg_set_option( BF_TTC_BUSY_TIMING_CTRL_LIMIT_TIME, limit );
      cout << " (set to: " << limit << ") ";
    }
  if( limit != -1 || !skip_ttc )
    cout << endl;

  // Enable or disable BUSY-by-DMA and BUSY-by-FIFO
  if( busy_dma_ena >= 0 )
    {
      bar2->DMA_BUSY_STATUS.ENABLE = busy_dma_ena;
      cout << "=> BUSY-by-DMA "
           << (busy_dma_ena ? "enabled" : "disabled") << " <=" << endl;
    }
  if( busy_fifo_ena >= 0 )
    {
      bar2->BUSY_MAIN_OUTPUT_FIFO_THRESH.BUSY_ENABLE = busy_fifo_ena;
      cout << "=> BUSY-by-FIFO "
           << (busy_fifo_ena ? "enabled" : "disabled") << " <=" << endl;
    }

  // BUSY-by-DMA
  cout << "BUSY-by-DMA  : *enabled=" << bar2->DMA_BUSY_STATUS.ENABLE << endl
       << "               ToHost=" << bar2->DMA_BUSY_STATUS.TOHOST_BUSY
       << " (BAR0:" << (bar0[0x490/8] & 0x1) << ")"
       << " (latched=" << bar2->DMA_BUSY_STATUS.TOHOST_BUSY_LATCHED
       << ")" << endl;
  if( thresh_on != -1 || thresh_diff != -1 )
    {
      // Configure thresholds
      if( thresh_on == -1 )
        thresh_on = bar0[0x470/8]/0x100000;

      if( thresh_diff == -1 )
        thresh_diff = (bar0[0x480/8] - bar0[0x470/8])/0x100000;

      // Assert threshold
      bar0[0x470/8] = thresh_on*0x100000;

      // Deassert threshold
      bar0[0x480/8] = (thresh_on + thresh_diff)*0x100000;

      cout << "               => Configured:" << endl;
    }
  cout << "               buffer free space: "
       << dec << "assert=" << bar0[0x470/8]/0x100000 << "MiB"
       << hex << " (" << bar0[0x470/8] << ")"
       << dec << " deassert=" << bar0[0x480/8]/0x100000 << "MiB"
       << hex << " (" << bar0[0x480/8] << ")"
       << dec << endl;
  if( clear && bar2->DMA_BUSY_STATUS.TOHOST_BUSY_LATCHED != 0 )
    {
      uint64_t c = bar2->DMA_BUSY_STATUS.ENABLE;
      bar2->DMA_BUSY_STATUS.ENABLE = c;
      cout << "=> CLEARED latched <=" << endl;
    }

  // BUSY-by-OUTPUT-FIFO
  cout << "BUSY-by-FIFO : enabled="
       << bar2->BUSY_MAIN_OUTPUT_FIFO_THRESH.BUSY_ENABLE << endl;
  if( thresh_f_on != -1 || thresh_f_diff != -1 )
    {
      // Configure thresholds
      if( thresh_f_on == -1 )
        thresh_on = bar2->BUSY_MAIN_OUTPUT_FIFO_THRESH.HIGH;

      if( thresh_f_diff == -1 )
        thresh_f_diff = (bar2->BUSY_MAIN_OUTPUT_FIFO_THRESH.HIGH -
                         bar2->BUSY_MAIN_OUTPUT_FIFO_THRESH.LOW);

      // Assert threshold
      bar2->BUSY_MAIN_OUTPUT_FIFO_THRESH.HIGH = thresh_f_on;

      // Deassert threshold
      bar2->BUSY_MAIN_OUTPUT_FIFO_THRESH.LOW = (thresh_f_on - thresh_f_diff);

      cout << "               => Configured:" << endl;
    }
  cout << hex << setw(3)
       << "               thresh: deassert="
       << bar2->BUSY_MAIN_OUTPUT_FIFO_THRESH.LOW
       << setw(3) << " assert="
       << bar2->BUSY_MAIN_OUTPUT_FIFO_THRESH.HIGH << endl
       << "               status: low_crossed="
       << bar2->BUSY_MAIN_OUTPUT_FIFO_STATUS.LOW_THRESH_CROSSED
       << " high_crossed="
       << bar2->BUSY_MAIN_OUTPUT_FIFO_STATUS.HIGH_THRESH_CROSSED
       << " (latched="
       << bar2->BUSY_MAIN_OUTPUT_FIFO_STATUS.HIGH_THRESH_CROSSED_LATCHED
       << ")" << dec << endl;
  if( clear &&
      bar2->BUSY_MAIN_OUTPUT_FIFO_STATUS.HIGH_THRESH_CROSSED_LATCHED != 0 )
    {
      uint64_t c = bar2->BUSY_MAIN_OUTPUT_FIFO_STATUS.HIGH_THRESH_CROSSED_LATCHED;
      bar2->BUSY_MAIN_OUTPUT_FIFO_STATUS.HIGH_THRESH_CROSSED_LATCHED = c;
      cout << "=> CLEARED latched <=" << endl;
    }

  // BUSY for FULLMODE
  cout << "BUSY FullMode: busy=" << hex << setw(6)
       << bar2->FM_BUSY_CHANNEL_STATUS.BUSY
       << " (latched=" << setw(6) << bar2->FM_BUSY_CHANNEL_STATUS.BUSY_LATCHED
       << ")" << endl;
  if( clear && bar2->FM_BUSY_CHANNEL_STATUS.BUSY_LATCHED != 0 )
    {
      uint64_t c = bar2->FM_BUSY_CHANNEL_STATUS.BUSY;
      bar2->FM_BUSY_CHANNEL_STATUS.BUSY = c;
      cout << "=> CLEARED latched <=" << endl;
    }

  // TTC B-channel / TriggerType
  cout << "TTC Bch/TType: *"
       << bar2->TTC_DEC_CTRL.TT_BCH_EN << endl;
  if( bchan_ena >= 0 )
    {
      bar2->TTC_DEC_CTRL.TT_BCH_EN = bchan_ena;
      cout << "=> Configured: "
           << "bchan-ena=" << bar2->TTC_DEC_CTRL.TT_BCH_EN << endl;
    }

  // Display BUSY output info and configure if required
  cout << "BUSY output  : *status=" << bar2->TTC_DEC_CTRL.BUSY_OUTPUT_STATUS
       << ", *inhibit=" << bar2->TTC_DEC_CTRL.BUSY_OUTPUT_INHIBIT
       << ", *master=" << bar2->TTC_DEC_CTRL.MASTER_BUSY
       << endl;
  if( master >= 0 || inhibit >= 0 )
    {
      if( master == 0 || master == 1 )
        {
          bar2->TTC_DEC_CTRL.MASTER_BUSY = master;
        }
      else
        {
          // Master BUSY pulse test: generate 0-1-0 'master' times
          usleep( 100000 ); // 0.1 s
          for( int i=0; i<master; ++i )
            {
              bar2->TTC_DEC_CTRL.MASTER_BUSY = 1;
              //usleep( 1000-100 ); // 1 ms
              usleep( 100-50 );
              bar2->TTC_DEC_CTRL.MASTER_BUSY = 0;
              //usleep( 500-100 ); // 0.5 ms
              usleep( 100-50 );
            }
        }
      if( inhibit >= 0 )
        bar2->TTC_DEC_CTRL.BUSY_OUTPUT_INHIBIT = inhibit;
      cout << "=> Configured: "
           << "status=" << bar2->TTC_DEC_CTRL.BUSY_OUTPUT_STATUS
           << ", inhibit=" << bar2->TTC_DEC_CTRL.BUSY_OUTPUT_INHIBIT
           << ", master=" << bar2->TTC_DEC_CTRL.MASTER_BUSY;
      cout << endl;
    }

  if( reset )
    {
      // Reset the TTC decoder
      bar2->TTC_DEC_CTRL.TOHOST_RST = 1;
      bar2->TTC_DEC_CTRL.TOHOST_RST = 0;

      cout << "=> TTC decoder reset: done <=" << endl;
    }

  cout << "(NB: items marked by * are global for the card, "
       << "access via endpoint 0 only!)\n";

  // Close the FLX-device
  try {
    flx->card_close();
  }
  catch( FlxException &ex ) {
    cout << "FlxCard close: " << ex.what() << endl;
  }

  return 0;
}

// ----------------------------------------------------------------------------

void usage()
{
  cout << "fttcbusy version " << hex << VERSION_ID << dec << endl <<
    "Displays BUSY-related settings and optionally E-link (LTI)TTC BUSY status "
    "and enables,\n"
    "optionally clearing (latched) E-link BUSY bits.\n"
    "With option -T the 'TTC BUSY accepted' register contents are displayed, "
    "as well as\n"
    "the corresponding E-link numbers, while option -C clears these registers "
    "after being displayed.\n"
    "Also the tool may be used to configure the TTC-BUSY signal settings "
    "(limit, prescale, width)\n"
    "and BUSY output settings (master, inhibit, B-channel, DMA and FIFO tresholds).\n"
    "Option -R resets the TTC decoder.\n"
    "NB: some of the settings are only read and written via FLX-card device #0\n"
    "    (in the output indicated by a '*').\n\n"
    "Usage: fttcbusy -h|V -d<devnr> -C -R -G<linknr>|-T\n"
    "                -e|n<elinknr> | -E|N<linknr>\n"
    "                -l<limit> -p<prescale> -w<width>\n"
    "                -m<b> -i<b> -b<b> -B<b>\n"
    "                -X<thresh> -x<diff> -Y<thresh> -y<diff>\n"
    "  -h           : Show this help text.\n"
    "  -V           : Show version.\n"
    "  -d <devnr>   : FLX-device number (default: 0).\n"
    "  -C           : Clear (latched) TTC-BUSY register bits.\n"
    "  -R           : Reset TTC decoder.\n"
    "  -G <linknr>  : GBT-link number, implies option -T.\n"
    "  -T           : Display per-(E)link (LTI)TTC-BUSY info (default: all links).\n"
    "  -e|n<elinknr>: Set/reset e-link BUSY-enable of selected e-link number (hex).\n"
    "  -E|N <linknr>: Set/reset e-link BUSY-enable for e-links of this link.\n"
    "  -l <limit>   : Set TTC BUSY limit time parameter (16-bit).\n"
    "  -p <prescale>: Set TTC BUSY prescale parameter (20-bit).\n"
    "  -w <width>   : Set TTC BUSY width parameter (16-bit).\n"
    "  -m <b>       : Set (1) or clear (0) Master BUSY.\n"
    "  -i <b>       : Set (1) or clear (0) BUSY Inhibit (= BUSY off).\n"
    "  -b <b>       : Enable(1) or disable(0) BUSY-by-DMA and BUSY-by-FIFO.\n"
    "  -D <b>       : Enable(1) or disable(0) BUSY-by-DMA.\n"
    "  -F <b>       : Enable(1) or disable(0) BUSY-by-FIFO.\n"
    "  -B <b>       : Enable(1) or disable(0) TTC B-channel/TriggerType.\n"
    "                 (NB: if set limits TTCtoHost rate to ca. 200KHz max)\n"
    "  -X <thresh>  : Set BUSY-by-DMA assert threshold (in MiB).\n"
    "  -x <diff>    : Set BUSY-by-DMA difference assert/deassert thresholds "
    "(in MiB).\n"
    "  -Y <thresh>  : Set BUSY-by-FIFO assert threshold (hex, max=0xFFF).\n"
    "  -y <diff>    : Set BUSY-by-FIFO difference assert/deassert thresholds "
    "(hex).\n";
}

// ----------------------------------------------------------------------------
