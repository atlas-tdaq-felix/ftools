#include <iostream>
#include <iomanip>
using namespace std;
#include <unistd.h>
#include <math.h>

#include "arg.h"
#include "felixtag.h"
#include "FlxUpload.h"
#include "FlxReceiver.h"
#include "FlxParser.h"
#include "lpgbt-regs-v0.h"
#include "lpgbt-regs-v1.h"
#include "ic.h"

// Version identifier: year, month, day, release number
const int VERSION_ID = 0x22020400;

uint8_t ds24_crc( uint8_t *data );

static int zeroReplies    = 0;
static int wrongReplies   = 0;
static int recvReplies    = 0;
static int icParityErrors = 0;

// ----------------------------------------------------------------------------

void getDs24Replies( FlxParser *parser, int linknr, int io,
                     bool lpgbt_v1, bool ec )
{
  int egroup = 7;
  int epath  = 6;
  if( ec )
    epath = 7;
  int elinknr = ((linknr << BLOCK_LNK_SHIFT) |
                 (egroup << BLOCK_EGROUP_SHIFT) | epath);

  uint8_t  iobit = 1 << (io & 0x7);
  uint32_t pioin_addr;
  if( io/8 == 1 )
    pioin_addr = (lpgbt_v1 ? PIOINH_V1 : PIOINH_V0);
  else
    pioin_addr = (lpgbt_v1 ? PIOINL_V1 : PIOINL_V0);

  cout << hex << setfill( '0' );

  uint32_t parity_start_index = (lpgbt_v1 ? 0 : 2);
  uint32_t offset = (lpgbt_v1 ? 0 : 1);

  bool     first_reply = true;
  uint32_t id_byte = 0;
  uint32_t id_bits = 0;
  static uint8_t IdBytes[8];

  uint8_t *chunkdata = 0;
  uint32_t size;
  parser->startRecvTime(); // Reset time-out on receiving (none or non-stop)
  while( parser->nextChunkRecvd( &chunkdata, &size, 10000, elinknr ) )
    {
      ++recvReplies;
      if( size == 0 ) ++zeroReplies; // Keep track of these unexpected ones

      // Keep track of unexpected reply sizes
      // (so any IC frame with more than 1 register value returned)
      if( size != offset+8 )
        ++wrongReplies;

      // IC reply parameters
      uint32_t len  = ((uint32_t) chunkdata[offset+2] +
                       ((uint32_t) chunkdata[offset+3]<<8));
      uint32_t addr = ((uint32_t) chunkdata[offset+4] +
                       ((uint32_t) chunkdata[offset+5]<<8));
      uint8_t  regval = chunkdata[offset+6];

      // Check IC frame parity word
      // (do not include zero-byte and I2C-address, in case of lpGBTv0)
      u8 parity = 0;
      for( uint32_t i=parity_start_index; i<offset+7; ++i )
        parity ^= chunkdata[i];
      if( chunkdata[offset+7] != parity )
        ++icParityErrors;

      // Extract the single ID bit from the messages
      if( addr == pioin_addr && len == 1 )
        {
          if( first_reply )
            {
              // Expect to read presence pulse (low)
              cout << "Presence signal: ";
              if( (regval & iobit) != 0 )
                cout << "NO";
              else
                cout << "YES";
              cout << endl;
              first_reply = false;
            }
          else
            {
              // ID bit: MSB first
              id_byte >>= 1;
              if( (regval & iobit) != 0 )
                id_byte |= 0x80;
              ++id_bits;

              // An ID byte completed ?
              if( (id_bits & 0x7) == 0 )
                {
                  // Display it
                  cout << setw(2) << id_byte << " ";
                  // Store it
                  IdBytes[id_bits/8-1] = id_byte;
                  // Reset for next ID byte
                  id_byte = 0;
                }
              if( id_bits == 64 )
                {
                  // ID should be complete (64 bits): check its CRC
                  if( IdBytes[7] != ds24_crc( IdBytes ) )
                    cout << " (###CRC 0x"
                         << (uint32_t) ds24_crc( IdBytes ) << ")";
                  //else
                  //  cout << " (CRC=OK)";
                  cout << endl;
                  id_bits = 0;
                }
            }
        }
    }
  cout << dec << setfill( ' ' );

  cout << "Replies received: " << recvReplies;
  if( zeroReplies > 0 )
    cout << " (Zero " << zeroReplies << ")";
  if( icParityErrors > 0 )
    cout << " (Errors: CRC=" << icParityErrors << ")";
  cout << endl;
}

// ----------------------------------------------------------------------------

int main( int argc, char *argv[] )
{
  int  opt;
  int  cardnr     = 0;
  int  linknr     = -1;
  int  dma_write  = -1;// Autoselect FromHost DMA controller index
  int  dma_read   = 0;
  bool receive    = true;
  int  io         = -1;
  int  repeat_cnt = 1;
  int  i2c_addr   = -1;
  bool use_ec     = false;
  bool lpgbt_v1   = false;

  // Parse the options
  unsigned int x;
  while( (opt = getopt(argc, argv, "1d:D:eG:hi:I:r:VZ")) != -1 )
    {
      switch( opt )
        {
        case '1':
          // lpGBTv1 or lpGBTv0
          lpgbt_v1 = true;
          break;
        case 'd':
          if( sscanf( optarg, "%d", &cardnr ) != 1 )
            arg_error( 'd' );
          break;
        case 'D':
          // DMA controller to use
          if( sscanf( optarg, "%d", &dma_read ) != 1 )
            arg_error( 'D' );
          if( dma_read < 0 || dma_read > 7 )
            arg_range( 'D', 0, 7 );
          break;
        case 'e':
          use_ec = true;
          break;
        case 'G':
          // Link number to use
          if( sscanf( optarg, "%d", &linknr ) != 1 )
            arg_error( 'G' );
          if( linknr < 0 || linknr > FLX_LINKS-1 )
            arg_range( 'G', 0, FLX_LINKS-1 );
          break;
        case 'h':
          usage();
          return 0;
        case 'i':
          if( sscanf( optarg, "%d", &io ) != 1 )
            arg_error( 'i' );
          break;
        case 'I':
          // lpGBT I2C address
          if( sscanf( optarg, "%x", &x ) != 1 )
            arg_error( 'I' );
          i2c_addr = x;
          if( i2c_addr < 0 || i2c_addr > 127 )
            arg_range_hex( 'I', 0, 127 );
          break;
        case 'r':
          if( sscanf( optarg, "%d", &repeat_cnt ) != 1 )
            arg_error( 'r' );
          if( repeat_cnt < 1 )
            arg_range( 'r', 1, 65536 );
          break;
        case 'V':
          cout << "Version " << hex << VERSION_ID << dec
               << " (tag: " << FELIX_TAG << ")" << endl;
          return 0;
        case 'Z':
          receive = false;
          break;
        default: // '?'
          usage();
          return 1;
        }
    }

  // Check for mandatory parameters
  if( linknr < 0 || i2c_addr < 0 || io < 0 )
    {
      if( linknr < 0 )
        cout << "### Provide a lpGBT link number (-G)" << endl;
      if( i2c_addr < 0 )
        cout << "### Provide an lpGBT I2C address (-I)" << endl;
      if( io < 0 )
        cout << "### Provide a GPIO bit number (-i)" << endl;
      return 1;
    }

  // -------------------------------------------------------------------------
  // FLX-card sender, receiver and FELIX data chunk reader/data checker

  FlxUpload fup( cardnr, 0, dma_write );
  if( fup.hasError() )
    {
      cout << "### " << fup.errorString() << endl;
      return 1;
    }
  cout << "Opened FLX-device " << cardnr
       << ", firmw " << fup.firmwareVersion() << endl;

  if( fup.fanOutLocked() )
    cout <<  "**NB**: FanOut-Select registers are locked!" << endl;
  fup.setFanOutForDaq();

  // HDLC delay support?
  cout << "HDLC delay support: "
       << (fup.hdlcDelaySupport() ? "YES" : "NO") << endl;

  FlxReceiver *frecvr = 0;
  if( receive )
    {
      frecvr = new FlxReceiver( cardnr, 256*1024*1024, dma_read );
      if( frecvr->hasError() )
        {
          cout << "### " << frecvr->errorString() << endl;
          frecvr->stop();
          return 1;
        }
    }

  int     iobyt = io/8;
  uint8_t iobit = 1 << (io & 0x7);

  // -------------------------------------------------------------------------

  // lpGBTv0 or lpGBTv1 ?
  lpgbt_v1 = detectLpGbtV1( &fup, frecvr, linknr, i2c_addr, use_ec );

  // Determine the register addresses to use
  int piodir_addr, pioin_addr, pioout_addr;
  if( iobyt == 1 )
    {
      piodir_addr = (lpgbt_v1 ? PIODIRH_V1 : PIODIRH_V0);
      pioin_addr  = (lpgbt_v1 ? PIOINH_V1 : PIOINH_V0);
      pioout_addr = (lpgbt_v1 ? PIOOUTH_V1 : PIOOUTH_V0);
    }
  else
    {
      piodir_addr = (lpgbt_v1 ? PIODIRL_V1 : PIODIRL_V0);
      pioin_addr  = (lpgbt_v1 ? PIOINL_V1 : PIOINL_V0);
      pioout_addr = (lpgbt_v1 ? PIOOUTL_V1 : PIOOUTL_V0);
    }

  // -------------------------------------------------------------------------
  // Get current GPIO I/O status

  uint8_t gpio_dir, gpio_out; // pull-up settings too?

  // Read PIODIR register
  if( !readIcRegs( &fup, frecvr, linknr, i2c_addr, piodir_addr, 1, &gpio_dir,
                   use_ec, lpgbt_v1 ) )
    {}//return 1;

  // Read PIOOUT register
  if( !readIcRegs( &fup, frecvr, linknr, i2c_addr, pioout_addr, 1, &gpio_out,
                   use_ec, lpgbt_v1 ) )
    {}//return 1;

  //uint8_t tst = 0x11;
  //writeIcRegs( &fup, frecvr, linknr, i2c_addr, pioout_addr, 1, &tst,
  //            use_ec, lpgbt_v1 );
  //readIcRegs( &fup, frecvr, linknr, i2c_addr, piodir_addr, 1, &tst,
  //            use_ec, lpgbt_v1 );
  //readIcRegs( &fup, frecvr, linknr, 0, 0, 0, &tst,
  //            use_ec, lpgbt_v1 );
  //readIcRegs( &fup, frecvr, linknr, i2c_addr, pioout_addr, 1, &tst,
  //            use_ec, lpgbt_v1 );
  //fup.uploadIcFrame( true, linknr, i2c_addr, piodir_addr, 1, &tst,
  //            use_ec, lpgbt_v1 );
  //fup.uploadIcFrame( true, linknr, i2c_addr, pioout_addr, 1, &tst,
  //            use_ec, lpgbt_v1 );
  //return 0;
  // -------------------------------------------------------------------------

  fup.resetIcFrames();

  // The 1-Wire operation
  // -------------------------------------------------------
  // ds24_init():

  // ds24_to_output()
  gpio_dir |= iobit;
  fup.addIcWriteFrame( i2c_addr, piodir_addr, 1, &gpio_dir, lpgbt_v1 );

  /*
  uint8_t tst = 0x22;
  fup.addIcWriteFrame( i2c_addr, piodir_addr, 1, &tst, lpgbt_v1 );
  fup.addIcDelay( 1 );
  tst = 0x34;
  fup.addIcWriteFrame( i2c_addr, pioout_addr, 1, &tst, lpgbt_v1 );
  fup.addIcDelay( 1 );
  fup.addIcReadFrame( i2c_addr, piodir_addr, 1, lpgbt_v1 );
  fup.dumpIcFrames();

  for( int i=0; i<repeat_cnt; ++i )
    fup.uploadIcFrames( linknr, use_ec );
  return 0;
  */

  // ds24_clear()
  gpio_out &= ~iobit;
  fup.addIcWriteFrame( i2c_addr, pioout_addr, 1, &gpio_out, lpgbt_v1 );

  // 500 us delay: insert_delay( 500 );
  fup.addIcDelayUs( 500 );

  // ds24_to_input()
  gpio_dir &= ~iobit;
  fup.addIcWriteFrame( i2c_addr, piodir_addr, 1, &gpio_dir, lpgbt_v1 );
  // 70 us delay: insert_delay( 70 );
  fup.addIcDelayUs( 70 );

  // If a DS2411 device is present it will pull the line low
  fup.addIcReadFrame( i2c_addr, pioin_addr, 1, lpgbt_v1 );
  // 400 us delay: insert_delay( 400 );
  //fup.addicDelayUs( 400 ); // ALTERNATIVE DELAY CONFIG
  fup.addIcDelayUs( 400+200 );

  // -------------------------------------------------------
  // ds24_write_byte( DS2401_READROM_CMD ):
  int val = 0x33; // DS24_READROM_CMD
  for( int i=0; i<8; ++i, val>>=1 )
    {
      //fup.addicDelay( 300 ); // >100 // ALTERNATIVE DELAY CONFIG

      // ds24_to_output()
      gpio_dir |= iobit;
      fup.addIcWriteFrame( i2c_addr, piodir_addr, 1, &gpio_dir, lpgbt_v1 );

      // ds24_clear()
      gpio_out &= ~iobit;
      fup.addIcWriteFrame( i2c_addr, pioout_addr, 1, &gpio_out, lpgbt_v1 );

      if( val & 1 )
        {
          // ds24_to_input()
          gpio_dir &= ~iobit;
          fup.addIcWriteFrame( i2c_addr, piodir_addr,
                               1, &gpio_dir, lpgbt_v1 );
        }
      else
        {
          // Delay..
          fup.addIcDelay( 300 ); // >24*12

          // ds24_to_input()
          gpio_dir &= ~iobit;
          fup.addIcWriteFrame( i2c_addr, piodir_addr,
                               1, &gpio_dir, lpgbt_v1 );
        }
      fup.addIcDelay( 300 ); // >100
    }

  // -------------------------------------------------------
  // Read ID bytes (pseudo-code):
  //   for( j=0; j<DS2401_BYTES; ++j )
  //     ds24_data[j] = ds24_read_byte();

  for( int j=0; j<8; ++j ) 
    // ds24_read_byte():
    for( int i=0; i<8; ++i )
      {
        //fup.addScaDelay( 350 ); // >=310 && <=390 // ALTERNATIVE DELAY CONFIG

        // ds24_to_output()
        gpio_dir |= iobit;
        fup.addIcWriteFrame( i2c_addr, piodir_addr, 1, &gpio_dir, lpgbt_v1 );
        // ds24_clear()
        gpio_out &= ~iobit;
        fup.addIcWriteFrame( i2c_addr, pioout_addr, 1, &gpio_out, lpgbt_v1 );
        // ds24_to_input()
        gpio_dir &= ~iobit;
        fup.addIcWriteFrame( i2c_addr, piodir_addr, 1, &gpio_dir, lpgbt_v1 );

        // Read
        fup.addIcReadFrame( i2c_addr, pioin_addr, 1, lpgbt_v1 );

        fup.addIcDelay( 350 ); // >=310 && <=390
      }

  //fup.dumpIcFrames(); // DEBUG: display compiled lpGBT IC frames

  // Execute the 1-Wire operation
  for( int i=0; i<repeat_cnt; ++i )
    fup.uploadIcFrames( linknr, use_ec );

  //fup.dumpData( 16*32 ); // DEBUG: display (some) uploaded FromHost blocks

  // Check replies and extract the ID information
  FlxParser fparser;
  fparser.setReceiver( frecvr );
  cout << "ID:" << endl;
  getDs24Replies( &fparser, linknr, io, lpgbt_v1, use_ec );

  // -------------------------------------------------------------------------
  // Finish up

  fup.stop();
  if( frecvr )
    frecvr->stop();
  //cout << endl;
  return 0;
}

// ----------------------------------------------------------------------------

void usage()
{
  cout << "flpgbtds24 version " << hex << VERSION_ID << dec << endl <<
    "Tool to read out the 64-bit ID from a 1-Wire DS24xx chip connected to "
    "an lpGBT GPIO.\n"
    "Usage:\n"
    " flpgbtds24 [-h|V] [-d<devnr>] [-D<dma>] -G<link> -I<i2c> [-1] [-e] "
    "-i<pin> [-r<cnt>] [-Z]\n"
    "  -h         : Show this help text.\n"
    "  -V         : Show version.\n"
    "  -d <devnr> : FLX-device number (default: 0).\n"
    "  -D <dma>   : FLX-device DMA controller for receiving (default: 0).\n"
    "  -1         : Assume lpGBTv1 (default: lpGBTv0) ###OBSOLETE\n"
    "  -e         : Use the EC-channel (default: IC-channel).\n"
    "  -G <link>  : lpGBT link number.\n"
    "  -I <i2c>   : lpGBT I2C address (hex).\n"
    "  -i <pin>   : Use GPIO bit <pin> for the 1-Wire protocol ([0..15]).\n"
    "  -r <cnt>   : Number of times to read the ID (default: 1).\n"
    "  -Z         : Do not receive and display replies.\n";
}

/* ------------------------------------------------------------------------ */

uint8_t ds24_crc( uint8_t *data )
{
  // Calculate CRC-8 value of the first 7 bytes of DS24xx data
  // (1 byte family code and 6 bytes serial number);
  // uses The CCITT-8 polynomial, expressed as X^8 + X^5 + X^4 + 1
  uint8_t crc = 0x00;
  for( int index=0; index<7; ++index ) {
    uint8_t byt = data[index];
    for( int b=0; b<8; ++b ) {
      if( (byt^crc) & 0x01 )
        {
          crc ^= 0x18;
          crc >>= 1;
          crc |= 0x80;
        }
      else
        {
          crc >>= 1;
        }
      byt >>= 1;
    }
  }
  return crc;
}

// ----------------------------------------------------------------------------
