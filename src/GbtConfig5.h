#ifndef GBTCONFIG5_H
#define GBTCONFIG5_H

#include "flxdefs.h"

// Legacy RM4 enable bits values, for non-egroup e-links
// (in TO/FROMHOST_MINI_GROUP, except FULL_ENABLED_BIT, which is in egroup 0)
const uint32_t EC_ENABLED_BIT    = 14;
const uint32_t IC_ENABLED_BIT    = 13;
const uint32_t AUX_ENABLED_BIT   = 12;
const uint32_t TTC2H_ENABLED_BIT = 10;
const uint32_t FULL_ENABLED_BIT  = 16;
const uint32_t EC_ENABLED        = (1<<EC_ENABLED_BIT);    // 0x4000
const uint32_t IC_ENABLED        = (1<<IC_ENABLED_BIT);    // 0x2000
const uint32_t AUX_ENABLED       = (1<<AUX_ENABLED_BIT);   // 0x1000
const uint32_t TTC2H_ENABLED     = (1<<TTC2H_ENABLED_BIT); // 0x0400
const uint32_t FULL_ENABLED      = (1<<FULL_ENABLED_BIT);  // 0x10000

enum : uint32_t {
  LINKMODE_GBT = 0,
  LINKMODE_GBTWIDE,
  LINKMODE_FULL,
  LINKMODE_LPGBT10_F5,
  LINKMODE_LPGBT5_F5,
  LINKMODE_LPGBT10_F12,
  LINKMODE_LPGBT5_F12,
  LINKMODE_INVALID
};

// For additional register settings optionally included in configuration files
typedef struct register_setting {
  std::string name;
  uint64_t value;
} regsetting_t;

// Structure/class describing a single (lp)GBT-link configuration for FELIX
class GbtConfig
{
 private:
  // Link type: GBT, GBT-wide, FULL or lpGBT
  uint32_t _linkMode;

  // Clock: TTC or local
  bool     _ttcClock;

  // FromHost link mode: GBT or LTI (applies to FULL mode only)
  bool     _ltiTtc;

  // ToHost:
  // E-link configuration bitmask, 7+1 e-groups per link
  uint32_t _enablesToHost[FLX_TOHOST_GROUPS];
  // E-link group width (actually a width code: 0=2bits, 1=4, 2=8, 3=16, 4=32)
  // 7+1 (to Host) e-groups per link
  uint32_t _widthToHost[FLX_TOHOST_GROUPS];
  // E-link group mode words, 7+1 (to Host) e-groups per link
  uint64_t _modesToHost[FLX_TOHOST_GROUPS];
  // DMA index for ToHost E-links (1 nibble per E-path, 7+1 groups * 8)
  uint64_t _dmaIndices[FLX_TOHOST_GROUPS];
  // Stream-ID indicator bits (1 per ToHost E-path, 7 groups * 8 bits)
  uint64_t _streamIdBits[FLX_TOHOST_GROUPS-1];
  // Maximum ToHost chunk length word (4x3 bits per GBT)
  uint32_t _maxChunkWord;

  // FromHost:
  // E-link enable configuration bitmask, 5+1 e-groups per link
  uint32_t _enablesFromHost[FLX_FROMHOST_GROUPS];
  // E-link group width (actually a width code: 0=2bits, 1=4, 2=8, 3=16, 4=32)
  // 5+1 (from Host) e-groups per link
  uint32_t _widthFromHost[FLX_FROMHOST_GROUPS];
  // E-link group mode words, 5+1 (from Host) e-groups per link
  uint64_t _modesFromHost[FLX_FROMHOST_GROUPS];
  // E-link group TTC option value, 5 (from Host) e-groups per link
  uint32_t _ttcOptionFromHost[FLX_FROMHOST_GROUPS-1];

  // --------------------------------------------------------------------------

 public:
  GbtConfig()
  {
    _linkMode = LINKMODE_GBT;

    _ttcClock = false;

    _ltiTtc = false;

    // Default 2-bit elinks, '8b10b' mode, standard-mode GBT
    // In case of full-mode: link enabled (bit 16 set in 'enables' word)
    int egroup;
    for( egroup=0; egroup<FLX_TOHOST_GROUPS-1; ++egroup )
      {
        _enablesToHost[egroup] = 0x00;
        _widthToHost[egroup]   = 0; // 2-bit
        _modesToHost[egroup]   = 0x11111111; // All '8b10b'
        _dmaIndices[egroup]    = 0;
      }

    // If FULL mode then link is enabled
    //_enablesToHost[0] |= FULL_ENABLED;

    // EC+IC+AUX+TTCtoHost group
    //_enablesToHost[TOHOST_MINI_GROUP] = 0x4400; // EC, TTCtoHost
    //_enablesToHost[TOHOST_MINI_GROUP] = 0x6400; // EC, IC, TTCtoHost
    //_enablesToHost[TOHOST_MINI_GROUP] = 0x7400; // EC, IC, AUX, TTCtoHost
    //_enablesToHost[TOHOST_MINI_GROUP] = EC_ENABLED; // EC, not TTCtoHost
    _enablesToHost[TOHOST_MINI_GROUP] = 0; // None enabled
    _widthToHost[TOHOST_MINI_GROUP]   = 0; // 2-bit
    _modesToHost[TOHOST_MINI_GROUP]   = ((uint64_t)2 << (EC_ENABLED_BIT*4)); // EC 'HDLC'
    _modesToHost[TOHOST_MINI_GROUP]  |= ((uint64_t)2 << (IC_ENABLED_BIT*4));  // IC 'HDLC'
    _modesToHost[TOHOST_MINI_GROUP]  |= ((uint64_t)2 << (AUX_ENABLED_BIT*4)); // AUX 'HDLC'
    _dmaIndices[TOHOST_MINI_GROUP]    = 0;

    for( egroup=0; egroup<FLX_FROMHOST_GROUPS-1; ++egroup )
      {
        _enablesFromHost[egroup]   = 0x00;
        _widthFromHost[egroup]     = 0; // 2-bit
        _modesFromHost[egroup]     = 0x11111111; // All '8b10b'
        _ttcOptionFromHost[egroup] = 0;
      }
    // EC+IC+AUX group
    //_enablesFromHost[FROMHOST_MINI_GROUP] = EC_ENABLED; // EC
    _enablesFromHost[FROMHOST_MINI_GROUP] = 0; // None enabled
    _widthFromHost[FROMHOST_MINI_GROUP]   = 0; // 2-bit
    _modesFromHost[FROMHOST_MINI_GROUP]   = ((uint64_t)2 << (EC_ENABLED_BIT*4)); // EC 'HDLC'
    _modesFromHost[FROMHOST_MINI_GROUP]  |= ((uint64_t)2 << (IC_ENABLED_BIT*4));  // IC 'HDLC'
    _modesFromHost[FROMHOST_MINI_GROUP]  |= ((uint64_t)2 << (AUX_ENABLED_BIT*4)); // AUX 'HDLC'

    //maxChunkWord = 0x249; Or rather (512 bytes max chunksize):
    //_maxChunkWord = (1<<(0*3)) | (1<<(1*3)) | (1<<(2*3)) | (1<<(3*3));

    // Max chunk size set to all 0 (no maximum)
    _maxChunkWord = (0<<(0*3)) | (0<<(1*3)) | (0<<(2*3)) | (0<<(3*3));

    for( egroup=0; egroup<FLX_TOHOST_GROUPS-1; ++egroup )
      _streamIdBits[egroup] = 0;
  }

  ~GbtConfig() {}

  // --------------------------------------------------------------------------

  int numberOfElinksToHost()
  {
    // Return the enabled ToHost (E-)link count
    if( _linkMode >= LINKMODE_INVALID )
      return 0;

    int cnt = 0;
    if( _linkMode != LINKMODE_FULL )
      {
        // Groups: GBT 5, GBT-wide 7, lpGBT-FEC5 7, lpGBT-FEC12 6
        uint32_t grp_cnt, path_cnt;
        if( _linkMode == LINKMODE_LPGBT10_F5 ||
            _linkMode == LINKMODE_LPGBT5_F5 )
          {
            grp_cnt = 7;
            path_cnt = 4;
          }
        else if( _linkMode == LINKMODE_LPGBT10_F12 ||
                 _linkMode == LINKMODE_LPGBT5_F12 )
          {
            grp_cnt = 6;
            path_cnt = 4;
          }
        else if( _linkMode == LINKMODE_GBTWIDE )
          {
            grp_cnt = 7;
            path_cnt = 8;
          }
        else
          {
            grp_cnt = 5;
            path_cnt = 8;
          }

        for( uint32_t i=0; i<grp_cnt; ++i )
          for( uint32_t j=0; j<path_cnt; ++j )
            if( _enablesToHost[i] & (1<<j) )
              ++cnt;
      }
    else
      {
        // FULLMODE link
        if( _enablesToHost[0] & FULL_ENABLED )
          cnt = 1;
      }
    return cnt;
  }

  // --------------------------------------------------------------------------

  int numberOfElinksFromHost()
  {
    // Return the enabled FromHost E-link count
    // GBT: 5, GBT-wide: 3, lpGBT: 4 groups
    uint32_t grp_cnt, path_cnt;
    if( _linkMode == LINKMODE_LPGBT10_F5 ||
        _linkMode == LINKMODE_LPGBT5_F5 )
      {
        grp_cnt = 4;
        //path_cnt = 4;
        path_cnt = 5; // STRIP firmware has 5
      }
    else if( _linkMode == LINKMODE_LPGBT10_F12 ||
             _linkMode == LINKMODE_LPGBT5_F12 )
      {
        grp_cnt = 4;
        path_cnt = 4;
      }
    else if( _linkMode == LINKMODE_GBTWIDE )
      {
        grp_cnt = 5;
        path_cnt = 8;
      }
    else
      {
        grp_cnt = 5;
        path_cnt = 8;
      }

    int cnt = 0;
    for( uint32_t i=0; i<grp_cnt; ++i )
      for( uint32_t j=0; j<path_cnt; ++j )
        if( _enablesFromHost[i] & (1<<j) )
          ++cnt;
    return cnt;
  }

  // --------------------------------------------------------------------------

  int numberOfEcIcToHost()
  {
    // Return the number of enabled ToHost EC/IC/AUX/TTCtoHost E-links
    if( _linkMode >= LINKMODE_INVALID )
      return 0;

    int cnt = 0;
    if( _linkMode != LINKMODE_FULL )
      {
        // EC + IC + AUX + TTCtoHost
        for( uint32_t j=0; j<16; ++j )
          if( _enablesToHost[TOHOST_MINI_GROUP] & (1<<j) )
            ++cnt;
      }
    else
      {
        // TTCtoHost only
        if( _enablesToHost[TOHOST_MINI_GROUP] & TTC2H_ENABLED )
          ++cnt;
      }
    return cnt;
  }

  // --------------------------------------------------------------------------

  int numberOfEcIcFromHost()
  {
    // Return the number of enabled FromHost EC/IC/AUX E-links
    int cnt = 0;
    // EC + IC + AUX
    for( uint32_t j=0; j<16; ++j )
      if( _enablesFromHost[FROMHOST_MINI_GROUP] & (1<<j) )
        ++cnt;
    return cnt;
  }

  // --------------------------------------------------------------------------

  uint32_t enablesToHost( uint32_t egroup )
  {
    if( egroup >= FLX_TOHOST_GROUPS )
      return 0;
    return _enablesToHost[egroup];
  }

  void setEnablesToHost( uint32_t egroup, uint32_t enables )
  {
    if( egroup >= FLX_TOHOST_GROUPS )
      return;
    _enablesToHost[egroup] = enables;
  }

  // --------------------------------------------------------------------------

  uint32_t enablesFromHost( uint32_t egroup )
  {
    if( egroup >= FLX_FROMHOST_GROUPS )
      return 0;
    return _enablesFromHost[egroup];
  }

  void setEnablesFromHost( uint32_t egroup, uint32_t enables )
  {
    if( egroup >= FLX_FROMHOST_GROUPS )
      return;
    _enablesFromHost[egroup] = enables;
  }

  // --------------------------------------------------------------------------

  uint32_t widthToHost( uint32_t egroup )
  {
    if( egroup >= FLX_TOHOST_GROUPS )
      return 0;
    return _widthToHost[egroup];
  }

  void setWidthToHost( uint32_t egroup, uint32_t width )
  {
    if( egroup >= FLX_TOHOST_GROUPS )
      return;
    _widthToHost[egroup] = width;
  }

  // --------------------------------------------------------------------------

  uint32_t widthFromHost( uint32_t egroup )
  {
    if( egroup >= FLX_FROMHOST_GROUPS )
      return 0;
    return _widthFromHost[egroup];
  }

  void setWidthFromHost( uint32_t egroup, uint32_t width )
  {
    if( egroup >= FLX_FROMHOST_GROUPS )
      return;
    _widthFromHost[egroup] = width;
  }

  // --------------------------------------------------------------------------

  uint64_t modesToHost( uint32_t egroup )
  {
    if( egroup >= FLX_TOHOST_GROUPS )
      return 0;
    return _modesToHost[egroup];
  }

  void setModesToHost( uint32_t egroup, uint64_t modes )
  {
    if( egroup >= FLX_TOHOST_GROUPS )
      return;
    _modesToHost[egroup] = modes;
  }

  int mode( int egroup, int epath, bool from_host = false )
  {
    // Returns the currently set mode for the given E-path
    uint64_t modes;
    if( from_host )
      {
        if( egroup >= FLX_FROMHOST_GROUPS )
          return 0;
        modes = _modesFromHost[egroup];
      }
    else
      {
        if( egroup >= FLX_TOHOST_GROUPS )
          return 0;
        modes = _modesToHost[egroup];
      }

    return( (modes >> (epath*4)) & 0xF );
  }

  // --------------------------------------------------------------------------

  uint64_t modesFromHost( uint32_t egroup )
  {
    if( egroup >= FLX_FROMHOST_GROUPS )
      return 0;
    return _modesFromHost[egroup];
  }

  void setModesFromHost( uint32_t egroup, uint64_t modes )
  {
    if( egroup >= FLX_FROMHOST_GROUPS )
      return;
    _modesFromHost[egroup] = modes;
  }

  // --------------------------------------------------------------------------

  uint32_t ttcOptionFromHost( uint32_t egroup )
  {
    if( egroup >= FLX_FROMHOST_GROUPS-1 )
      return 0;
    return _ttcOptionFromHost[egroup];
  }

  void setTtcOptionFromHost( uint32_t egroup, uint32_t option )
  {
    if( egroup >= FLX_FROMHOST_GROUPS-1 )
      return;
    _ttcOptionFromHost[egroup] = option;
  }

  // --------------------------------------------------------------------------

  uint32_t dmaIndices( uint32_t egroup )
  {
    if( egroup >= FLX_TOHOST_GROUPS )
      return 0;
    return _dmaIndices[egroup];
  }

  void setDmaIndices( uint32_t egroup, uint32_t indices )
  {
    if( egroup >= FLX_TOHOST_GROUPS )
      return;
    _dmaIndices[egroup] = indices;
  }

  // --------------------------------------------------------------------------

  uint32_t maxChunkWord()
  {
    return _maxChunkWord;
  }

  void setMaxChunkWord( uint32_t chunkword )
  {
    _maxChunkWord = chunkword;
  }

  void setMaxChunkSize( int index, int size )
  {
    _maxChunkWord &= ~(7 << (index*3));
    _maxChunkWord |= (size/FLX_MAXCHUNK_UNIT) << (index*3);
  }

  // --------------------------------------------------------------------------

  uint32_t linkMode()
  {
    return _linkMode;
  }

  void setLinkMode( uint32_t mode )
  {
    _linkMode = mode;

    // Adjust other settings dependent on the mode
    if( mode == LINKMODE_LPGBT10_F5 ||
        mode == LINKMODE_LPGBT10_F12 )
      {
        for( int i=0; i<FLX_TOHOST_GROUPS-1; ++i )
          if( _widthToHost[i] < 2 ) // Invalid width, so set to:
            _widthToHost[i] = 2;    // lpGBT-10 8-bit

        for( int i=0; i<FLX_TOHOST_GROUPS-1; ++i )
          _enablesToHost[i] &= ~0xF0; // Only 4 paths per ToHost group

        for( int i=0; i<FLX_FROMHOST_GROUPS-1; ++i )
          //_enablesFromHost[i] &= ~0xF0; // Only 4 paths per FromHost group
          _enablesFromHost[i] &= ~0xE0;   // STRIP firmware has 5...
      }
    else if( mode == LINKMODE_LPGBT5_F5 ||
             mode == LINKMODE_LPGBT5_F12 )
      {
        for( int i=0; i<FLX_TOHOST_GROUPS-1; ++i )
          if( _widthToHost[i] < 1 ) // Invalid width, so set to:
            _widthToHost[i] = 1;    // lpGBT-5 4-bit

        for( int i=0; i<FLX_TOHOST_GROUPS-1; ++i )
          _enablesToHost[i] &= ~0xF0; // Only 4 paths per ToHost group

        for( int i=0; i<FLX_FROMHOST_GROUPS-1; ++i )
          _enablesFromHost[i] &= ~0xF0; // Only 4 paths per FromHost group
      }
  }

  // --------------------------------------------------------------------------

  bool ltiTtc()
  {
    return _ltiTtc;
  }

  void setLtiTtc( bool b )
  {
    _ltiTtc = b;
  }

  // --------------------------------------------------------------------------

  bool ttcClock()
  {
    return _ttcClock;
  }

  void setTtcClock( bool b )
  {
    _ttcClock = b;
  }

  // --------------------------------------------------------------------------

  uint32_t streamIdBits( uint32_t egroup )
  {
    if( egroup >= FLX_TOHOST_GROUPS-1 )
      return 0;
    return _streamIdBits[egroup];
  }

  void setStreamIdBits( uint32_t egroup, uint32_t bits )
  {
    if( egroup >= FLX_TOHOST_GROUPS-1 )
      return;
    _streamIdBits[egroup] = bits;
  }

  // --------------------------------------------------------------------------

  bool isEnabled( int egroup, int epath, int *nbits, bool from_host = false )
  {
    // Determine whether a certain E-link number (defined by its 'egroup' and
    // 'path') in this GbtConfig is enabled, and if so,
    // how many bits wide it is (returned in 'nbits')

    // Take into account special egroup/epath numbers for EC,IC etc. e-links
    // in combination with from/to direction (which is asymetrical)
    // (this is a bit messy, blame it on legacy...)
    if( egroup == TOHOST_MINI_GROUP )
      {
        if( epath == 7 ) // EC
          epath = EC_ENABLED_BIT;
        else if( epath == 6 ) // IC
          epath = IC_ENABLED_BIT;
        else if( epath == 5 ) // SCA-AUX
          epath = AUX_ENABLED_BIT;
        else if( epath == 3 ) // TTCtoHost
          epath = TTC2H_ENABLED_BIT;
      }
    else if( !from_host && _linkMode == LINKMODE_FULL &&
             egroup == 0 && epath == 0 )
      {
        epath = FULL_ENABLED_BIT;
      }

    // In FromHost direction the special egroup is a different one...
    if( from_host && egroup == TOHOST_MINI_GROUP )
      egroup = FROMHOST_MINI_GROUP;

    if( nbits )
      {
        if( from_host )
          *nbits = GbtConfig::widthCodeToBits( _widthFromHost[egroup] );
        else if( !from_host && _linkMode == LINKMODE_FULL )
          *nbits = 0;
        else
          *nbits = GbtConfig::widthCodeToBits( _widthToHost[egroup] );
      }

    uint32_t enable_bits;
    if( from_host )
      {
        if( egroup >= FLX_FROMHOST_GROUPS )
          return false;
        enable_bits = _enablesFromHost[egroup];
      }
    else
      {
        if( egroup >= FLX_TOHOST_GROUPS )
          return false;
        enable_bits = _enablesToHost[egroup];
      }

    bool enabled = ((enable_bits & (1 << epath)) != 0);
    return enabled;
  }

  // --------------------------------------------------------------------------

  static uint64_t epathBitsWord( uint32_t enables, uint32_t width )
  {
    // Compile a 64-bit word in which each byte indicates the number
    // of bits per E-path/FIFO, in order of (up to 8) E-paths,
    // zero meaning E-path/FIFO is not in use
    // (utility function used e.g. for emulator data generation)
    uint64_t nbits_word = 0;
    for( int i=0; i<8; ++i )
      if( enables & (1<<i) )
        nbits_word |= ((uint64_t) width) << (i*8);
    return nbits_word;
  }

  // --------------------------------------------------------------------------

  static int widthCodeToBits( uint32_t width_code )
  {
    if( width_code == 0 )      return 2;
    else if( width_code == 1 ) return 4;
    else if( width_code == 2 ) return 8;
    else if( width_code == 3 ) return 16;
    else if( width_code == 4 ) return 32;
    return 0;
  }

  // --------------------------------------------------------------------------
};

#endif // GBTCONFIG5_H
