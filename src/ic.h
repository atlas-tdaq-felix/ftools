
bool detectLpGbtV1 ( FlxUpload *fup, FlxReceiver *frecvr,
                     int linknr, int i2c_addr,
                     bool use_ec = false );
bool writeIcRegs   ( FlxUpload *fup, FlxReceiver *frecvr,
                     int linknr, int i2c_addr,
                     int reg_addr, int nbytes, uint8_t *reg_vals,
                     bool use_ec, bool lpgbt_v1,
                     bool debug = false, bool display = true );
bool readIcRegs    ( FlxUpload *fup, FlxReceiver *frecvr,
                     int linknr, int i2c_addr,
                     int reg_addr, int nbytes, uint8_t *reg_vals,
                     bool use_ec, bool lpgbt_v1,
                     bool debug = false, bool display = true );
bool receiveIcReply( FlxReceiver *frecvr, int linknr,
                     int reg_addr, int nbytes, uint8_t *reg_vals,
                     int timeout_us,
                     bool use_ec, bool lpgbt_v1,
                     bool debug = false, bool display = true );

bool probeIcConfigFile( const std::string &filename,
                        bool *with_addr, bool *with_comment );
bool nextFromIcConfigFile( std::ifstream &file, uint32_t &line_nr,
                           uint32_t &regaddr, uint32_t &byteval,
                           bool with_addr, bool with_comment );
int  compareIcConfigFile( uint8_t *regarray, int size,
                          bool is_lpgbt, bool is_lpgbt_v1,
                          const std::string &filename,
                          int *nbytes_file );

bool writeIcRegsViaLpgbtI2c( FlxUpload *fup, FlxReceiver *frecvr,
                             int linknr, int i2c_addr,
                             int masternr, int i2c_freq, int i2c_addr_slave,
                             bool i2c_configure,
                             int reg_addr, int nbytes, uint8_t *reg_vals,
                             bool use_ec = false, bool single_dma = false,
                             bool debug = false, bool display = true );
bool readIcRegsViaLpgbtI2c( FlxUpload *fup, FlxReceiver *frecvr,
                            int linknr, int i2c_addr,
                            int masternr, int i2c_freq, int i2c_addr_slave,
                            bool i2c_configure,
                            int reg_addr, int nbytes, uint8_t *reg_vals,
                            bool use_ec = false, bool single_dma = false,
                            bool debug = false, bool display = true );
void writeIcConfigFileViaLpgbtI2c( FlxUpload *fup, FlxReceiver *frecvr,
                                   int linknr, int i2c_addr,
                                   int i2c_master, int i2c_freq, int i2c_addr_slave,
                                   const std::string &filename,
                                   int *nbytes_file,
                                   bool use_ec = false );
void configI2c( FlxUpload *fup, FlxReceiver *frecvr,
                int linknr, int i2c_addr,
                int i2c_master, int i2c_freq,
                int i2c_addr_slave, int nbytes,
                bool use_ec, bool lpgbt_v1,
                bool single_dma = false );
void enableI2cPullups( FlxUpload *fup, FlxReceiver *frecvr,
                       int linknr, int i2c_addr,
                       int masternr, bool enable,
                       bool use_ec, bool lpgbt_v1,
                       bool single_dma = false );
