#include <iostream>
#include <iomanip>
#include <cstring> // for strlen()
using namespace std;

#include "FlxUpload.h"
#include "FlxReceiver.h"
#include "FlxParser.h"
#include "ic.h"
#include "lpgbt-items-v0.h"
#include "lpgbt-items-v1.h"

// ----------------------------------------------------------------------------

bool detectLpGbtV1( FlxUpload *fup, FlxReceiver *frecvr,
                    int linknr, int i2c_addr,
                    bool use_ec )
{
  int reg_addr = 0; // Read a random register..
  uint8_t tmp;
  bool v1 = true;
  bool lpgbt_v1 = readIcRegs( fup, frecvr, linknr, i2c_addr, reg_addr, 1,
                              &tmp, use_ec, v1, false, false );
  if( lpgbt_v1 )
    {
      cout << ">>> Detected lpGBTv1" << endl;
    }
  else
    {
      v1 = false;
      if( readIcRegs( fup, frecvr, linknr, i2c_addr, reg_addr, 1,
                      &tmp, use_ec, v1, false, false ) )
        cout << ">>> Detected lpGBTv0/GBTX" << endl;
      else
        cout << "### Failed to detect GBTX/lpGBTv0 or v1" << endl;
    }
  return lpgbt_v1;
}

// ----------------------------------------------------------------------------

bool writeIcRegs( FlxUpload *fup, FlxReceiver *frecvr,
                  int linknr, int i2c_addr,
                  int reg_addr, int nbytes, uint8_t *reg_vals,
                  bool use_ec, bool lpgbt_v1,
                  bool debug, bool display )
{
  if( !fup->writeIcChannel( linknr, i2c_addr, reg_addr,
                            nbytes, reg_vals, use_ec, lpgbt_v1 ) )
    cout << "### writeIcRegs(0x" << hex << reg_addr << dec
         << ",nbytes=" << nbytes << "): "
         << fup->errorString() << endl;

  // Receive the reply (containing nbytes bytes) and display
  bool reply_received = receiveIcReply( frecvr, linknr,
                                        reg_addr, nbytes, reg_vals, 8000,
                                        use_ec, lpgbt_v1,
                                        debug, display );
  if( !reply_received && display )
    cout << "### writeIcRegs(): Reply not received (addr=0x"
         << hex << reg_addr << dec << ")" << endl;

  if( debug ) return true;
  return reply_received;
}

// ----------------------------------------------------------------------------

bool readIcRegs( FlxUpload *fup, FlxReceiver *frecvr,
                 int linknr, int i2c_addr,
                 int reg_addr, int nbytes, uint8_t *reg_vals,
                 bool use_ec, bool lpgbt_v1,
                 bool debug, bool display )
{
  if( !fup->readIcChannel( linknr, i2c_addr, reg_addr,
                           nbytes, use_ec, lpgbt_v1 ) )
    cout << "### readIcRegs(0x" << hex << reg_addr << dec
         << ",nbytes=" << nbytes << "): "
         << fup->errorString() << endl;

  // Receive the reply (containing nbytes bytes) and display
  bool reply_received = receiveIcReply( frecvr, linknr,
                                        reg_addr, nbytes, reg_vals, 8000,
                                        use_ec, lpgbt_v1,
                                        debug, display );
  if( !reply_received && display )
    cout << "### readIcRegs(): Reply not received (addr=0x"
         << hex << reg_addr << dec << ")" << endl;

  if( debug ) return true;
  return reply_received;
}

// ----------------------------------------------------------------------------

bool receiveIcReply( FlxReceiver *frecvr, int linknr,
                     int reg_addr, int nbytes, uint8_t *reg_vals,
                     int timeout_us,
                     bool use_ec, bool lpgbt_v1,
                     bool debug, bool display )
{
  // Function expects to read a single reply
  // with one or more register values returned

  // The IC (or EC) elink number from the original GBT firmware
  // (maintained within the software; will be mapped to the real number
  //  for Phase2 firmwares, internally)
  int egroup = 7;
  int epath  = 6;
  if( use_ec )
    epath = 7;
  int elinknr = ((linknr << BLOCK_LNK_SHIFT) |
                 (egroup << BLOCK_EGROUP_SHIFT) | epath);

  FlxParser fparser;
  fparser.setReceiver( frecvr );

  uint8_t *chunkdata = 0;
  uint32_t size, chunknr = 1;
  uint32_t parity_start_index = (lpgbt_v1 ? 0 : 2);
  int      offset = (lpgbt_v1 ? 0 : 1);
  bool     reply_received = false;
  // Note: must continue calling nextChunkRecvd() after reply_received becomes
  //       true (at least once), or block will not be freed and the same chunk
  //       is 'received' again in the next call of receiveIcReply()
  //       (better solution in nextChunkRecvd() desirable?)
  while( fparser.nextChunkRecvd( &chunkdata, &size, timeout_us, elinknr ) )
    {
      if( size == 0 ) continue;

      // Process chunk from the IC (or EC) channel
      uint32_t frame_len = size;
      uint8_t *frame     = chunkdata;
      // Check reply frame CRC
      // (do not include I2C-address and zero-byte, in case of lpGBTv0)
      if( frame_len > 2 )
        {
          if( debug )
            cout << "Chunk " << chunknr << ": ";
          uint8_t parity = 0;
          for( uint32_t i=parity_start_index; i<frame_len-1; ++i )
            parity ^= frame[i];
          if( parity != frame[frame_len-1] )
            {
              cout << hex << setfill('0')
                   << "### Parity "
                   << setw(2) << (uint32_t) frame[frame_len-1]
                   << ", expected "
                   << setw(2) << (uint32_t) parity
                   << dec << setfill(' ');
              if( !debug ) cout << endl;
              //continue;
            }
          else
            {
              if( debug )
                cout << "parity OK";
            }

          if( debug )
            {
              if( frame[0] & 1 )
                cout << ",  READ:";
              else
                cout << ", WRITE:";
              //cout << endl;
            }
        }

      if( debug )
        {
          // Display IC frame bytes
          cout << hex << uppercase << setfill('0');
          for( uint32_t i=0; i<frame_len; ++i )
            cout << " " << setw(2) << (uint32_t) frame[i];
          cout << endl << dec << nouppercase << setfill(' ');
        }

      // Display returned register values in IC frame
      // (I2C-address, zero-byte (lpGBTv0), command, #bytes, #address, parity:
      //  7 (v1) or 8 (v0) bytes in total) (###TBD: define v0 and v1 structs?)
      int data_len = frame_len - (offset+1+1+2+2+1);
      if( data_len > 0 && nbytes > 0 )
        {
          uint32_t len  = ((uint32_t) frame[offset+2] +
                           ((uint32_t) frame[offset+3]<<8));
          uint32_t addr = ((uint32_t) frame[offset+4] +
                           ((uint32_t) frame[offset+5]<<8));

          if( display || debug )
            cout << hex << uppercase << setfill('0');
          if( len == (uint32_t) nbytes && addr == (uint32_t) reg_addr )
            {
              if( display )
                {
                  cout << "Address 0x" << setw(3) << addr << ":";
                  for( int i=0; i<nbytes; ++i )
                    {
                      if( i > 0 && (i & 0xF) == 0 )
                        cout << endl << "              ";
                      cout << " 0x" << setw(2) << (uint32_t) frame[offset+6+i];
                    }
                  cout << endl;
                }
              reply_received = true;
            }
          else if( debug )
            {
              cout << "### Received unexpected length or address: "
                   << dec << len << hex << ", 0x" << setw(3) << addr
                   << " (expected " << nbytes << ", 0x" << setw(3) << reg_addr
                   << ")" << endl;
            }
          if( display || debug )
            cout << dec << nouppercase << setfill(' ');

          // Extract the register values to return
          for( int i=0; i<nbytes; ++i )
            reg_vals[i] = frame[offset+6+i];
        }
      ++chunknr;
    }

  return reply_received;
}

// ----------------------------------------------------------------------------

bool probeIcConfigFile( const std::string &filename,
                        bool *with_addr, bool *with_comment )
{
  std::ifstream file( filename );
  if( !file.is_open() )
    {
      cout << "### Failed to open file: " <<  filename << endl;
      return false;
    }

  char line[64], comment[64];
  uint32_t regaddr, byteval;
  file.getline( line, 64 );
  if( sscanf( line, "%X %X", &regaddr, &byteval ) == 2 )
    *with_addr = true;
  if( sscanf( line, "%X %X %s", &regaddr, &byteval, comment ) == 3 )
    *with_comment = true;
  file.close();

  cout << "File " << filename << ", format=";
  if( *with_comment && *with_addr )
    cout << "\"<addr> <bytevalue> <comment>\" (per line)" << endl;
  else if( *with_addr )
    cout << "\"<addr> <bytevalue>\" (per line)" << endl;
  else
    cout << "\"<bytevalue>\" (per line)" << endl;

  return true;
}

// ----------------------------------------------------------------------------

bool nextFromIcConfigFile( std::ifstream &file, uint32_t &line_nr,
                           uint32_t &regaddr, uint32_t &byteval,
                           bool with_addr, bool with_comment )
{
  if( file.eof() )
    return false;

  // Next value or address/value pair
  if( with_addr )
    {
      file >> regaddr;
      if( file.eof() )
        return false;
    }
  file >> byteval;

  if( file.fail() )
    {
      if( file.eof() )
        return false;

      std::ostringstream oss;
      oss << "illegal value at line " << line_nr
          << " addr=0x" << std::hex << std::setfill('0')
          << std::setw(3) << regaddr;
      cout << "### Config file: " << oss.str() << endl;
      file.close();
      return false;
    }

  if( with_comment )
    {
      // Skip the rest of this line
      char comment[128];
      file.getline( comment, 128 );
    }

  if( byteval < 0 || byteval > 0xFF )
    {
      std::ostringstream oss;
      oss << "invalid byte value 0x" << std::hex << std::setfill('0')
          << std::setw(2) << byteval
          << " at line " << line_nr
          << " addr=0x" << std::setw(3) << regaddr;
      cout << "### Config file: " << oss.str() << endl;
      file.close();
      return false;
    }

  return true;
}

// ----------------------------------------------------------------------------

int compareIcConfigFile( uint8_t *regarray, int size,
                         bool is_lpgbt, bool is_lpgbt_v1,
                         const std::string &filename,
                         int *nbytes_file )
{
  *nbytes_file = 0;

  // Determine if file can be opened and whether it contains a single
  // byte value per line, or register address followed by byte value
  // (all hexadecimal), and optionally a comment
  bool with_addr = false;
  bool with_comment = false;
  if( !probeIcConfigFile(filename, &with_addr, &with_comment) )
    return 0;

  // Read file contents (ASCII)
  std::ifstream file( filename );
  uint32_t number_of_regs = 0;
  uint32_t number_of_diffs = 0;
  uint32_t line_nr = 1;
  uint32_t regaddr = 0;
  uint32_t byteval;
  file >> std::hex; // Values are hexadecimal
  while( nextFromIcConfigFile( file, line_nr, regaddr, byteval,
                               with_addr, with_comment ) )
    {
      // Compare value in file with contents of 'regarray'
      uint8_t byt = (uint8_t) (byteval & 0xFF);
      if( byt != regarray[regaddr] )
        {
          cout << std::hex << std::setfill('0')
               << "### Reg 0x" << setw(3) << regaddr << " ("
               << std::dec << std::setfill(' ')
               << std::setw(3) << regaddr << "): "
               << std::hex << setfill('0')
               << " " << setw(2) << (uint32_t) regarray[regaddr]
               << ", in file: " << setw(2) << (uint32_t) byt;
          if( is_lpgbt )
            {
              // Display the register name:
              // find the first item corresponding to the given address
              // (which should be the full register, not its subfields)
              const lpgbt_item_t *it;
              if( is_lpgbt_v1 )
                it = &LPGBTv1_ITEM[0];
              else
                it = &LPGBTv0_ITEM[0];
              while( strlen(it->name) != 0 )
                {
                  if( (uint32_t) it->addr == regaddr )
                    // Found it
                    break;
                  ++it;
                }
              if( strlen(it->name) != 0 )
                cout << " (" << it->name << ")";
            }
          cout << endl;
          ++number_of_diffs;
        }

      if( !with_addr )
        ++regaddr;
      ++number_of_regs;
      ++line_nr;
    }
  file.close();
  cout << std::dec << std::setfill( ' ' );

  *nbytes_file = number_of_regs; // Number of byte values in file
  return number_of_diffs; // Number of differences found
}

// ----------------------------------------------------------------------------
// Secondary lpGBT register read/write via a primary lpGBT I2C Master
// ----------------------------------------------------------------------------

#include "lpgbt-regmap/lpgbt-regs-v1.h"

bool writeIcRegsViaLpgbtI2c( FlxUpload *fup, FlxReceiver *frecvr,
                             int linknr, int i2c_addr,
                             int i2c_master, int i2c_freq, int i2c_addr_slave,
                             bool i2c_configure,
                             int reg_addr, int nbytes, uint8_t *reg_vals,
                             bool use_ec, bool single_dma,
                             bool debug, bool display )
{
  // (This function for lpGBT-v1 only)
  int addr;
  uint8_t val;
  bool lpgbtv1 = true;

  if( i2c_configure )
    {
      // Configure the I2CCtrl register to write the register address (2 bytes)
      // plus the register bytes (nbytes)
      if( 2+nbytes > 16 )
        {
          cout << "### lpGBT-via-I2C rd/wr op: 16-2=14 registers max per op\n";
          return false;
        }
      configI2c( fup, frecvr, linknr, i2c_addr,
                 i2c_master, i2c_freq, i2c_addr_slave, 2+nbytes,
                 use_ec, lpgbtv1, single_dma );
    }

  // Assemble the I2C frame for the slave lpGBT register write operation
  uint8_t i2c_frame[16];
  i2c_frame[0] = (uint8_t) (reg_addr & 0xFF);
  i2c_frame[1] = (uint8_t) ((reg_addr>>8) & 0xFF);
  for( int i=0; i<nbytes; ++i )
    i2c_frame[2+i] = reg_vals[i]; // Register bytes
  for( int i=(2+nbytes); i<16; ++i )
    i2c_frame[i] = (uint8_t) 0x00;

  // Write the assembled frame to the master lpGBT I2C data registers
  for( int i=0; i<(2+nbytes); i+=4 )
    {
      // Write 4 bytes per operation
      addr = I2CM0DATA0_V1 + 7*i2c_master;
      if( single_dma )
        fup->addIcWriteFrame( i2c_addr, addr,
                              4, &i2c_frame[i], lpgbtv1 );
      else
        fup->writeIcChannel( linknr, i2c_addr, addr,
                             4, &i2c_frame[i], use_ec, lpgbtv1 );

      // Store in I2C internal data registers
      addr = I2CM0CMD_V1 + 7*i2c_master;
      val = 8 + (i/4); // I2C_W_MULTI_4BYTE0,1,2,3
      if( single_dma )
        fup->addIcWriteFrame( i2c_addr, addr,
                              1, &val, lpgbtv1 );
      else
        fup->writeIcChannel( linknr, i2c_addr, addr,
                             1, &val, use_ec, lpgbtv1 );
    }

  // Execute the I2C write operation
  addr = I2CM0CMD_V1 + 7*i2c_master;
  val  = 0xC; // I2C_MULTI_WRITE
  if( single_dma )
    fup->addIcWriteFrame( i2c_addr, addr, 1, &val, lpgbtv1 );
  else
    fup->writeIcChannel( linknr, i2c_addr, addr, 1, &val, use_ec, lpgbtv1 );

  if( single_dma )
    {
      // Add a delay that should be sufficient for the I2C operation to complete:
      // I2C write 1+2+nbytes bytes (I2C address, 2 bytes reg address,
      // reg bytes) @100KHz ca. (1+2+nbytes) times 10us
      fup->addIcDelayUs( ((1+2+nbytes)*10*10)/(i2c_freq/100) );

      fup->uploadIcFrames( linknr, use_ec );
    }

  // Receive the replies of above operations and display on request
  //receiveIcReply( frecvr, linknr, 0, 0, 0, 8000,
  //                use_ec, lpgbtv1, debug, display );

  // Wait for the write operation to complete
  // (also receiving/absorbing replies from the operations above)
  // NB: I2C op is in progress as long as bits 2 (I2CMxSuccess),
  //     bit 3 (I2CMxLevelError) and bit 6 (I2CMxNoAck) are *not* set
  int cnt = 0;
  bool reply_received;
  addr = I2CM0STATUS_V1 + 0x15*i2c_master;
  uint8_t i2c_status = 0;
  while( (i2c_status & 0x4C) == 0 && cnt < 20 )
  //while( (i2c_status & 4) == 0 && cnt < 20 )
    {
      reply_received = readIcRegs( fup, frecvr,
                                   linknr, i2c_addr,
                                   addr, 1, &i2c_status,
                                   use_ec, lpgbtv1,
                                   debug, display );
      if( !reply_received )
        {
          cout << "### Reply (I2C status/done) not received "
               << "(slave lpGBT reg=0x" << hex << reg_addr << ")" << endl;
          break;
        }
      ++cnt;
    }
  if( cnt >= 20 )
    // If error bits set should reset the I2C Master?
    // (bit in register RST0)
    cout << "### Timeout on I2C success: I2CMxStatus=0x"
         << hex << (uint32_t) i2c_status << dec << endl;
  /*
  // Read I2C status and transaction count ?
  addr = I2CM0STATUS_V1 + 0x15*i2c_master;
  fup->readIcChannel( linknr, i2c_addr, addr, 2, use_ec, lpgbt_v1 );

  // Receive the reply (containing 2 bytes) and display
  reply_received = receiveIcReply( frecvr, linknr,
                                   addr, 2, i2c_frame, 1000,
                                   use_ec, lpgbtv1,
                                   debug, display );
  if( !reply_received && display )
    cout << "### Reply (I2C status,count) not received (addr=0x"
         << hex << addr << ")" << endl;
  */
  if( debug ) return true;
  return reply_received;
}

// ----------------------------------------------------------------------------

bool readIcRegsViaLpgbtI2c( FlxUpload *fup, FlxReceiver *frecvr,
                            int linknr, int i2c_addr,
                            int i2c_master, int i2c_freq, int i2c_addr_slave,
                            bool i2c_configure,
                            int reg_addr, int nbytes, uint8_t *reg_vals,
                            bool use_ec, bool single_dma,
                            bool debug, bool display )
{
  // (This function for lpGBT-v1 only)
  int addr;
  uint8_t val;
  bool lpgbtv1 = true;

  if( i2c_configure )
    {
      // Configure the I2CCtrl register to write the register address (2 bytes)
      configI2c( fup, frecvr, linknr, i2c_addr,
                 i2c_master, i2c_freq, i2c_addr_slave, 2,
                 use_ec, lpgbtv1, single_dma );
    }

  // Slave lpGBT register write I2C frame: the register address
  uint8_t i2c_frame[2];
  i2c_frame[0] = (uint8_t) (reg_addr & 0xFF);
  i2c_frame[1] = (uint8_t) ((reg_addr>>8) & 0xFF);

  // Write the 2-byte frame to the master lpGBT I2C data registers
  addr = I2CM0DATA0_V1 + 7*i2c_master;
  if( single_dma )
    fup->addIcWriteFrame( i2c_addr, addr, 2, i2c_frame, lpgbtv1 );
  else
    fup->writeIcChannel( linknr, i2c_addr, addr, 2, i2c_frame, use_ec, lpgbtv1 );

  // Store in I2C internal data registers
  addr = I2CM0CMD_V1 + 7*i2c_master;
  val  = 8; // I2C_W_MULTI_4BYTE0
  if( single_dma )
    fup->addIcWriteFrame( i2c_addr, addr, 1, &val, lpgbtv1 );
  else
    fup->writeIcChannel( linknr, i2c_addr, addr, 1, &val, use_ec, lpgbtv1 );

  // Execute the I2C (multi)write operation
  addr = I2CM0CMD_V1 + 7*i2c_master;
  val  = 0xC; // I2C_MULTI_WRITE
  if( single_dma )
    fup->addIcWriteFrame( i2c_addr, addr, 1, &val, lpgbtv1 );
  else
    fup->writeIcChannel( linknr, i2c_addr, addr, 1, &val, use_ec, lpgbtv1 );

  int cnt = 0;
  uint8_t i2c_status = 0;
  if( single_dma )
    {
      // Add a delay that should be sufficient for the I2C operation to complete:
      // I2C write 3 bytes (I2C address, 2 bytes reg address), ca. 3x9 bit times
      // @100KHz ca. 30 times 10us
      fup->addIcDelayUs( (3*10*10)/(i2c_freq/100) );

      // Single read of the I2C Master status register
      addr = I2CM0STATUS_V1 + 0x15*i2c_master;
      fup->addIcReadFrame( i2c_addr, addr, 1, lpgbtv1 );
    }
  else
    {
      // Receive the replies of above operations and display only on request
      //receiveIcReply( frecvr, linknr, 0, 0, 0, 8000, false, true, debug, display );
      // Wait for the write operation to complete
      // (also receiving/absorbing replies from above operations)
      // NB: I2C op is in progress as long as bits 2 (I2CMxSuccess),
      //     bit 3 (I2CMxLevelError) and bit 6 (I2CMxNoAck) are *not* set
      addr = I2CM0STATUS_V1 + 0x15*i2c_master;
      while( (i2c_status & 0x4C) == 0 && cnt < 20 )
      //while( (i2c_status & 4) == 0 && cnt < 20 )
        {
          bool reply_received = readIcRegs( fup, frecvr,
                                            linknr, i2c_addr,
                                            addr, 1, &i2c_status,
                                            use_ec, lpgbtv1,
                                            debug, display );
          if( !reply_received )
            {
              cout << "### Reply (I2C status/done) not received "
                   << "(slave lpGBT reg=0x" << hex << reg_addr << ")" << endl;
              break;
            }
          ++cnt;
        }
      if( cnt >= 20 )
        // If error bits set should reset the I2C Master?
        // (bit in register RST0)
        cout << "### Timeout on I2C success: I2CMxStatus=0x"
             << hex << (uint32_t) i2c_status << dec << endl;
    }

  if( nbytes == 1 )
    {
      // Execute a 1-byte I2C read operation
      addr = I2CM0CMD_V1 + 7*i2c_master;
      val  = 0x3; // I2C_1BYTE_READ
      if( single_dma )
        fup->addIcWriteFrame( i2c_addr, addr, 1, &val, lpgbtv1 );
      else
        fup->writeIcChannel( linknr, i2c_addr, addr, 1, &val, use_ec, lpgbtv1 );
    }
  else
    {
      // Reconfigure the I2CCtrl register to read the requested number of bytes
      configI2c( fup, frecvr, linknr, i2c_addr,
                 i2c_master, i2c_freq, i2c_addr_slave, nbytes,
                 use_ec, lpgbtv1, single_dma );

      // Execute the I2C (multi)read operation
      addr = I2CM0CMD_V1 + 7*i2c_master;
      val  = 0xD; // I2C_MULTI_READ
      if( single_dma )
        fup->addIcWriteFrame( i2c_addr, addr, 1, &val, lpgbtv1 );
      else
        fup->writeIcChannel( linknr, i2c_addr, addr, 1, &val, use_ec, lpgbtv1 );
    }

  if( single_dma )
    {
      // Add a delay that should be sufficient for the I2C operation to complete:
      // I2C write 1+'nbytes' bytes (I2C address plus data), ca. (1+nbytes) x 9 bit times
      // @100KHz ca. (nbytes+1)*10 times 10us
      fup->addIcDelayUs( ((1+nbytes)*10*10)/(i2c_freq/100) );

      // Single read of the I2C Master status register
      addr = I2CM0STATUS_V1 + 0x15*i2c_master;
      fup->addIcReadFrame( i2c_addr, addr, 1, lpgbtv1 );
    }
  else
    {
      // Receive the replies of above operations and display only on request
      //receiveIcReply( frecvr, linknr, 0, 0, 0, 8000, use_ec, lpgbtv1, debug, display );

      // Wait for the read operation to complete
      // (also receiving/absorbing replies from above operation)
      // NB: I2C op is in progress as long as bits 2 (I2CMxSuccess),
      //     bit 3 (I2CMxLevelError) and bit 6 (I2CMxNoAck) are *not* set
      cnt = 0;
      addr = I2CM0STATUS_V1 + 0x15*i2c_master;
      i2c_status = 0;
      while( (i2c_status & 0x4C) == 0 && cnt < 20 )
      //while( (i2c_status & 4) == 0 && cnt < 20 )
        {
          bool reply_received = readIcRegs( fup, frecvr,
                                            linknr, i2c_addr,
                                            addr, 1, &i2c_status,
                                            use_ec, lpgbtv1,
                                            debug, display );
          if( !reply_received )
            {
              cout << "### Reply (I2C status/done) not received "
                   << "(slave lpGBT reg=0x" << hex << reg_addr << ")" << endl;
              break;
            }
          ++cnt;
        }
      if( cnt >= 20 )
        // If error bits set should reset the I2C Master?
        // (bit in register RST0)
        cout << "### Timeout on I2C success: I2CMxStatus=0x"
             << hex << (uint32_t) i2c_status << dec << endl;
    }

  // Read the slave lpGBT register bytes from the I2C data registers
  // (### Stored starting from I2CMxREAD15, in reversed order:
  //      mistake by the I2C Master designers?)
  if( nbytes == 1 )
    addr = I2CM0READBYTE_V1 + 0x15*i2c_master;
  else
    addr = (I2CM0READ15_V1 - nbytes+1) + 0x15*i2c_master;
  if( single_dma )
    fup->addIcReadFrame( i2c_addr, addr, nbytes, lpgbtv1 );
  else
    fup->readIcChannel( linknr, i2c_addr, addr, nbytes, use_ec, lpgbtv1 );

  if( single_dma )
    {
      fup->uploadIcFrames( linknr, use_ec );

      // Should check I2C status in replies, from 2 operations,
      // before reading I2C data bytes...
      //...
    }

  // Receive the reply (containing nbytes) and display
  bool reply_received = receiveIcReply( frecvr, linknr,
                                        addr, nbytes, reg_vals, 4000,
                                        use_ec, lpgbtv1,
                                        debug, display );
  if( !reply_received && display )
    cout << "### Reply (I2C data bytes) not received (addr=0x"
         << hex << addr << ")" << dec << endl;

  // Reverse the order of the bytes...
  for( int i=0; i<nbytes/2; ++i )
    {
      uint8_t tmp = reg_vals[i];
      reg_vals[i] = reg_vals[nbytes-1-i];
      reg_vals[nbytes-1-i] = tmp;
    }

  if( debug ) return true;
  return reply_received;
}

// ----------------------------------------------------------------------------

void writeIcConfigFileViaLpgbtI2c( FlxUpload *fup, FlxReceiver *frecvr,
                                   int linknr, int i2c_addr,
                                   int i2c_master, int i2c_freq, int i2c_addr_slave,
                                   const std::string &filename,
                                   int *nbytes_file,
                                   bool use_ec )
{
  *nbytes_file = 0;

  // Determine if file can be opened and whether it contains a single
  // byte value per line, or register address followed by byte value
  // (all hexadecimal), and optionally a comment
  bool with_addr = false;
  bool with_comment = false;
  if( !probeIcConfigFile(filename, &with_addr, &with_comment) )
    return;

  // Read file contents (ASCII)
  std::ifstream file( filename );
  char comment[128];
  uint32_t number_of_regs = 0;
  uint32_t line_nr = 1;
  uint32_t regaddr = 0;
  uint32_t byteval;
  bool i2c_configure = true; // Configure once..
  file >> std::hex; // Values are hexadecimal
  while( !file.eof() )
    {
      // Next value or address/value pair
      if( with_addr )
        {
          file >> regaddr;
          if( file.eof() )
            break;
        }
      file >> byteval;

      if( file.fail() )
        {
          if( file.eof() )
            break;

          std::ostringstream oss;
          oss << "illegal value at line " << line_nr
              << " addr=0x" << std::hex << std::setfill('0')
              << std::setw(3) << regaddr;
          cout << "### Config file: " << oss.str() << endl;
          file.close();
          return;
        }

      if( with_comment )
        // Skip the rest of this line
        file.getline( comment, 128 );

      if( byteval < 0 || byteval > 0xFF )
        {
          std::ostringstream oss;
          oss << "invalid byte value 0x" << std::hex << std::setfill('0')
              << std::setw(2) << byteval
              << " at line " << line_nr
              << " addr=0x" << std::setw(3) << regaddr;
          cout << "### Config file: " << oss.str() << endl;
          file.close();
          return;
        }

      // Upload one register at a time
      uint8_t byt = (uint8_t) (byteval & 0xFF);
      writeIcRegsViaLpgbtI2c( fup, frecvr, linknr, i2c_addr,
                              i2c_master, i2c_freq, i2c_addr_slave, i2c_configure,
                              regaddr, 1, &byt, use_ec, false, false, false );
      i2c_configure = false;

      if( !with_addr )
        ++regaddr;
      ++number_of_regs;
      ++line_nr;
    }
  file.close();
  *nbytes_file = number_of_regs; // Number of byte values in file
}

// ----------------------------------------------------------------------------

void configI2c( FlxUpload *fup, FlxReceiver *frecvr,
                int linknr, int i2c_addr,
                int i2c_master, int i2c_freq,
                int i2c_addr_slave, int nbytes,
                bool use_ec, bool lpgbt_v1,
                bool single_dma )
{
  // Configure the I2CCtrl register
  int addr = I2CM0DATA0_V1 + 7*i2c_master;
  uint8_t val = 0; // 100 KHz

  if( i2c_freq == 200 )
    val |= 1;
  else if( i2c_freq == 400 )
    val |= 2;
  else if( i2c_freq == 1000 )
    val |= 3;
  // Going to read/write 'nbytes'
  val |= ((uint8_t) nbytes << 2);
  if( single_dma )
    fup->addIcWriteFrame( i2c_addr, addr, 1, &val, lpgbt_v1 );
  else
    fup->writeIcChannel( linknr, i2c_addr, addr, 1, &val, use_ec, lpgbt_v1 );

  // Write the I2CCtrl register
  addr = I2CM0CMD_V1 + 7*i2c_master;
  val  = 0x0; // I2C_WRITE_CR
  if( single_dma )
    fup->addIcWriteFrame( i2c_addr, addr, 1, &val, lpgbt_v1 );
  else
    fup->writeIcChannel( linknr, i2c_addr, addr, 1, &val, use_ec, lpgbt_v1 );

  // Configure the slave lpGBT I2C address
  addr = I2CM0ADDRESS_V1 + 7*i2c_master;
  val  = (uint8_t) i2c_addr_slave;
  if( single_dma )
    fup->addIcWriteFrame( i2c_addr, addr, 1, &val, lpgbt_v1 );
  else
    fup->writeIcChannel( linknr, i2c_addr, addr, 1, &val, use_ec, lpgbt_v1 );

  // Receive the replies of above operations and display only on request
  //receiveIcReply( frecvr, linknr, 0, 0, 0, 500, use_ec, lpgbt_v1 );
}

// ----------------------------------------------------------------------------

void enableI2cPullups( FlxUpload *fup, FlxReceiver *frecvr,
                       int linknr, int i2c_addr,
                       int i2c_master, bool enable,
                       bool use_ec, bool lpgbt_v1,
                       bool single_dma )
{
  int addr = I2CM0CONFIG_V1 + 7*i2c_master;
  uint8_t val;

  if( enable )
    val = 0x10 | 0x40;
  else
    val = 0;
  if( single_dma )
    fup->addIcWriteFrame( i2c_addr, addr, 1, &val, lpgbt_v1 );
  else
    fup->writeIcChannel( linknr, i2c_addr, addr, 1, &val, use_ec, lpgbt_v1 );

  // Receive the replies of above operations and display only on request
  //receiveIcReply( frecvr, linknr, 0, 0, 0, 500, use_ec, lpgbt_v1 );
}

// ----------------------------------------------------------------------------
