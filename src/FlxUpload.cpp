#include <string.h> // for memset()
#include <unistd.h> // for usleep()
#include <time.h>   // for nanosleep()

#include "FlxUpload.h"
#include "flxcard/FlxException.h"
#include "EmuDataGenerator.h"
#include "crc.h"

//#define USE_LIB_CMEM

#ifndef USE_LIB_CMEM
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#endif

#include <iostream>
#include <iomanip>
#include <sstream> // for ostringstream
#include <fstream> // for ifstream
using namespace std;

#include "nlohmann/json.hpp"
using namespace nlohmann;
// NOTE: Ignore the deprecated-declarations warning of gcc13, see FLX-2310
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include "yaml-cpp/yaml.h"
#pragma GCC diagnostic pop

// ----------------------------------------------------------------------------

FlxUpload::FlxUpload( int card_nr,
                      i64 buffer_size,
                      int dma_index,
                      int hdlc_seq_nr )
  : _flx( 0 ),
    _cardNr( card_nr ),
    _blockSize( -1 ),
    _bar2( 0 ),
    _hdlcDelaySupport( false ),
    _mapElink( true ),
    _fromHostFormat( FH_FORMAT_REGMAP4 ),
    _toFlxHeaderSize( 2 ),
    _toFlxBlockBytes( 32 ),
    _toFlxPayloadBytes( 30 ),
    _toFlxLengthMask( TOFLX_LENGTH_MASK ),
    _toFlxElinkShift( TOFLX_ELINK_SHIFT ),
    _toFlxHeaderFields8Bit( false ),
    _bufferSize( 0 ),
    _bufferSizeOrg( 0 ),
    _dataBuffer( 0 ),
    _cmemHandle( -1 ),
    _cmemStartAddr( 0 ),
    _cmemEndAddr( 0 ),
    _dmaIndex( dma_index ),
    _dmaCircular( false ),
    _dmaTlp( 0 ),
    _dmaSoftwPtr( 0 ),
    _bar0( 0 ),
    _dmaDesc( 0 ),
    _dmaStat( 0 ),
    _bytesToUpload( 0 ),
    //_uploadDmaSize( 8*1024 ), // Do not exceed 16KB -> data corruption
    // Corruption at > 1024 (10 Oct 2017)
    //_uploadDmaSize( 1024 ),
    _uploadDmaSize( 0 ), // 'Unlimited' (12 Apr 2022)
    _uploadIndex( 0 ),
    _writeIndex( 0 ),
    _timestamp( 0 ),
    _fillerByte( 0 ),
    _hdlcSeqNr( hdlc_seq_nr ),
    _pbChunkCntr( 0 ),
    _pbBlockCntr( 0 ),
    _pbRandomData( false )
{
  // A buffer size of 0 also selects the default size
  if( buffer_size == 0 ) buffer_size = DFLT_SEND_BUF_SIZE;

  // Allocate a (continuous) buffer of the requested size
  _dataBuffer = this->cmemAllocate( buffer_size, card_nr );

  if( _dataBuffer )
    {
      // Initialize the buffer
      memset( static_cast<void *> (_dataBuffer), 0xFF, buffer_size );
      _bufferSize = buffer_size;
      _bufferSizeOrg = _bufferSize;
    }
  else
    {
      if( !_errString.empty() ) _errString += "; ";
      _errString += "Failed to allocate requested buffer size";
    }

  // Open FELIX FLX-card
  _flx = new FlxCard;
  try {
    uint32_t lockbits = LOCK_NONE;
    // Backwards compatibility
    if( dma_index == 0 )
      lockbits |= LOCK_DMA0;
    else if( dma_index == 1 )
      lockbits |= LOCK_DMA1;
    // New lockbits
    if( dma_index > -1 )
      lockbits |= ((1 << dma_index) << 16);

    _flx->card_open( _cardNr, lockbits );

    // Find DMA index to use?
    if( dma_index == -1 )
      {
        // DMA controller index for FromHost data
        dma_index = _flx->cfg_get_option(BF_GENERIC_CONSTANTS_DESCRIPTORS) - 1;
        lockbits |= ((1 << dma_index) << 16); // New lockbit
        // Backwards compatibility
        if( dma_index == 0 )
          lockbits |= LOCK_DMA0;
        else if( dma_index == 1 )
          lockbits |= LOCK_DMA1;

        // Reopen device with correct lockbits
        _flx->card_close();
        _flx->card_open( _cardNr, lockbits );
      }
    _dmaIndex = dma_index;
  }
  catch( FlxException &ex ) {
    if( !_errString.empty() ) _errString += "; ";
    _errString += "FlxCard open: ";
    _errString += ex.what();
    delete _flx;
    _flx = 0;
    return;
  }

  // Get a pointer to the BAR2 registers
  _bar2 = (flxcard_bar2_regs_t *) _flx->bar2Address();

  // Get a pointer to the BAR0 registers
  u64 bar0_addr = _flx->bar0Address();
  _bar0 = (volatile flxcard_bar0_regs_t *) bar0_addr;
  _dmaDesc = &_bar0->DMA_DESC[dma_index];
  _dmaStat = &_bar0->DMA_DESC_STATUS[dma_index];

  this->dmaStop();
  // Force circular DMA inactive state initially
  // to prevent hanging in upload(): dmaActive()
  _dmaSoftwPtr = _dmaStat->fw_pointer;

  // Get this firmware's to-host data blocksize and trailer format
  _blockSize = _bar2->BLOCKSIZE;
  if( _blockSize == 0 ) // Backwards compatibility!
    _blockSize = 1024;
  _blockSizeCode  = (_blockSize/1024-1) << 8;
  _trailerFmt.setTrailerIs32Bit( (_bar2->CHUNK_TRAILER_32B == 1) );

#if REGMAP_VERSION >= 0x500
  _fromHostFormat = _bar2->FROMHOST_DATA_FORMAT;
  if( _fromHostFormat == FH_FORMAT_REGMAP4 )
    {
      _toFlxHeaderSize   = 2;
      _toFlxBlockBytes   = 32;
      _toFlxPayloadBytes = 30;
      _toFlxLengthMask   = TOFLX_LENGTH_MASK;
      _toFlxElinkShift   = TOFLX_ELINK_SHIFT;
    }
  else if( _fromHostFormat == FH_FORMAT_5BIT_LENGTH )
    {
      _toFlxHeaderSize   = 2;
      _toFlxBlockBytes   = 32;
      _toFlxPayloadBytes = 30;
      _toFlxLengthMask   = TOFLX_LENGTH_MASK_5BIT;
      _toFlxElinkShift   = TOFLX_ELINK_SHIFT;
    }
  else if( _fromHostFormat == FH_FORMAT_HDR32_PACKET32 )
    {
      _toFlxHeaderSize   = 4;
      _toFlxBlockBytes   = 32;
      _toFlxPayloadBytes = 28;
      _toFlxLengthMask   = TOFLX_LENGTH_MASK_HDR32;
      _toFlxElinkShift   = TOFLX_ELINK_SHIFT_HDR32;
    }
  else if( _fromHostFormat == FH_FORMAT_HDR32_PACKET64 )
    {
      _toFlxHeaderSize   = 4;
      _toFlxBlockBytes   = 64;
      _toFlxPayloadBytes = 60;
      _toFlxLengthMask   = TOFLX_LENGTH_MASK_HDR32;
      _toFlxElinkShift   = TOFLX_ELINK_SHIFT_HDR32;
    }
  else if( _fromHostFormat == FH_FORMAT_HDR32_PACKET32_8B ||
           _fromHostFormat == FH_FORMAT_HDR32_PACKET64_8B ||
           _fromHostFormat == FH_FORMAT_HDR32_PACKET128_8B )
    {
      _toFlxHeaderFields8Bit = true;
      _toFlxHeaderSize = 4;
      if( _fromHostFormat == FH_FORMAT_HDR32_PACKET32_8B )
        _toFlxBlockBytes = 32;
      else if( _fromHostFormat == FH_FORMAT_HDR32_PACKET64_8B )
        _toFlxBlockBytes = 64;
      else
        _toFlxBlockBytes = 128;
      _toFlxPayloadBytes = _toFlxBlockBytes - 4;
      _toFlxLengthMask   = TOFLX_LENGTH_MASK_HDR32_8B;
      _toFlxElinkShift   = TOFLX_ELINK_SHIFT_HDR32_8B;
    }

  bool b = (_bar2->SUPPORT_HDLC_DELAY == 1);
  this->setHdlcDelaySupport( b );
#endif // REGMAP_VERSION

  _minimumUsleep = this->minimumUsleep();
}

// ----------------------------------------------------------------------------

FlxUpload::~FlxUpload()
{
  if( _pbFile.is_open() ) _pbFile.close();

  // Close the FLX-card
  try {
    if( _flx )
      _flx->card_close();
  }
  catch( FlxException &ex ) {
    // Do something..?
  }
  delete _flx;

  // Free the receive buffer and close CMEM(_RCC)
#ifdef USE_LIB_CMEM
  int ret = CMEM_SegmentFree( _cmemHandle );
  if( ret == CMEM_RCC_SUCCESS )
    ret = CMEM_Close();
  if( ret != CMEM_RCC_SUCCESS )
    rcc_error_print( stdout, ret );

#else
  if( _dataBuffer )
    {
      munmap( (void *) _cmemDescriptor.uaddr, _cmemDescriptor.size );
      ioctl( _cmemHandle, CMEM_RCC_FREE, &_cmemDescriptor.handle );
      close( _cmemHandle );
    }
#endif // USE_LIB_CMEM
}

// ----------------------------------------------------------------------------

void FlxUpload::stop()
{
  this->dmaStop();
}

// ----------------------------------------------------------------------------

std::string FlxUpload::errorString()
{
  if( _errString.empty() )
    return std::string( "" );

  std::ostringstream oss;
  oss << "FlxUpload#" << _cardNr << ": " << _errString;

  // Clear the error string
  _errString.clear();

  return oss.str();
}

// ----------------------------------------------------------------------------

bool FlxUpload::hasError()
{
  return !_errString.empty();
}

// ----------------------------------------------------------------------------

std::string FlxUpload::debugString()
{
  std::ostringstream oss;
  oss << "FlxUpload" << _cardNr << " DEBUG: "
      << std::hex << setfill('0')
      << "start=" << std::setw(10) << _cmemStartAddr
      << ", end=" << std::setw(10) << _cmemEndAddr
      << ", psoftw=" << std::setw(10) << _dmaDesc->sw_pointer
      << ", pfirmw=" << std::setw(10) << _dmaStat->fw_pointer
      << ", even-PC/DMA " << _dmaStat->even_addr_pc
      << std::dec << " " << _dmaStat->even_addr_dma
      << std::endl;
    return oss.str();
}

// ----------------------------------------------------------------------------

void FlxUpload::setFanOutForDaq()
{
  if( _flx == 0 ) return;

  // (Set "downlink fanout select")
  bool locked = this->fanOutIsLocked();
  if( !locked )
    {
      _bar2->GBT_TOFRONTEND_FANOUT.SEL = 0;
      _bar2->GBT_TOHOST_FANOUT.SEL = 0;
    }
}

// ----------------------------------------------------------------------------

bool FlxUpload::fanOutIsLocked()
{
  if( _flx == 0 ) return false;

  // Check for the FANOUT_LOCK bit(s)
  bool locked = false;
  if( _bar2->GBT_TOHOST_FANOUT.LOCK == 1 )
    locked = true;
  if( _bar2->GBT_TOFRONTEND_FANOUT.LOCK == 1 )
    locked = true;
  return locked;
}

// ----------------------------------------------------------------------------

void FlxUpload::setBufferSize( i64 sz )
{
  // This is special and normally not called (buffer size is set in c'tor):
  // it is used to set the DMA buffer size for trickle DMA
  // which has to match exactly the prepared data blocks
  // (but it cannot exceed the originally allocated 'cmem' buffer size)
  if( sz <= _bufferSizeOrg )
    _bufferSize = sz;
  else
    _bufferSize = _bufferSizeOrg;
}

// ----------------------------------------------------------------------------

int FlxUpload::prepareData( int elink_count, int *elink,
                            int size, int pattern_id, int repeat )
{
  for( int i=0; i<elink_count; ++i )
    elink[i] = mapElink( elink[i] );

  // Create the test data bytes according to a pattern identifier
  // to be uploaded to 'elink_count' E-links defined in array 'elink'
  char data[256];
  int  i;
  if( pattern_id == 1 )
    {
      // 0x55-0xAA pattern
      for( i=0; i<256; ++i )
        if( i & 1 )
          data[i] = (char) 0xAA;
        else
          data[i] = (char) 0x55;
    }
  else if( pattern_id == 2 )
    {
      // 0xFF only
      for( i=0; i<256; ++i )
        data[i] = (char) 0xFF;
    }
  else if( pattern_id == 3 )
    {
      // See below..
    }
  else
    {
      // Incrementing pattern
      for( i=0; i<256; ++i )
        data[i] = (char) (i & 0xFF);
    }

  // Format and write 'size' bytes of test data to the DMA buffer,
  // for each E-link in turn, and this 'repeat' times
  int  cnt, lnk, blocks = 0; //j, data_i = 0;
  i64  buf_i = _writeIndex;
  bool is_delay_chunk = false;
  bool use_byteval = (pattern_id == 3);
  for( cnt=0; cnt<repeat; ++cnt )
    {
      for( lnk=0; lnk<elink_count; ++lnk )
        {
          blocks += prepareBlocks( elink[lnk], size, (uint8_t *) data, 256,
          //                       &_writeIndex, is_delay_chunk, use_byteval, cnt );
                                   &buf_i, is_delay_chunk, use_byteval, cnt );
          /*
          int hdr, final_bytes, final_size, final_hdr;
          blocks = (size + _toFlxPayloadBytes-1) / _toFlxPayloadBytes;
          final_bytes = size - (blocks-1) * _toFlxPayloadBytes;
          if( _fromHostFormat != FH_FORMAT_REGMAP4 )
            {
              hdr = ((elink[lnk] << _toFlxElinkShift) | _toFlxLengthMask);
              final_hdr  = hdr & ~_toFlxLengthMask;
              final_hdr |= final_bytes; // Length in bytes
            }
          else
            {
              hdr = ((elink[lnk] << _toFlxElinkShift) |
                     ((_toFlxPayloadBytes/2) << TOFLX_LENGTH_SHIFT));
              final_size = (final_bytes + 1) / 2; // Length in 2-byte units
              final_hdr  = hdr & ~_toFlxLengthMask;
              final_hdr |= ((final_size << TOFLX_LENGTH_SHIFT)|TOFLX_EOM_MASK);
            }

          data_i = 0;

          // Fully-filled FromHost byte blocks
          for( i=0; i<blocks-1; ++i )
            {
              if( pattern_id == 3 )
                {
                  // Fill the block with a constant,
                  // incrementing the constant for each subsequent chunk
                  for( j=_toFlxPayloadBytes-1; j>=0; --j, ++buf_i )
                    _dataBuffer[buf_i] = (char) (cnt & 0xFF);
                }
              else
                {
                  for( j=_toFlxPayloadBytes-1; j>=0; --j, ++buf_i )
                    _dataBuffer[buf_i] = data[(data_i + j) & 0xFF];
                }
              data_i += _toFlxPayloadBytes;
              for( int j=0; j<_toFlxHeaderSize; ++j, ++buf_i )
                _dataBuffer[buf_i] = (char) ((hdr >> j*8) & 0xFF);
              if( buf_i >= _bufferSize ) buf_i = 0;
            }

          // Final (possibly not fully filled) FromHost byte block
          for( j=_toFlxPayloadBytes-1; j>=final_bytes; --j, ++buf_i )
            _dataBuffer[buf_i] = _fillerByte; // 'Empty' block bytes
          if( pattern_id == 3 )
            {
              // Final data bytes
              for( j=final_bytes-1; j>=0; --j, ++buf_i )
                _dataBuffer[buf_i] = (char) (cnt & 0xFF);
            }
          else
            {
              // Final data bytes
              for( j=final_bytes-1; j>=0; --j, ++buf_i )
                _dataBuffer[buf_i] = data[(data_i + j) & 0xFF];
            }
          for( int j=0; j<_toFlxHeaderSize; ++j, ++buf_i )
            _dataBuffer[buf_i] = (char) ((final_hdr >> j*8) & 0xFF);
          if( buf_i >= _bufferSize ) buf_i = 0;
          */
        }
    }

  _bytesToUpload = blocks * _toFlxBlockBytes;
  _writeIndex = buf_i;
  //_uploadIndex = 0;

  return blocks * _toFlxBlockBytes;
}

// ----------------------------------------------------------------------------

int FlxUpload::prepareData( int elink, int size, int pattern_id, int repeat )
{
  elink = mapElink( elink );

  // Create the test data bytes according to a pattern identifier
  // to be uploaded to the E-link defined by the given parameters
  char data[256];
  int  i;
  if( pattern_id == 1 )
    {
      // 0x55-0xAA pattern
      for( i=0; i<256; ++i )
        if( i & 1 )
          data[i] = (char) 0xAA;
        else
          data[i] = (char) 0x55;
    }
  else if( pattern_id == 2 )
    {
      // 0xFF only
      for( i=0; i<256; ++i )
        data[i] = (char) 0xFF;
    }
  else if( pattern_id == 3 )
    {
      // See below..
    }
  else if( (pattern_id & 0xFF) == 4 )
    {
      // Given byte value (2nd byte of 'pattern_id') only
      char byt = (char) ((pattern_id >> 8) & 0xFF);
      for( i=0; i<256; ++i )
        data[i] = byt;
    }
  else
    {
      // Incrementing pattern
      for( i=0; i<256; ++i )
        data[i] = (char) (i & 0xFF);
    }

  // Format and write 'size' bytes of test data to the DMA buffer,
  // 'repeat' times
  /*
  int hdr, blocks, final_bytes, final_size, final_hdr;
  blocks = (size + _toFlxPayloadBytes-1) / _toFlxPayloadBytes;
  final_bytes = size - (blocks-1) * _toFlxPayloadBytes;
  if( _fromHostFormat != FH_FORMAT_REGMAP4 )
    {
      hdr = ((elink << _toFlxElinkShift) | _toFlxLengthMask);
      final_hdr  = hdr & ~_toFlxLengthMask;
      final_hdr |= final_bytes; // Length in bytes
    }
  else
    {
      hdr = ((elink << _toFlxElinkShift) |
             ((_toFlxPayloadBytes/2) << TOFLX_LENGTH_SHIFT));
      final_size = (final_bytes + 1) / 2; // Length in 2-byte units
      final_hdr  = hdr & ~_toFlxLengthMask;
      final_hdr |= ((final_size << TOFLX_LENGTH_SHIFT) | TOFLX_EOM_MASK);
    }
    */
  //int data_i, j;
  int blocks = 0;
  i64 buf_i = _writeIndex;
  //std::cout << hex << "writeIndex=" << _writeIndex << dec << endl;
  bool is_delay_chunk = false;
  bool use_byteval = (pattern_id == 3);
  for( int cnt=0; cnt<repeat; ++cnt )
    {
      blocks += prepareBlocks( elink, size, (uint8_t *) data, 256,
                               //&_writeIndex, is_delay_chunk, use_byteval, cnt );
                               &buf_i, is_delay_chunk, use_byteval, cnt );
      /*
      data_i = 0;
      // Fully-filled FromHost byte blocks
      for( i=0; i<blocks-1; ++i )
        {
          if( pattern_id == 3 )
            {
              // Fill the block with a constant,
              // incrementing the constant for each subsequent chunk
              for( j=_toFlxPayloadBytes-1; j>=0; --j, ++buf_i )
                _dataBuffer[buf_i] = (char) (cnt & 0xFF);
            }
          else
            {
              for( j=_toFlxPayloadBytes-1; j>=0; --j, ++buf_i )
                _dataBuffer[buf_i] = data[(data_i + j) & 0xFF];
            }
          data_i += _toFlxPayloadBytes;
          for( int j=0; j<_toFlxHeaderSize; ++j, ++buf_i )
            _dataBuffer[buf_i] = (char) ((hdr >> j*8) & 0xFF);

          // Inverted byte order:
          //for( int j=0; j<_toFlxHeaderSize; ++j, ++buf_i )
          //  _dataBuffer[buf_i] = (char) ((hdr >> j*8) & 0xFF);
          //for( j=0; j<_toFlxPayloadBytes; ++j, ++buf_i, ++data_i )
          //  _dataBuffer[buf_i] = data[data_i & 0xFF];

          if( buf_i >= _bufferSize ) buf_i = 0;
        }

      // Final (possibly not fully filled) FromHost byte block
      //for( j=_toFlxPayloadBytes-1; j>=final_size*2; --j, ++buf_i )
      for( j=_toFlxPayloadBytes-1; j>=final_bytes; --j, ++buf_i )
        _dataBuffer[buf_i] = _fillerByte; // 'Empty' block bytes
      if( pattern_id == 3 )
        {
          for( j=final_bytes-1; j>=0; --j, ++buf_i )
            _dataBuffer[buf_i] = (char) (cnt & 0xFF); // Final data bytes
        }
      else
        {
          for( j=final_bytes-1; j>=0; --j, ++buf_i )
            _dataBuffer[buf_i] = data[(data_i + j) & 0xFF]; // Final data bytes
        }
      for( int j=0; j<_toFlxHeaderSize; ++j, ++buf_i )
        _dataBuffer[buf_i] = (char) ((final_hdr >> j*8) & 0xFF);

      // Inverted byte order:
      //for( int j=0; j<_toFlxHeaderSize; ++j, ++buf_i )
      //  _dataBuffer[buf_i] = (char) ((hdr >> j*8) & 0xFF);
      //for( j=0; j<size*2; ++j, ++buf_i, ++data_i )
      //  _dataBuffer[buf_i] = data[data_i & 0xFF]; // Final data bytes
      //for( ; j<_toFlxPayloadBytes; ++j, ++buf_i )
      //  _dataBuffer[buf_i] = (char) 0xFF; // Remaining block bytes: 0xFF

      if( buf_i >= _bufferSize ) buf_i = 0;
      */
    }

  // Start DMA (now done in upload())
  //this->dmaStart( 0, repeat*blocks*_toFlxBlockBytes );

  _bytesToUpload = blocks * _toFlxBlockBytes;
  _writeIndex = buf_i;
  //_writeIndex = 0;
  //_uploadIndex = 0;

  return blocks * _toFlxBlockBytes;
}

// ----------------------------------------------------------------------------

int FlxUpload::prepareData( int elink,
                            const std::string &filename,
                            int repeat, bool binary, bool display )
{
  elink = mapElink( elink );

  std::ifstream file( filename );
  if( !file.is_open() )
    {
      _errString = "Failed to open file: ";
      _errString += filename;
      cout << "###" << this->errorString() << endl;
      return -1; // Something went wrong
    }
  cout << "Opened file " << filename << endl;
  _delays.clear();

  // Storage for the number of bytes to read from the file at a time
  char data[1024*60]; // 1024 * maximum number of payload bytes
  i64 shorts, bytes_read = 0, total_bytes_read = 0, blocks_to_upload = 0;
  int i, j, blocks, hdr;
  int data_i;
  int eom_i = -1;
  int line_nr = 0;
  i64 buf_i = _writeIndex;
  if( _fromHostFormat != FH_FORMAT_REGMAP4 )
    hdr = ((elink << _toFlxElinkShift) | _toFlxLengthMask);
  else
    hdr = ((elink << _toFlxElinkShift) |
           ((_toFlxPayloadBytes/2) << TOFLX_LENGTH_SHIFT));
  if( binary )
    {
      file.read( data, 1024*_toFlxPayloadBytes );
      bytes_read = file.gcount();
      while( bytes_read > 0 )
        {
          total_bytes_read += bytes_read;

          // Format and write data bytes to the DMA buffer
          if( _fromHostFormat != FH_FORMAT_REGMAP4 )
            {
              hdr |= _toFlxLengthMask;
            }
          else
            {
              hdr &= ~_toFlxLengthMask;
              hdr |= ((_toFlxPayloadBytes/2) << TOFLX_LENGTH_SHIFT);
            }
          blocks = (bytes_read + _toFlxPayloadBytes-1) / _toFlxPayloadBytes;
          blocks_to_upload += blocks;
          data_i = 0;

          // Is it going to fit?
          if( blocks_to_upload*_toFlxPayloadBytes > (_bufferSize-_writeIndex) )
            {
              std::ostringstream oss;
              oss << "Data (" << blocks_to_upload
                  << "*" << _toFlxPayloadBytes
                  << " bytes) does not fit buffer (size "
                  << _bufferSize << ")";
              _errString = oss.str();
              cout << "###" << this->errorString() << endl;
              return -1;
            }

          // Fully filled FromHost byte blocks
          for( i=0; i<blocks-1; ++i )
            {
              for( j=_toFlxPayloadBytes-1; j>=0; --j, ++buf_i )
                _dataBuffer[buf_i] = data[data_i + j];
              data_i += _toFlxPayloadBytes;
              for( j=0; j<_toFlxHeaderSize; ++j, ++buf_i )
                _dataBuffer[buf_i] = (char) ((hdr >> j*8) & 0xFF);
            }

          // Final (possibly not fully filled) FromHost byte block
          eom_i = buf_i; // Remember this index,
                         // as it may become the final header
                         // NB: ###does not work for 5-bit length:
                         //     <30bytes is automatically considered to be EoM
          bytes_read -= (blocks-1) * _toFlxPayloadBytes;
          shorts      = (bytes_read + 1) / 2; // Length in 2-byte units
          if( _fromHostFormat != FH_FORMAT_REGMAP4 )
            {
              hdr &= ~_toFlxLengthMask;
              hdr |= bytes_read;
              for( j=_toFlxPayloadBytes-1; j>=bytes_read; --j, ++buf_i )
                _dataBuffer[buf_i] = _fillerByte; // 'Empty' block bytes
              for( j=bytes_read-1; j>=0; --j, ++buf_i )
                _dataBuffer[buf_i] = data[data_i + j]; // Final data bytes
            }
          else
            {
              hdr &= ~_toFlxLengthMask;
              hdr |= (shorts << TOFLX_LENGTH_SHIFT);
              for( j=_toFlxPayloadBytes-1; j>=shorts*2; --j, ++buf_i )
                _dataBuffer[buf_i] = _fillerByte; // 'Empty' block bytes
              for( j=shorts*2-1; j>=0; --j, ++buf_i )
                _dataBuffer[buf_i] = data[data_i + j]; // Final data bytes
            }
          for( j=0; j<_toFlxHeaderSize; ++j, ++buf_i )
            _dataBuffer[buf_i] = (char) ((hdr >> j*8) & 0xFF);

          file.read( data, 1024*_toFlxPayloadBytes );
          bytes_read = file.gcount();
        }
      // Set End-Of-Message bit in the final header
      if( eom_i >= 0 && _fromHostFormat == FH_FORMAT_REGMAP4 )
        _dataBuffer[eom_i] |= TOFLX_EOM_MASK;
    }
  else
    {
      i64 bytes_read_prev = 0;
      int bytecount_prev = 0;
      char byteval_prev;

      // Text file: one line per data chunk
      std::string line;
      if( display )
        cout << "Line: Data" << endl
             << "----------" << endl;
      while( !file.eof() )
        {
          std::getline( file, line );
          if( line.empty() )
            {
              if( !file.eof() )
                ++line_nr;
              // Next line
              continue;
            }
          ++line_nr;

          // Remove leading white-space
          size_t startpos = line.find_first_not_of( " \t" );
          if( startpos != string::npos && startpos != 0 )
            line = line.substr( startpos );

          std::istringstream iss;
          int val, bytecount = 0;
          char byteval = 0;
          if( line[0] == '#' )
            {
              // Comment line: skip it, next line
              continue;
            }
          else if( line[0] == '+' )
            {
              // Multi-byte marker:
              // two numbers in such a line mean: bytevalue + bytecount
              // (Add a chunk of size 'bytecount' with each byte='byteval')
              line = line.substr( 1 );
              iss.str( line );
              iss >> std::hex >> val; // Byte value: hexadecimal
              if( iss.fail() )
                {
                  cout << "### Line " << line_nr
                       << ", '+' syntax" << endl;
                }
              else if( val < 0 || val > 255 )
                {
                  cout << "### Line " << line_nr
                       << ", '+' value range err" << endl;
                }
              else
                {
                  iss >> std::dec >> bytecount; // Byte count: decimal
                  if( iss.fail() )
                    cout << "### Line " << line_nr
                         << ", '+' syntax" << endl;
                  else if( bytecount < 0 )
                    cout << "### Line " << line_nr
                         << ", '+' bytecount range err" << endl;
                }
              byteval = (char) (val & 0xFF);
              bytes_read = bytecount;

              if( display )
                cout << setw(3) << line_nr << ": '+' = 0x" << hex
                     << (unsigned int) byteval << ", "
                     << dec << bytecount << " bytes" << endl;
            }
          else if( line[0] == '&' )
            {
              // Delay marker: followed by the number of microseconds to delay
              line = line.substr( 1 );
              iss.str( line );
              iss >> val; // Delay value: decimal
              if( iss.fail() )
                {
                  cout << "### Line " << line_nr
                       << ", '&' syntax" << endl;
                }
              else if( val <= 0 )
                {
                  cout << "### Line " << line_nr
                       << ", '&' value range err" << endl;
                }
              else
                {
                  delay_t d;
                  // This is the data block index at which to insert the delay
                  d.index = _writeIndex + blocks_to_upload * _toFlxBlockBytes;
                  // Length of the delay in microseconds
                  d.delay_us = val;
                  _delays.push_back( d );

                  if( display )
                    cout << setw(3) << line_nr << ": "
                         << "delay " << d.delay_us
                         << " @ index " << d.index
                         << hex << " (" << d.index << ")" << dec << endl;
                }
              // Skip the rest of this line
              continue;
            }
          else if( line[0] == '*' )
            {
              // Repeat-chunk marker:
              // followed by the number of times to repeat the previous chunk
              int repeat_chunk;
              line = line.substr( 1 );
              iss.str( line );
              iss >> val; // Repeat count value: decimal
              if( iss.fail() )
                {
                  cout << "### Line " << line_nr
                       << ", '*' syntax" << endl;
                }
              else if( val <= 0 )
                {
                  cout << "### Line " << line_nr
                       << ", '*' value range err" << endl;
                }
              else if( bytes_read_prev == 0 )
                {
                  cout << "### Line " << line_nr
                       << ", '*' no previous chunk to repeat" << endl;
                }
              else
                {
                  // Repeat previous chunk 'n' times
                  repeat_chunk = val;

                  // Is it going to fit?
                  blocks = (bytes_read_prev + _toFlxPayloadBytes-1)/_toFlxPayloadBytes;
                  blocks *= repeat_chunk;
                  blocks_to_upload += blocks;
                  if( blocks_to_upload*_toFlxPayloadBytes > (_bufferSize-_writeIndex) )
                    {
                      std::ostringstream oss;
                      oss << "Data (" << blocks_to_upload
                          << "*" << _toFlxPayloadBytes
                          << " bytes) does not fit buffer (size "
                          << _bufferSize << ")";
                      _errString = oss.str();
                      cout << "###" << this->errorString() << endl;
                      return -1;
                    }
                  total_bytes_read += repeat_chunk * bytes_read_prev;

                  bool is_delay_chunk = false;
                  for( int i=0; i<repeat_chunk; ++i )
                    prepareBlocks( elink, bytes_read_prev,
                                   (uint8_t *) data, sizeof(data), &buf_i,
                                   is_delay_chunk, (bytecount_prev>0), byteval_prev );

                  if( display )
                    cout << setw(3) << line_nr << ": "
                         << "repeat chunk " << repeat_chunk
                         << " times, size " << bytes_read_prev << endl;
                }
              // Skip the rest of this line
              continue;
            }
          else if( line[0] == '>' )
            {
              // Change the E-link number to upload to!
              line = line.substr( 1 );
              iss.str( line );
              iss >> std::hex >> val; // E-link number: hexadecimal
              if( iss.fail() )
                {
                  cout << "### Line " << line_nr
                       << ", '>' syntax" << endl;
                }
              else if( val < 0 || val > FLX_MAX_ELINK_NR )
                {
                  cout << "### Line " << line_nr
                       << ", '>' value range err" << endl;
                }
              else
                {
                  elink = (val & BLOCK_ELINK_MASK) >> BLOCK_ELINK_SHIFT;
                  /*
                  if( _fromHostFormat != FH_FORMAT_REGMAP4 )
                    hdr = ((elink << _toFlxElinkShift) | _toFlxLengthMask);
                  else
                    hdr = ((elink << _toFlxElinkShift) |
                           ((_toFlxPayloadBytes/2) << TOFLX_LENGTH_SHIFT));
                  */
                  if( display )
                    cout << setw(3) << line_nr << ": "
                         << "E-link = " << hex << val << dec << endl;
                }
              // Skip the rest of this line
              continue;
            }
          else
            {
              // Line with data chunk byte values
              int cnt = 1; // Entry counter
              unsigned int val;
              iss.str( line );
              iss >> std::hex; // Byte values: hexadecimal
              while( !iss.eof() )
                {
                  // Next value (or end-of-file/line)
                  iss >> val;
                  if( !iss.fail() )
                    {
                      if( val >= 0 && val <= 255 )
                        {
                          data[bytes_read] = (char) (val & 0xFF);
                          ++bytes_read;
                        }
                      else
                        {
                          cout << "###Line " << line_nr
                               << ", value out-of-range [0..0xFF]: 0x"
                               << hex << val << dec << endl;
                        }
                    }
                  else if( !iss.eof() )
                    {
                      std::string s;
                      iss.clear();
                      iss >> s; // Skip past the illegal entry
                      cout << "###Line " << line_nr
                           << ", illegal entry #" << cnt << endl;
                    }
                  ++cnt;
                }

              if( display )
                cout << setw(3) << line_nr << ": "
                     << bytes_read << " bytes" << endl;
            }

          if( bytes_read <= 0 )
            {
              // Nothing to upload, go to next line
              bytes_read = 0;
              bytes_read_prev = 0;
              bytecount_prev  = 0;
              continue;
            }
          total_bytes_read += bytes_read;
          bytes_read_prev = bytes_read;
          bytecount_prev  = bytecount;
          byteval_prev    = byteval;

          // Format and write data bytes to the DMA buffer
          /*if( _fromHostFormat != FH_FORMAT_REGMAP4 )
            {
              hdr |= _toFlxLengthMask;
            }
          else
            {
              hdr &= ~(_toFlxLengthMask | TOFLX_EOM_MASK);
              hdr |= ((_toFlxPayloadBytes/2) << TOFLX_LENGTH_SHIFT);
            }*/
          blocks = (bytes_read + _toFlxPayloadBytes-1) / _toFlxPayloadBytes;
          blocks_to_upload += blocks;
          data_i = 0;

          // Is it going to fit?
          if( blocks_to_upload*_toFlxPayloadBytes > (_bufferSize-_writeIndex) )
            {
              std::ostringstream oss;
              oss << "Data (" << blocks_to_upload
                  << "*" << _toFlxPayloadBytes
                  << " bytes) does not fit buffer (size "
                  << _bufferSize << ")";
              _errString = oss.str();
              cout << "###" << this->errorString() << endl;
              return -1;
            }

          bool is_delay_chunk = false;
          prepareBlocks( elink, bytes_read,
                         (uint8_t *) data, sizeof(data), &buf_i,
                         is_delay_chunk, (bytecount>0), byteval );

          /*
          // Fully filled FromHost byte blocks
          for( i=0; i<blocks-1; ++i )
            {
              if( bytecount > 0 )
                // Fill with a constante byte value
                for( j=_toFlxPayloadBytes-1; j>=0; --j, ++buf_i )
                  _dataBuffer[buf_i] = byteval;
              else
                for( j=_toFlxPayloadBytes-1; j>=0; --j, ++buf_i )
                  _dataBuffer[buf_i] = data[data_i + j];
              data_i += _toFlxPayloadBytes;
              for( j=0; j<_toFlxHeaderSize; ++j, ++buf_i )
                _dataBuffer[buf_i] = (char) ((hdr >> j*8) & 0xFF);
            }

          // Final (possibly not fully filled) FromHost byte block
          bytes_read -= (blocks-1) * _toFlxPayloadBytes;
          shorts      = (bytes_read + 1) / 2; // Length in 2-byte units
          if( _fromHostFormat != FH_FORMAT_REGMAP4 )
            {
              hdr &= ~_toFlxLengthMask;
              hdr |= bytes_read;
              for( j=_toFlxPayloadBytes-1; j>=bytes_read; --j, ++buf_i )
                _dataBuffer[buf_i] = _fillerByte; // 'Empty' block bytes
              if( bytecount > 0 )
                // Fill with a constante byte value
                for( j=bytes_read-1; j>=0; --j, ++buf_i )
                  _dataBuffer[buf_i] = byteval; // Final data bytes
              else
                for( j=bytes_read-1; j>=0; --j, ++buf_i )
                  _dataBuffer[buf_i] = data[data_i + j]; // Final data bytes
            }
          else
            {
              hdr &= ~_toFlxLengthMask;
              hdr |= (shorts << TOFLX_LENGTH_SHIFT) | TOFLX_EOM_MASK;
              for( j=_toFlxPayloadBytes-1; j>=shorts*2; --j, ++buf_i )
                _dataBuffer[buf_i] = _fillerByte; // 'Empty' block bytes
              if( bytecount > 0 )
                // Fill with a constante byte value
                for( j=shorts*2-1; j>=0; --j, ++buf_i )
                  _dataBuffer[buf_i] = byteval; // Final data bytes
              else
                for( j=shorts*2-1; j>=0; --j, ++buf_i )
                  _dataBuffer[buf_i] = data[data_i + j]; // Final data bytes
            }
          for( j=0; j<_toFlxHeaderSize; ++j, ++buf_i )
            _dataBuffer[buf_i] = (char) ((hdr >> j*8) & 0xFF);
          */
          // Next line
          bytes_read = 0;
        }
    }

  file.close();
  cout << "Bytes read: " << total_bytes_read;
  if( !binary )
    cout << ", lines: " << line_nr << ", delays: " << _delays.size();
  cout << endl;

  if( bytes_read == 0 )
    {
      // Is it going to fit?
      _bytesToUpload = repeat * blocks_to_upload * _toFlxBlockBytes;
      if( _bytesToUpload > (_bufferSize - _writeIndex) )
        {
          std::ostringstream oss;
          oss << "Data (" << repeat
              << "*" << blocks_to_upload
              << "*" << _toFlxPayloadBytes
              << " bytes) does not fit buffer (size "
              << _bufferSize << ")";
          _errString = oss.str();
          cout << "###" << this->errorString() << endl;
          return -1;
        }

      // Repeat data 'repeat' times
      --repeat;
      for( int cnt=0; cnt<repeat; ++cnt )
        {
          memcpy( (void *) &_dataBuffer[buf_i], (void *) _dataBuffer,
                  blocks_to_upload * _toFlxBlockBytes );
          buf_i += blocks_to_upload * _toFlxBlockBytes;
        }
      if( !_delays.empty() && repeat > 0 )
        {
          // Also repeat any defined delays
          int sz = _delays.size();
          delay_t d;
          std::list<delay_t>::iterator it;
          for( int cnt=0; cnt<repeat; ++cnt )
            {
              it = _delays.begin();
              for( i=0; i<sz; ++i,++it )
                {
                  d = *it;
                  // Adjust the index to point into the repeated data block
                  d.index += (cnt+1) * blocks_to_upload * _toFlxBlockBytes;
                  // Append the delay info to the list
                  _delays.push_back( d );
                }
            }
        }

      // DMA is started in upload()
      _writeIndex = buf_i;
      //_uploadIndex  = 0;

      return _bytesToUpload;
    }
  return -1; // Something went wrong
}

// ----------------------------------------------------------------------------

int FlxUpload::prepareData( int elink, int size, u8 *data )
{
  elink = mapElink( elink );

  // Format and write 'size' bytes from 'data' to the DMA buffer
  int blocks = prepareBlocks( elink, size, data, 0, &_writeIndex );

  // DMA is started in upload()
  _bytesToUpload = blocks * _toFlxBlockBytes;

  return _bytesToUpload;
}

// ----------------------------------------------------------------------------

int FlxUpload::prepareData( int elink, std::vector<sca_frame_t> &msgs )
{
  elink = mapElink( elink );
  //std::cout << "ELINK=" << elink << std::endl;

  // Format and write bytes from GBT-SCA message list 'msgs' to the DMA buffer
  // NB: negative sizes are turned into 'delay chunks'
  //     (with zero-bytes as data *OR* using the RM5 firmw delay packet feature)
  /*int hdr;
  if( _fromHostFormat != FH_FORMAT_REGMAP4 )
    hdr = ((elink << _toFlxElinkShift) | _toFlxLengthMask);
  else
    hdr = ((elink << _toFlxElinkShift) |
           ((_toFlxPayloadBytes/2) << TOFLX_LENGTH_SHIFT));
  int delay_hdr = ((elink  << TOFLX_ELINK_SHIFT) |
                   ((_toFlxPayloadBytes/2) << TOFLX_LENGTH_SHIFT) |
                   TOFLX_EOM_MASK);
  */
  int total_blocks = 0;
  i64 buf_i = _writeIndex;
  bool is_delay_chunk;
  //uint8_t data_os[(2+(4+4)+2)*4]; // For up to 4 times oversampling
  for( unsigned int msg=0; msg<msgs.size(); ++msg )
    {
      int sz;//, blocks, final_bytes, final_size, final_hdr;
      //uint8_t *data_tosend = msgs[msg].data;
      sz = msgs[msg].len;
      if( sz < 0 )
        {
          // Negative size: insert a 'delay chunk' of that size
          // (i.e. insert a delay of a certain length)
          sz = (-sz);
          is_delay_chunk = true;
        }
      else
        {
          is_delay_chunk = false;
          /*
          int _overSampled = 2;
          if( _overSampled > 0 )
            {
              // Convert msgs[msg].data[] into oversampled chunk in data_os[]
              uint32_t nbits = sz * 8;
              uint32_t src_bit, dest_bit = 0;
              uint8_t *src = msgs[msg].data;
              uint8_t *dst = data_os;
              memset( data_os, 0, sizeof(data_os) );
              for( src_bit=0; src_bit<nbits; ++src_bit )
                {
                  if( src[src_bit/8] & (1<<(src_bit & 7)) )
                    {
                      for( int i=0; i<_overSampled; ++i, ++dest_bit )
                        dst[dest_bit/8] |= 1<<(dest_bit & 7);
                    }
                }

              data_tosend = data_os;
              sz *= _overSampled;
            }
          */
        }

      // Alternative to code below:
      //total_blocks += prepareBlocks( elink, sz, data_tosend, 0, &buf_i,
      total_blocks += prepareBlocks( elink, sz, msgs[msg].data, 0, &buf_i,
                                     is_delay_chunk );
      /*
      if( is_delay_chunk && _hdlcDelaySupport )
        {
          // Add a 1-byte chunk 0xFF for each microsecond of delay
          // (equivalent to a size of 10 bytes of the 'delay chunk')
          // (firmware support added, JIRA ticket FLX-1826)
          final_bytes = 1;
          final_hdr  = hdr & ~_toFlxLengthMask;
          final_hdr |= final_bytes; // Length in bytes
          blocks = (sz+9) / 10; // 10 bytes equivalent to 1 microsecond
          int i, j;
          for( i=0; i<blocks; ++i )
            {
              for( j=_toFlxPayloadBytes-1; j>=final_bytes; --j, ++buf_i )
                _dataBuffer[buf_i] = _fillerByte; // 'Empty' block bytes

              _dataBuffer[buf_i] = (u8) 0xFF; // The single data byte
              ++buf_i;
              for( j=0; j<_toFlxHeaderSize; ++j, ++buf_i )
                _dataBuffer[buf_i] = (char) ((final_hdr >> j*8) & 0xFF);
              if( buf_i >= _bufferSize ) buf_i = 0;
            }
          total_blocks += blocks;
          // This concludes the delay chunk generation in delay-support mode
          continue;
        }

      blocks = (sz + _toFlxPayloadBytes-1) / _toFlxPayloadBytes;
      final_bytes = sz - (blocks-1) * _toFlxPayloadBytes;
      if( _fromHostFormat != FH_FORMAT_REGMAP4 )
        {
          final_hdr  = hdr & ~_toFlxLengthMask;
          final_hdr |= final_bytes; // Length in bytes
        }
      else
        {
          final_size = (final_bytes + 1) / 2; // Length in 2-byte units
          final_hdr  = hdr & ~_toFlxLengthMask;
          final_hdr |= ((final_size << TOFLX_LENGTH_SHIFT) | TOFLX_EOM_MASK);
        }

      // Fully-filled FromHost byte blocks (doesn't occur for GBT-SCA messages)
      int i, j, data_i = 0;
      for( i=0; i<blocks-1; ++i )
        {
          if( is_delay_chunk )
            {
              for( j=_toFlxPayloadBytes-1; j>=0; --j, ++buf_i )
                _dataBuffer[buf_i] = (u8) 0x00;
            }
          else
            {
              for( j=_toFlxPayloadBytes-1; j>=0; --j, ++buf_i )
                _dataBuffer[buf_i] = msgs[msg].data[data_i + j];
              data_i += _toFlxPayloadBytes;
            }
          for( j=0; j<_toFlxHeaderSize; ++j, ++buf_i )
            _dataBuffer[buf_i] = (char) ((hdr >> j*8) & 0xFF);
          if( buf_i >= _bufferSize ) buf_i = 0;
        }

      // Final (possibly not fully filled) FromHost byte block
      for( j=_toFlxPayloadBytes-1; j>=final_bytes; --j, ++buf_i )
        _dataBuffer[buf_i] = _fillerByte; // 'Empty' block bytes
      if( is_delay_chunk )
        {
          for( j=final_bytes-1; j>=0; --j, ++buf_i )
            _dataBuffer[buf_i] = (u8) 0x00; // Final data bytes
        }
      else
        {
          for( j=final_bytes-1; j>=0; --j, ++buf_i )
            _dataBuffer[buf_i] = msgs[msg].data[data_i + j]; // Final data bytes
        }
      for( j=0; j<_toFlxHeaderSize; ++j, ++buf_i )
        _dataBuffer[buf_i] = (char) ((final_hdr >> j*8) & 0xFF);
      if( buf_i >= _bufferSize ) buf_i = 0;

      total_blocks += blocks;
      */
      // Add (a) delay block(s) in between
      /*for( i=0; i<0; ++i )
        {
          for( j=0; j<_toFlxPayloadBytes; ++j, ++buf_i )
            _dataBuffer[buf_i] = (char) 0;
          for( j=0; j<_toFlxHeaderSize; ++j, ++buf_i )
            _dataBuffer[buf_i] = (char) ((delay_hdr >> j*8) & 0xFF);
        }
        total_blocks +=0;*/
    }

  // DMA is started in upload()
  _bytesToUpload = total_blocks * _toFlxBlockBytes;
  _writeIndex = buf_i;
  //_uploadIndex = 0;

  return _bytesToUpload;
}

// ----------------------------------------------------------------------------

int FlxUpload::prepareBlocks( int elink, int nbytes,
                              uint8_t *data, int datasize,
                              int64_t *buf_i, bool is_delay_chunk,
                              bool use_byteval, uint8_t byteval )
{
  // Build the 32-byte FromHost data blocks to upload
  // 'nbytes' of 'data' to 'elink' (or: insert delay chunks
  // of fill upload blocks with a constant byte value 'byteval')

  // NB: assume 'elink' has already been 'mapped'
  // NB: '*buf_i' has to be updated for return
  int hdr, blocks, final_bytes, final_size, final_hdr;
  int64_t index = *buf_i;

  blocks = (nbytes + _toFlxPayloadBytes-1) / _toFlxPayloadBytes;
  final_bytes = nbytes - (blocks-1) * _toFlxPayloadBytes;
  if( _fromHostFormat != FH_FORMAT_REGMAP4 )
    {
      hdr = ((elink << _toFlxElinkShift) | _toFlxLengthMask);
      final_hdr  = hdr & ~_toFlxLengthMask;
      final_hdr |= final_bytes; // Length in bytes
    }
  else
    {
      hdr = ((elink << _toFlxElinkShift) |
             ((_toFlxPayloadBytes/2) << TOFLX_LENGTH_SHIFT));
      final_size = (final_bytes + 1) / 2; // Length in 2-byte units
      final_hdr  = hdr & ~_toFlxLengthMask;
      final_hdr |= ((final_size << TOFLX_LENGTH_SHIFT) | TOFLX_EOM_MASK);
    }

  if( is_delay_chunk && _hdlcDelaySupport )
    {
      // Add a 1-byte chunk 0xFF for each microsecond of delay
      // (equivalent to a size of 10 bytes of the 'delay chunk')
      // (firmware support added, JIRA ticket FLX-1826)
      final_bytes = 1;
      final_hdr  = hdr & ~_toFlxLengthMask;
      final_hdr |= final_bytes; // Length in bytes
      blocks = (nbytes+9) / 10; // 10 bytes equivalent to 1 microsecond
      int i, j;
      for( i=0; i<blocks; ++i )
        {
          for( j=_toFlxPayloadBytes-1; j>=final_bytes; --j, ++index )
            _dataBuffer[index] = _fillerByte; // 'Empty' block bytes

          _dataBuffer[index] = (u8) 0xFF; // The single data byte
          ++index;
          for( j=0; j<_toFlxHeaderSize; ++j, ++index )
            _dataBuffer[index] = (char) ((final_hdr >> j*8) & 0xFF);

          if( index >= _bufferSize ) index = 0;
        }

      // This concludes the delay chunk generation in delay-support mode
      *buf_i = index;
      return blocks;
    }

  int i, j, data_i = 0;
  // Fully-filled FromHost byte blocks
  for( i=0; i<blocks-1; ++i )
    {
      if( is_delay_chunk )
        {
          // A delay in the shape of 0-byte chunks
          for( j=_toFlxPayloadBytes-1; j>=0; --j, ++index )
            _dataBuffer[index] = (u8) 0x00;
        }
      else if( use_byteval )
        {
          for( j=_toFlxPayloadBytes-1; j>=0; --j, ++index )
            _dataBuffer[index] = byteval;
        }
      else
        {
          // Only if datasize > 0 take datasize into account
          if( datasize == 0 || nbytes <= datasize )
            for( j=_toFlxPayloadBytes-1; j>=0; --j, ++index )
              _dataBuffer[index] = data[data_i + j];
          else
            // Copy data[] modulo 'datasize'
            for( j=_toFlxPayloadBytes-1; j>=0; --j, ++index )
              _dataBuffer[index] = data[(data_i + j) % datasize];
          data_i += _toFlxPayloadBytes;
        }
      for( j=0; j<_toFlxHeaderSize; ++j, ++index )
        _dataBuffer[index] = (char) ((hdr >> j*8) & 0xFF);
      if( index >= _bufferSize ) index = 0;
    }

  // Final (possibly not fully filled) FromHost byte block
  for( j=_toFlxPayloadBytes-1; j>=final_bytes; --j, ++index )
    _dataBuffer[index] = _fillerByte; // 'Empty' block bytes
  if( is_delay_chunk )
    {
      // A delay in the shape of 0-byte chunks
      for( j=final_bytes-1; j>=0; --j, ++index )
        _dataBuffer[index] = (u8) 0x00; // Final 'delay' bytes
    }
  else if( use_byteval )
    {
      for( j=final_bytes-1; j>=0; --j, ++index )
        _dataBuffer[index] = byteval;
    }
  else
    {
      // Final data bytes;
      // only if datasize > 0 take datasize into account
      if( datasize == 0 || nbytes <= datasize )
        for( j=final_bytes-1; j>=0; --j, ++index )
          _dataBuffer[index] = data[data_i + j];
      else
        // Copy data[] modulo 'datasize'
        for( j=final_bytes-1; j>=0; --j, ++index )
          _dataBuffer[index] = data[(data_i + j) % datasize];
    }
  for( j=0; j<_toFlxHeaderSize; ++j, ++index )
    _dataBuffer[index] = (char) ((final_hdr >> j*8) & 0xFF);

  if( index >= _bufferSize ) index = 0;

  *buf_i = index;
  return blocks;
}

// ----------------------------------------------------------------------------

int FlxUpload::prepareDataRaw( int size, int pattern_id )
{
  // Create the test data bytes according to a pattern identifier
  char data[256];
  int  i;
  if( pattern_id == 1 )
    {
      // 0x55-0xAA pattern
      for( i=0; i<256; ++i )
        if( i & 1 )
          data[i] = (char) 0xAA;
        else
          data[i] = (char) 0x55;
    }
  else if( pattern_id == 2 )
    {
      // 0xFF only
      for( i=0; i<256; ++i )
        data[i] = (char) 0xFF;
    }
  else
    {
      // Incrementing pattern
      for( i=0; i<256; ++i )
        data[i] = (char) (i & 0xFF);
    }

  // Write 'size' bytes of test data to the DMA buffer
  i64 buf_i  = _writeIndex;
  int data_i = 0;
  for( i=0; i<size; ++i, ++buf_i, ++data_i )
    _dataBuffer[buf_i] = data[data_i & 0xFF];

  // DMA is started in upload()
  _bytesToUpload = size;
  _writeIndex = buf_i;
  //_uploadIndex = 0;

  return size;
}

// ----------------------------------------------------------------------------

int FlxUpload::prepareDataRaw( const std::string &filename, bool binary )
{
  std::ifstream file( filename );
  if( !file.is_open() )
    {
      _errString = "Failed to open file: ";
      _errString += filename;
      cout << "###" << this->errorString() << endl;
      return -1; // Something went wrong
    }
  cout << "Opened file " << filename << endl;

  i64 bytes_read = 0, bytes_to_upload = 0;
  i64 buf_i = _writeIndex;
  int line_nr = 0;
  if( binary )
    {
      // Storage for the number of bytes to read from the file at a time
      char data[1024*60]; // 1024 * maximum number of payload bytes
      int i, data_i;

      file.read( data, sizeof(data) );
      bytes_read = file.gcount();
      while( bytes_read > 0 )
        {
          bytes_to_upload += bytes_read;
          data_i = 0;

          for( i=0; i<bytes_read; ++i, ++buf_i, ++data_i )
            _dataBuffer[buf_i] = data[data_i];

          file.read( data, 1024*_toFlxPayloadBytes );
          bytes_read = file.gcount();
        }
    }
  else
    {
      // Text file: lines with data(word) values
      std::string line;
      std::getline( file, line );
      while( !file.eof() )
        {
          ++line_nr;

          if( line.empty() )
            {
              // Next line
              std::getline( file, line );
              continue;
            }

          // Remove leading white-space
          size_t startpos = line.find_first_not_of( " \t" );
          if( startpos != string::npos && startpos != 0 )
            line = line.substr( startpos );

          std::istringstream iss;
          unsigned int val;
          if( line[0] == '#' )
            {
              // Comment line: skip it
              std::getline( file, line );
              continue;
            }
          else
            {
              // Line with (MROD) data
              int cnt = 1; // Entry counter
              iss.str( line );
              //cout << "Line " << line_nr << ": " << line << endl;
              iss >> std::hex; // Word values: hexadecimal
              while( !iss.eof() )
                {
                  // Next value (or end-of-file/line)
                  iss >> val;
                  if( !iss.fail() )
                    {
                      unsigned char *buf =
                        (unsigned char *) &_dataBuffer[buf_i];
                      *buf = (unsigned char) ((val>> 0) & 0xFF); ++buf;
                      *buf = (unsigned char) ((val>> 8) & 0xFF); ++buf;
                      *buf = (unsigned char) ((val>>16) & 0xFF); ++buf;
                      *buf = (unsigned char) ((val>>24) & 0xFF); ++buf;
                      buf_i += 4;
                      bytes_read += 4;
                    }
                  else if( !iss.eof() )
                    {
                      std::string s;
                      iss.clear();
                      iss >> s; // Skip past the illegal entry
                      cout << "###Line " << line_nr
                           << ", illegal entry #" << cnt << endl;
                    }
                  ++cnt;
                }
            }

          if( bytes_read <= 0 )
            {
              // Nothing to upload, go to next line
              std::getline( file, line );
              bytes_read = 0;
              continue;
            }
          bytes_to_upload += bytes_read;

          // Next line
          std::getline( file, line );
          bytes_read = 0;
        }
    }

  file.close();
  cout << "Bytes read: " << bytes_to_upload
       << " (words: " << bytes_to_upload/4 << ")";
  if( !binary )
    cout << ", lines: " << line_nr;
  cout << endl;

  if( bytes_read == 0 )
    {
      // DMA is started in upload()
      _bytesToUpload = ((bytes_to_upload + 31)/32) * 32;
      _writeIndex = buf_i;

      cout << "Bytes in upload: " << _bytesToUpload << endl;

      return _bytesToUpload;
    }
  return -1; // Something went wrong
}

// ----------------------------------------------------------------------------

bool FlxUpload::upload( int speed_factor )
{
  if( _bytesToUpload == 0 )
    return false;

  if( this->dmaActive() )
    {
      //struct timespec ts;
      //ts.tv_sec  = 0;
      //ts.tv_nsec = (_toFlxBlockBytes/8) * 1000; // in nanoseconds
      //nanosleep( &ts, 0 );
      this->pause( _toFlxBlockBytes/8 ); // in microseconds
      return false;
    }

  // Timestamp the start of an upload operation (the timestamp is reset
  // to zero when read by a call to function timestamp())
  if( _timestamp == 0 )
    {
      struct timespec ts;
      clock_gettime( CLOCK_REALTIME, &ts );
      _timestamp = ts.tv_sec*1000000000 + ts.tv_nsec;
    }

  u64 dma_size;
  if( _uploadDmaSize == 0 || _bytesToUpload < _uploadDmaSize )
    dma_size = _bytesToUpload;
  else
    dma_size = _uploadDmaSize;
  if( _uploadIndex + dma_size > (u64) _bufferSize )
    // Limit DMA to up to end of buffer (in case of single-shot DMA)
    dma_size = (u64) _bufferSize - _uploadIndex;

  // Delay handling required?
  if( !_delays.empty() )
    {
      delay_t &d = _delays.front();
      if( _uploadIndex == d.index )
        {
          // Arrived at a delay index: insert the requested delay
          this->pause( d.delay_us );
          // This delay has been handled, get rid of it
          _delays.pop_front();
        }
      if( !_delays.empty() )
        {
          delay_t &d = _delays.front();
          if( _uploadIndex + dma_size > d.index )
            {
              // Upcoming delay: adjust the DMA size to arrive at the index
              // where the requested delay is to be inserted
              dma_size = d.index - _uploadIndex;
            }
        }
    }

  this->dmaStart( _uploadIndex, dma_size );

  // Update upload administration
  _bytesToUpload -= dma_size;
  _uploadIndex   += dma_size;
  if( _uploadIndex >= (u64) _bufferSize ) {
    //cout << "WRAP @ " << _uploadIndex << endl;
    _uploadIndex = 0;
  }

  // Limit overall upload rate to a maximum of 8 MB/s
  // (for a 2-bit E-link, could be twice that for a 4-bit E-link,
  //  and 4x that for an 8-bit E-link)
  if( dma_size > 127 )
    //this->pause( (dma_size/(8*speed_factor))+1 );
    this->pause( dma_size/(8*speed_factor) );

  return true;
}

// ----------------------------------------------------------------------------

bool FlxUpload::uploadFinished()
{
  return( _bytesToUpload == 0 && !this->dmaActive() );
}

// ----------------------------------------------------------------------------

bool FlxUpload::startPlayback( const std::string &filename, int blocksize )
{
  if( _pbFile.is_open() ) _pbFile.close();

  _pbFile.open( filename.c_str(), std::ifstream::binary );
  if( !_pbFile.is_open() )
    {
      _errString = "Failed to open playback file: ";
      _errString += filename;
      return false;
    }

  // Initialize playback counters
  _pbBlockCntr = 0;
  _pbChunkCntr = 0;

  // Initialize E-link intermediate buffers
  for( unsigned int i=0; i<FLX_MAX_ELINK_NR+1; ++i )
    _eBuf[i].clear();

  if( blocksize > 16384 )
    {
      _errString = "Playback of blocks > 16K currently not supported";
      return false;
    }
  else if( blocksize > 0 )
    {
      // Force a different size than read from the FLX-device itself
      _blockSize = blocksize;
      _blockSizeCode  = (_blockSize/1024-1) << 8;
    }

  // In case of random data upload
  unsigned int seed = 1;
  srandom( seed );

  return true;
}

// ----------------------------------------------------------------------------

bool FlxUpload::playback( int elink_filter, int speed_factor )
{
  // Allow up to 16K blocks (see startPlayback())
  char block[16384];//[BLOCK_BYTES];
  _pbFile.read( block, _blockSize );
  i64 bytes_read = _pbFile.gcount();
  if( bytes_read != _blockSize )
    return false;

  //_uploadIndex   = 0; // OLD method (one-shot DMAs from address 0)
  //_writeIndex    = 0; // OLD method (one-shot DMAs from address 0)
  _bytesToUpload = formatBlockForUpload( block, elink_filter );

  //this->dumpData( _bytesToUpload, _writeIndex - _bytesToUpload );

  if( _bytesToUpload > 0 )
    {
      ++_pbBlockCntr;

      while( !this->uploadFinished() )
        {
          //cout << "playback upload:" << _bytesToUpload << endl;
          this->upload( speed_factor );
        }
    }

  return true;
}

// ----------------------------------------------------------------------------

bool FlxUpload::writeIcConfigFile( int gbt, int i2c_addr,
                                   const std::string &filename,
                                   int *nbytes_file,
                                   bool ec, bool lpgbt_v1, bool one_by_one )
{
  std::ifstream file( filename );
  if( !file.is_open() )
    {
      _errString = "Failed to open IC file: ";
      _errString += filename;
      return false;
    }

  // Determine if file contains a single byte value per line,
  // or register address followed by byte value (all hexadecimal);
  // the first file type is read and uploaded in one operation,
  // while the second file type is uploaded one register at a time
  // as there may be address gaps or random address order
  bool with_addr = false;
  bool with_comment = false;
  char line[64], comment[128];
  uint32_t regaddr, byteval;
  file.getline( line, 64 );
  if( sscanf( line, "%X %X", &regaddr, &byteval ) == 2 )
    with_addr = true;
  if( sscanf( line, "%X %X %s", &regaddr, &byteval, comment ) == 3 )
    with_comment = true;
  file.close();
  cout << "File " << filename << ", format=";
  if( with_comment && with_addr )
    cout << "\"<address> <byteval> <comment>\" (per line)" << endl;
  else if( with_addr )
    cout << "\"<address> <byteval>\" (per line)" << endl;
  else
    cout << "\"<byteval>\" (per line)" << endl;

  // Read file contents (ASCII)
  file.open( filename );
  regaddr = 0;
  file >> std::hex; // Values are hexadecimal
  uint8_t databytes[1024];
  uint32_t number_of_regs = 0;
  uint32_t line_nr = 1;
  bool result = true;
  while( !file.eof() )
    {
      // Next value or address/value pair
      if( with_addr )
        {
          file >> regaddr;
          if( file.eof() )
            break;
        }
      file >> byteval;

      if( file.fail() )
        {
          if( file.eof() )
            break;

          std::ostringstream oss;
          oss << "Illegal value at line " << line_nr
              << " addr=0x" << std::hex << std::setfill('0')
              << std::setw(3) << regaddr;
          _errString = oss.str();
          file.close();
          return false;
        }

      if( with_comment )
        // Skip the rest of this line
        file.getline( comment, 128 );

      if( byteval < 0 || byteval > 0xFF )
        {
          std::ostringstream oss;
          oss << "Invalid byte value 0x" << std::hex << std::setfill('0')
              << std::setw(2) << byteval
              << " at line " << line_nr
              << " addr=0x" << std::setw(3) << regaddr;
          _errString = oss.str();
          file.close();
          return false;
        }

      if( with_addr || one_by_one )
        {
          // Upload one register at a time
          // (since addresses are not necessarily in order or consecutive)
          uint8_t byt = (uint8_t) (byteval & 0xFF);
          if( !this->writeIcChannel( gbt, i2c_addr, regaddr, 1, &byt,
                                     ec, lpgbt_v1 ) )
            result = false;
          //cout << "WRITE: " << gbt << " " << i2c_addr << " " << regaddr
          //     << hex << " " << (uint32_t) byt << dec <<  endl;
        }
      else
        {
          // ###DEBUG:
          //cout << hex << regaddr << ": " << byteval << endl;
          if( regaddr >= (int) sizeof(databytes) )
            {
              _errString = "Register address out-of-range (>1023)";
              //break;
              file.close();
              return false;
            }
          // Store the register setting
          databytes[regaddr] = (u8) (byteval & 0xFF);
        }

      if( !with_addr )
        ++regaddr;
      ++number_of_regs;
      ++line_nr;
    }
  file.close();
  *nbytes_file = number_of_regs; // Number of byte values in file

  // Upload the register values compiled in 'databytes'
  if( !with_addr )
    result = this->writeIcChannel( gbt, i2c_addr, 0, number_of_regs, databytes,
                                   ec, lpgbt_v1 );
  return result;
}

// ----------------------------------------------------------------------------

bool FlxUpload::writeIcChannel( int gbt, int i2c_addr, int reg_addr,
                                int nbytes, u8 *data, bool ec, bool lpgbt_v1 )
{
  bool read = false;
  return this->uploadIcFrame( read, gbt, i2c_addr, reg_addr, nbytes, data,
                              ec, lpgbt_v1 );
}

// ----------------------------------------------------------------------------

bool FlxUpload::readIcChannel( int gbt, int i2c_addr, int reg_addr,
                               int nbytes, bool ec, bool lpgbt_v1 )
{
  bool read = true;
  return this->uploadIcFrame( read, gbt, i2c_addr, reg_addr, nbytes, 0,
                              ec, lpgbt_v1 );
}

// ----------------------------------------------------------------------------

bool FlxUpload::uploadIcFrame( bool read,
                               int  gbt, int i2c_addr, int reg_addr,
                               int  nbytes, u8 *data,
                               bool ec, bool lpgbt_v1 )
{
  int len = nbytes;
  if( read == false && len > 1024 )
    {
      _errString = "Write IC payload too large (>1024) for uploadIcFrame()";
      return false;
    }

  // Compose IC frame for a socalled write-read sequence
  u8 frame[7 + 1024 + 1]; // Space for frame with up to 1024 bytes payload
  int index = 0;
  if( !lpgbt_v1 )
    frame[index++] = 0;                    // Additional byte!?...
  frame[index] = (u8) ((i2c_addr & 0x7F) << 1);// GBTX I2C address
  if( read )
    frame[index] |= (u8) 1;                // Read/write bit
  ++index;
  frame[index++] = 1;                      // Command (not used in GBTX v1 + 2)
  frame[index++] = (u8) (len & 0xFF);      // Number of data bytes
  frame[index++] = (u8) ((len >> 8) & 0xFF);
  frame[index++] = (u8) (reg_addr & 0xFF); // Register (start) address
  frame[index++] = (u8) ((reg_addr >> 8) & 0xFF);
  if( !read )
    {
      // Bytes to write
      memcpy( &frame[index], data, len );  // Copy data bytes into frame
      index += len;
    }

  // Calculate parity
  // (do not include zero-byte and I2C-address, in case of GBT or lpGBTv0)
  int parity_start_index = (lpgbt_v1 ? 0 : 2);
  u8 parity = 0;
  for( int i=parity_start_index; i<index; ++i )
    parity ^= frame[i];
  frame[index++] = parity;                 // Parity byte

  // Upload IC frame, to IC- or EC-channel
  int epath;
  if( ec )
    epath = 7;
  else
    epath = 6;
  int frame_len = index;
  int egroup    = 7;
  int elink     = ((gbt    << BLOCK_LNK_SHIFT) |
                   (egroup << BLOCK_EGROUP_SHIFT) | epath);
  this->prepareData( elink, frame_len, frame );
  while( !this->uploadFinished() )
    {
      //cout << "uploadIcFrame upload:" << _bytesToUpload << endl;
      this->upload();
    }

  return true;
}

// ----------------------------------------------------------------------------

bool FlxUpload::addIcFrame( bool read, int i2c_addr, int reg_addr,
                            int  nbytes, u8 *data,
                            bool lpgbt_v1 )
{
  int len = nbytes;
  if( read == false && len > 4 )
    {
      _errString = "Write IC payload too large (>4) for addIcFrame()";
      return false;
    }

  sca_frame_t msg;

  // Compose IC frame for a socalled write-read sequence
  u8 *frame = msg.data;
  int index = 0;
  if( !lpgbt_v1 )
    frame[index++] = 0;                    // Additional byte!?...
  frame[index] = (u8) ((i2c_addr & 0x7F) << 1);// GBTX I2C address
  if( read )
    frame[index] |= (u8) 1;                // Read/write bit
  ++index;
  frame[index++] = 1;                      // Command (not used in GBTX v1 + 2)
  frame[index++] = (u8) (len & 0xFF);      // Number of data bytes
  frame[index++] = (u8) ((len >> 8) & 0xFF);
  frame[index++] = (u8) (reg_addr & 0xFF); // Register (start) address
  frame[index++] = (u8) ((reg_addr >> 8) & 0xFF);
  if( !read )
    {
      // Bytes to write
      memcpy( &frame[index], data, len );  // Copy data bytes into frame
      index += len;
    }

  // Calculate parity
  // (do not include zero-byte and I2C-address, in case of GBT or lpGBTv0)
  int parity_start_index = (lpgbt_v1 ? 0 : 2);
  u8 parity = 0;
  for( int i=parity_start_index; i<index; ++i )
    parity ^= frame[i];
  frame[index++] = parity;                 // Parity byte

  msg.len = index;
  // Add to list of frames (reusing the GBT-SCA frames list)
  _scaFrames.push_back( msg );

  // There should be a minimal delay to prevent the next frame's Start-of-Frame
  // truncating the reply to this message when received by the (lp)GBT
  this->addIcDelayUs( 1 );

  return true;
}

// ----------------------------------------------------------------------------

int FlxUpload::uploadIcFrames( int linknr, bool ec )
{
  int egroup = 7;
  int epath  = 6;
  if( ec )
    epath = 7;
  int elinknr = ((linknr << BLOCK_LNK_SHIFT) |
                 (egroup << BLOCK_EGROUP_SHIFT) | epath);
  return this->uploadScaFrames( elinknr );
}

// ----------------------------------------------------------------------------
// Examples uploadScaFrame():
// Transaction id freely chosen from range 1..0xFE
//
// Enable GPIO, via GBT EC-bits, i.e. egroup = 5, epath = 7:
// uploadScaFrame(elinknr, 0xAA, SCA_DEV_CONFIG, 2,
//                SCA_CONFIG_WR_B, [0x00,(1<<SCA_DEV_GPIO)])
//
// Set GPIO direction (cmd=0x20) to all output:
// uploadScaFrame(elinknr, 0xAB, SCA_DEV_GPIO, 4,
//                SCA_GPIO_WR_DIR, [0xFF, 0xFF, 0xFF, 0xFF])
//
// Set GPIO (cmd=0x10) bit 0+1 to 0:
// uploadScaFrame(elinknr, 0xAC, SCA_DEV_GPIO, 4,
//                SCA_GPIO_WR_OUT, [0xFC, 0xFF, 0xFF, 0xFF])

int FlxUpload::uploadScaFrame( int elink,
                               int *trid, int chan, int len, int cmd,
                               u8  *data )
{
  int sca_addr = 0; // Always 0?
  // Compose EC frame: (SOF+)ADDR+CTRL, payload: 4 + 0/2/4 bytes, FCS(, EOF)
  // (SOF/EOF are added by the firmware)
  u8  frame[2+(4+4)+2];
  frame[0] = (u8) (sca_addr & 0xFF);
  u64 seqnr = _hdlcSeqNr & HDLC_CTRL_SEQNR_MASK;
  // Control byte: acknowledge up to previous message RECeiVeD (by default?)
  // i.e. set to "first frame not received; it acknowledges that all frames
  // with seqnr up to (seqnr-1)%8 have been received"
  frame[1] = (u8) ((seqnr << HDLC_CTRL_NRECVD_SHIFT)|
                   (seqnr << HDLC_CTRL_NSENT_SHIFT));
  ++_hdlcSeqNr;
  // Make sure the transaction ID is valid
  if( *trid == 0 || *trid == 255 )
    *trid = 1;
  frame[2] = (u8) ((*trid) & 0xFF);
  // Update the transaction ID
  ++(*trid);
  if( *trid > 252 ) // Reserve 253 and 254 for our own custom purposes
    *trid = 1;
  frame[3] = (u8) (chan & 0xFF);
  frame[4] = (u8) (len  & 0xFF);
  //if( chan == SCA_DEV_CONFIG ) --frame[4]; // ###HACK (required or not?? TBD)
  frame[5] = (u8) (cmd  & 0xFF);
  // Add GBT-SCA frame data bytes
  for( int i=0; i<len; ++i )
    frame[6+i] = data[i];
  // Add GBT-SCA frame CRC
  u16 crc = crc16( frame, 2+4+len );
  frame[6+len]   = (u8) ((crc >> 8) & 0xFF);
  frame[6+len+1] = (u8) (crc & 0xFF);

  /*
  int upload_sz;
  int _overSampled = 2;
  if( _overSampled > 0 )
    {
      // Convert frame[] into oversampled chunk in data_os[]
      int      sz = 6+len+2;
      uint32_t nbits = sz * 8;
      uint32_t src_bit, dest_bit = 0;
      uint8_t *src = frame;
      uint8_t *dst = data_os;
      uint8_t  data_os[(2+(4+4)+2)*4]; // Up to 4 times oversampling
      memset( data_os, 0, sizeof(data_os) );
      for( src_bit=0; src_bit<nbits; ++src_bit )
        {
          if( src[src_bit/8] & (1<<(src_bit & 7)) )
            {
              for( int i=0; i<_overSampled; ++i, ++dest_bit )
                dst[dest_bit/8] |= 1<<(dest_bit & 7);
            }
        }

      sz *= _overSampled;
      upload_sz = this->prepareData( elink, sz, data_os );
    }
  else
    {
      upload_sz = this->prepareData( elink, 6+len+2, frame );
    }
  */
  int upload_sz = this->prepareData( elink, 6+len+2, frame );
  while( !this->uploadFinished() )
    {
      //cout << "uploadScaFrame upload:" << _bytesToUpload << endl;
      this->upload();
    }
  return upload_sz;
}

// ----------------------------------------------------------------------------

void FlxUpload::resetScaFrames()
{
  _scaFrames.clear();
}

// ----------------------------------------------------------------------------

void FlxUpload::dumpScaFrames()
{
  sca_frame_t *pmsg;
  for( unsigned int i=0; i<_scaFrames.size(); ++i )
    {
      pmsg = &_scaFrames[i];
      cout << dec << setfill(' ') << setw(3) << i << ": ";
      if( pmsg->len < 0 )
        {
          cout << "delay=" << (-(pmsg->len));
        }
      else
        {
          cout << hex << setfill('0');
          for( int j=0; j<pmsg->len; ++j )
            cout << ' ' << setw(2) << ((int) pmsg->data[j] & 0xFF);
        }
      cout << endl;
    }
}

// ----------------------------------------------------------------------------

bool FlxUpload::addScaFrame( int *trid, int chan, int len, int cmd, u8 *data )
{
  sca_frame_t msg;
  int sca_addr = 0; // Always 0?
  // Compose EC frame: (SOF+)ADDR+CTRL, payload: 4 + 0/2/4 bytes, FCS(, EOF)
  // (SOF/EOF are added by the firmware)
  // and append it to the current list of SCA frames
  u8 *frame = msg.data;
  frame[0] = (u8) (sca_addr & 0xFF);
  u64 seqnr = _hdlcSeqNr & HDLC_CTRL_SEQNR_MASK;
  //if( (_hdlcSeqNr & 7) == 6 ){//DEBUG!!
  //  ++seqnr;
  //}
  // Control byte: acknowledge up to previous message RECeiVeD (by default?)
  // i.e. set to "first frame not received; it acknowledges that all frames
  // with seqnr up to (seqnr-1)%8 have been received"
  frame[1] = (u8) ((seqnr << HDLC_CTRL_NRECVD_SHIFT)|
                   (seqnr << HDLC_CTRL_NSENT_SHIFT));
  ++_hdlcSeqNr;
  // Make sure the transaction ID is valid, i.e. not 0 or 255 (reserved)
  if( *trid == 0 || *trid == 255 )
    *trid = 1;
  frame[2] = (u8) ((*trid) & 0xFF);
  // Update the transaction ID
  ++(*trid);
  if( *trid > 252 ) // Reserve 253 and 254 for our own custom purposes
    *trid = 1;
  frame[3] = (u8) (chan & 0xFF);
  frame[4] = (u8) (len  & 0xFF);
  //if( chan == SCA_DEV_CONFIG ) --frame[4]; // ###HACK (required or not?? TBD)
  frame[5] = (u8) (cmd  & 0xFF);
  // Add GBT-SCA frame data bytes
  for( int i=0; i<len; ++i )
    frame[6+i] = data[i];
  // Add GBT-SCA frame CRC
  u16 crc = crc16( frame, 2+4+len );
  frame[6+len]   = (u8) ((crc >> 8) & 0xFF);
  frame[6+len+1] = (u8) (crc & 0xFF);

  msg.len = 2 + 4 + len + 2;
  // Add to list of GBT-SCA frames
  _scaFrames.push_back( msg );

#if REGMAP_VERSION >= 0x500
  // NB: in the 'HDLC delay packet' firmware versions need to add this,
  //     or things won't work properly !
  //     (under investigation; see JIRA ticket FLX-1826; fixed June 2023)
  //this->addScaDelayUs( 1 );
#endif // REGMAP_VERSION

  return true;
}

// ----------------------------------------------------------------------------

bool FlxUpload::addScaDelay( int len )
{
  // Negative length value is turned into a 'delay chunk' of that length
  // by function prepareData()
  sca_frame_t msg;
  msg.len = -len;
  // Add to list of GBT-SCA frames
  _scaFrames.push_back( msg );
  return true;
}

// ----------------------------------------------------------------------------

bool FlxUpload::addScaDelayUs( int microseconds )
{
  return this->addScaDelay( microseconds*10 );
}

// ----------------------------------------------------------------------------

int FlxUpload::uploadScaFrames( int elink )
{
  int upload_sz = this->prepareData( elink, _scaFrames );
  //cout << "uploadScaFrames upload:" << _bytesToUpload << endl;
  while( !this->uploadFinished() )
    this->upload();
  return upload_sz;
}

// ----------------------------------------------------------------------------

int FlxUpload::scaConnect( int elink )
{
  return this->scaControl( elink,
                           //HDLC_CTRL_CONNECT | HDLC_CTRL_POLLBIT );
                           HDLC_CTRL_CONNECT );
}

// ----------------------------------------------------------------------------

int FlxUpload::scaReset( int elink )
{
  return this->scaControl( elink,
                           //HDLC_CTRL_RESET | HDLC_CTRL_POLLBIT );
                           HDLC_CTRL_RESET );
}

// ----------------------------------------------------------------------------

int FlxUpload::scaTest( int elink )
{
  return this->scaControl( elink,
                           //HDLC_CTRL_TEST | HDLC_CTRL_POLLBIT );
                           HDLC_CTRL_TEST );
}

// ----------------------------------------------------------------------------

void FlxUpload::setHdlcDelaySupport ( bool b )
{
#if REGMAP_VERSION >= 0x500
  _hdlcDelaySupport = b;
  if( _flx == 0 ) return;

  // Enable or disable it everywhere
  for( uint32_t lnk=0; lnk<_bar2->NUM_OF_CHANNELS; ++lnk )
    {
      for( uint32_t grp=0; grp<FLX_FROMHOST_GROUPS-1; ++grp )
        {
          flxcard_encoding_egroup_ctrl_t *ctrl =
            &_bar2->ENCODING_EGROUP_CTRL_GEN[lnk].ENCODING_EGROUP[grp].
            ENCODING_EGROUP_CTRL;
          ctrl->ENABLE_DELAY = (b ? 1 : 0);
        }
      flxcard_mini_egroup_fromhost_t *mini_egroup_fromhost =
        &_bar2->MINI_EGROUP_FROMHOST_GEN[lnk].MINI_EGROUP_FROMHOST;
      mini_egroup_fromhost->ENABLE_DELAY = (b ? 1 : 0);
      //mini_egroup_fromhost->ENABLE_DELAY = 0;
    }
#endif // REGMAP_VERSION
}

// ----------------------------------------------------------------------------

void FlxUpload::dumpData( int size, int offset )
{
  cout << "Blocksize (payload): " << _toFlxBlockBytes  << " ("
       << _toFlxPayloadBytes << ")";
  char *data = &_dataBuffer[offset];
  cout << hex << setfill('0');
  for( int i=0; i<size; ++i )
    {
      if( (i & 31) == 0 ) cout << endl << setw(6) << i+offset;
      cout << ' ' << setw(2) << ((int) data[i] & 0xFF);
    }
  cout << dec << endl;
}

// ----------------------------------------------------------------------------

std::string FlxUpload::firmwareVersion()
{
  if( _flx == 0 ) return std::string();
  return _flx->firmware_string();
}

// ----------------------------------------------------------------------------

int FlxUpload::firmwareMode()
{
  if( _flx == 0 ) return 0xFF;
  return _bar2->FIRMWARE_MODE;
}

// ----------------------------------------------------------------------------

bool FlxUpload::fullmodeType()
{
  if( _flx == 0 ) return false;
  return _flx->fullmode_type();
}

// ----------------------------------------------------------------------------

bool FlxUpload::lpgbtType()
{
  if( _flx == 0 ) return false;
  return _flx->lpgbt_type();
}

// ----------------------------------------------------------------------------

int FlxUpload::numberOfChans()
{
  if( _flx == 0 ) return 0;
  return _bar2->NUM_OF_CHANNELS;
}

// ----------------------------------------------------------------------------
// Private functions
// ----------------------------------------------------------------------------

char *FlxUpload::cmemAllocate( u64 buffer_size, int id )
{
  u64   cmemPhysAddr = 0;
  char *buffer = 0;

  // Open CMEM(_RCC) and allocate the requested buffer size
#ifdef USE_LIB_CMEM
  u64 cmemVirtAddr;
  int ret = CMEM_Open();
  ostringstream oss;
  oss << "FlxUpload";
  if( id >= 0 ) oss << id;
  char name[] = oss.str().c_str();
  if( ret == CMEM_RCC_SUCCESS )
    ret = CMEM_GFPBPASegmentAllocate( buffer_size, name, &_cmemHandle );
  if( ret == CMEM_RCC_SUCCESS )
    ret = CMEM_SegmentPhysicalAddress( _cmemHandle, &cmemPhysAddr );
  if( ret == CMEM_RCC_SUCCESS )
    ret = CMEM_SegmentVirtualAddress( _cmemHandle, &cmemVirtAddr );
  if( ret == CMEM_RCC_SUCCESS )
    {
      buffer = (char *) cmemVirtAddr;
    }
  else
    {
      rcc_error_print( stdout, ret );
      if( !_errString.empty() ) _errString += "; ";
      std::ostringstream oss;
      oss << "CMEM_RCC error: 0x" << std::hex << ret;
      _errString += oss.str();
    }

#else
  if( (_cmemHandle = open("/dev/cmem_rcc", O_RDWR)) < 0 )
    {
      if( !_errString.empty() ) _errString += "; ";
      _errString += "Failed to open /dev/cmem_rcc";
      return 0;
    }

  if( id >= 0 )
    sprintf( _cmemDescriptor.name, "FlxUpload%d", id );
  else
    sprintf( _cmemDescriptor.name, "FlxUpload" );
  _cmemDescriptor.size    = buffer_size;
  _cmemDescriptor.order   = 0;
  _cmemDescriptor.type    = TYPE_GFPBPA;
  _cmemDescriptor.numa_id = 0;
  if( CMEM_RCC_SUCCESS == ioctl(_cmemHandle, CMEM_RCC_GET, &_cmemDescriptor) )
    {
      i64 result = (i64) mmap( 0, buffer_size, PROT_READ|PROT_WRITE,
                               MAP_SHARED, _cmemHandle,
                               (i64) _cmemDescriptor.paddr );
      if( result != -1 )
        {
          _cmemDescriptor.uaddr = result;
          if( CMEM_RCC_SUCCESS == ioctl(_cmemHandle, CMEM_RCC_SETUADDR,
                                        &_cmemDescriptor) )
            buffer = (char *) _cmemDescriptor.uaddr;

          cmemPhysAddr = _cmemDescriptor.paddr;
          //cmemVirtAddr = _cmemDescriptor.uaddr;
        }
      if( buffer == 0 )
        {
          if( !_errString.empty() ) _errString += "; ";
          _errString += "mmap/ioctl(SETUADDR) failed";
        }
    }
  else
    {
      if( !_errString.empty() ) _errString += "; ";
      _errString += "ioctl(GET) failed";
    }
#endif // USE_LIB_CMEM
  if( buffer == 0 )
    {
      _cmemStartAddr = 0;
      _cmemEndAddr   = 0;
    }
  else
    {
      _cmemStartAddr = cmemPhysAddr;
      _cmemEndAddr   = _cmemStartAddr + buffer_size;
    }

  return buffer;
}

// ----------------------------------------------------------------------------

void FlxUpload::dmaStart( u64 upload_index,
                          u64 size )
{
  if( !(_flx && _cmemStartAddr != 0 && size >= 0)  )
    return;

  if( _dmaCircular )
    {
      // Circular DMA still to be set up?
      if( (_bar0->DMA_DESC_ENABLE & (1 << _dmaIndex)) == 0 )
        {
          _flx->dma_from_host( _dmaIndex, _cmemStartAddr, _bufferSize,
                               FLX_DMA_WRAPAROUND | (_dmaTlp << 16) );

          _dmaSoftwPtr = _dmaDesc->sw_pointer;

          // Adjust TLP: set to its minimum of 32 bytes
          // (to guarantee even a single GBT-SCA reply gets uploaded)
          // ###SHOULDN'T DO IT AFTER DMA ENABLE: dma_from_host() should do it
          //_dmaDesc->tlp = 32 / 4;
        }

      // Update the read pointer for circular DMA
      _dmaSoftwPtr += size;
      if( _dmaSoftwPtr >= _cmemEndAddr )
        _dmaSoftwPtr -= _bufferSize;
      _dmaDesc->sw_pointer = _dmaSoftwPtr;
    }
  else
    {
      // Just in case...
      this->dmaStop();

      // Configure and start a single-shot DMA
      _flx->dma_from_host( _dmaIndex, _cmemStartAddr + upload_index, size,
                           0 | (_dmaTlp << 16) );
    }
}

// ----------------------------------------------------------------------------

void FlxUpload::dmaStop()
{
  if( _flx == 0 ) return;

  _flx->dma_stop( _dmaIndex );

  // Reset etc.
  //_flx->soft_reset();
  //usleep( 10000 );
  //_flx->dma_reset();
  //usleep( 10000 );
  //_flx->dma_fifo_flush(); OBSOLETE
  //usleep( 10000 );
}

// ----------------------------------------------------------------------------

bool FlxUpload::dmaActive()
{
  if( _flx == 0 ) return false;

  if( _dmaCircular )
    {
      //return( _dmaStat->fw_pointer != _dmaSoftwPtr );

      // ### Temporary?: 'fw_pointer' remains 1 TLP-size behind...
      u64 fw_ptr = _dmaStat->fw_pointer;
      if( _dmaSoftwPtr > fw_ptr )
        // If consumer (fw_ptr) is 32 bytes or less behind the producer:
        // DMA not active
        return( (_dmaSoftwPtr - fw_ptr) > 32 );
      else if( _dmaSoftwPtr < fw_ptr )
        // If consumer (fw_ptr) is somewhere at top of buffer,
        // and producer at start of buffer, DMA is still active
        return true;
      else
        return false;
    }
  else
    {
      return( (_bar0->DMA_DESC_ENABLE & (1 << _dmaIndex)) != 0 );
    }
}

// ----------------------------------------------------------------------------

int FlxUpload::formatBlockForUpload( char *block, int elink_filter )
{
  uint16_t *block_u16 = (uint16_t *) block;

  // Check for a valid block header
  if( !(block_u16[1] == BLOCK_ID ||
        block_u16[1] == BLOCK_ID_32B ||
        block_u16[1] == (BLOCK_ID_PHASE2 | _blockSizeCode) ||
        block_u16[1] == (BLOCK_ID_PHASE2_CHUNKHDRS | _blockSizeCode)) )
    return 0;

  //int elinkseqnr = (int) (((uint32_t) block[0]) | ((uint32_t) block[1] << 8));
  int elinkseqnr          = (int) block_u16[0];
  int elinknr             = (elinkseqnr & BLOCK_ELINK_MASK) >> BLOCK_ELINK_SHIFT;
  int elinknr_mapped      = mapElink( elinknr );
  int elink_filter_mapped = mapElink( elink_filter );

  // Apply Elink filter (-1 is no filter)
  if( elink_filter_mapped >= 0 && elinknr_mapped != elink_filter_mapped )
    return 0;

  // Go through the chunks in the block,
  // from *end-of-block* to *start-of-block*, storing info about each
  chunk_desc_t chnk;
  std::vector<chunk_desc_t> chunks;
  int index, nbytes;
  uint8_t *block_u8 = (uint8_t *) block;
  chnk.trunc  = false;
  chnk.err    = false;
  chnk.crcerr = false;
  chnk.invalid_sz = -1;
  // Chunk headers or trailers?
  if( block_u16[1] == (BLOCK_ID_PHASE2_CHUNKHDRS | _blockSizeCode) )
    {
      // Block with chunk headers instead of chunk trailers
      uint32_t header, i;
      index = BLOCKHEADER_BYTES;
      while( index < _blockSize )
        {
          header = 0;
          for( i=0; i<_trailerFmt.nbytes; ++i )
            header |= (block_u8[index+i] << (i*8));
          chnk.length = header & _trailerFmt.lengthMsk;
          chnk.type   = ((header & _trailerFmt.typeMsk) >> _trailerFmt.typeShift);

          // The start of this chunk
          index += _trailerFmt.nbytes;
          chnk.index = index;

          // Move to the next header; account for possible padding bytes
          if( (chnk.length & (_trailerFmt.nbytes-1)) != 0 )
            index += (chnk.length + _trailerFmt.nbytes -
                      (chnk.length & (_trailerFmt.nbytes-1)));
          else
            index += chnk.length;

          if( (chnk.type == CHUNKTYPE_BOTH ||
               chnk.type == CHUNKTYPE_MIDDLE ||
               chnk.type == CHUNKTYPE_FIRST ||
               chnk.type == CHUNKTYPE_LAST) && index >= 0 )
            // Chunk is to be processed and uploaded
            chunks.push_back( chnk );
        }

      // Format chunk-by-chunk for upload
      // from *start-of-block* to *end-of-block*,
      // so in the order in which chunks were found above,
      // taking into account FIRST, MIDDLE and LAST chunk types
      int hdr;
      if( _fromHostFormat != FH_FORMAT_REGMAP4 )
        hdr = ((elinknr_mapped << _toFlxElinkShift) | _toFlxLengthMask);
      else
        hdr = ((elinknr_mapped << _toFlxElinkShift) |
               ((_toFlxPayloadBytes/2) << TOFLX_LENGTH_SHIFT));
      char *chunkdata;
      nbytes = 0;
      for( uint32_t c=0; c<chunks.size(); ++c )
        {
          chunkdata = &block[chunks[c].index];

          if( chunks[c].type == CHUNKTYPE_BOTH )
            {
              // Format directly from block into DMA buffer
              nbytes += formatChunkForUpload( hdr, nbytes,
                                              chunkdata, chunks[c].length );
              ++_pbChunkCntr;
            }
          else if( chunks[c].type == CHUNKTYPE_LAST )
            {
              // Append to saved FIRST(+MIDDLE) part(s) in intermediate buffer
              vector<char> *ebuf = &_eBuf[elinknr];
              ebuf->insert( ebuf->end(), chunkdata, chunkdata + chunks[c].length );

              // Format as FLX upload blocks in DMA buffer
              nbytes += formatChunkForUpload( hdr, nbytes,
                                              ebuf->data(), ebuf->size() );
              ++_pbChunkCntr;

              // Clear intermediate buffer
              ebuf->clear();
            }
          else if( chunks[c].type == CHUNKTYPE_FIRST ||
                   chunks[c].type == CHUNKTYPE_MIDDLE )
            {
              // Copy to intermediate buffer
              vector<char> *ebuf = &_eBuf[elinknr];
              ebuf->insert( ebuf->end(), chunkdata, chunkdata + chunks[c].length );
            }
        }
    }
  else
    {
      uint32_t trailer, i;
      index = _blockSize - _trailerFmt.nbytes; // Last trailer in block
      while( index > BLOCKHEADER_BYTES-1 )
        {
          trailer = 0;
          for( i=0; i<_trailerFmt.nbytes; ++i )
            trailer |= (block_u8[index+i] << (i*8));
          chnk.length = trailer & _trailerFmt.lengthMsk;
          chnk.type   = ((trailer & _trailerFmt.typeMsk) >> _trailerFmt.typeShift);

          // The start of this chunk; account for possible padding bytes
          if( (chnk.length & (_trailerFmt.nbytes-1)) != 0 )
            index -= (chnk.length + _trailerFmt.nbytes -
                      (chnk.length & (_trailerFmt.nbytes-1)));
          else
            index -= chnk.length;
          chnk.index = index;

          // Move to the preceeding trailer
          index -= _trailerFmt.nbytes;

          if( (chnk.type == CHUNKTYPE_BOTH ||
               chnk.type == CHUNKTYPE_MIDDLE ||
               chnk.type == CHUNKTYPE_FIRST ||
               chnk.type == CHUNKTYPE_LAST) && index >= 0 )
            // Chunk is to be processed and uploaded
            chunks.push_back( chnk );
        }

      // Format chunk-by-chunk for upload
      // from *start-of-block* to *end-of-block*,
      // so reverse order in which chunks were found above,
      // taking into account FIRST, MIDDLE and LAST chunk types
      int hdr;
      if( _fromHostFormat != FH_FORMAT_REGMAP4 )
        hdr = ((elinknr_mapped << _toFlxElinkShift) | _toFlxLengthMask);
      else
        hdr = ((elinknr_mapped << _toFlxElinkShift) |
               ((_toFlxPayloadBytes/2) << TOFLX_LENGTH_SHIFT));
      char *chunkdata;
      int c;
      nbytes = 0;
      for( c=chunks.size()-1; c>=0; --c )
        {
          chunkdata = &block[chunks[c].index];

          if( chunks[c].type == CHUNKTYPE_BOTH )
            {
              // Format directly from block into DMA buffer
              nbytes += formatChunkForUpload( hdr, nbytes,
                                              chunkdata, chunks[c].length );
              ++_pbChunkCntr;
            }
          else if( chunks[c].type == CHUNKTYPE_LAST )
            {
              // Append to saved FIRST(+MIDDLE) part(s) in intermediate buffer
              vector<char> *ebuf = &_eBuf[elinknr];
              ebuf->insert( ebuf->end(), chunkdata, chunkdata + chunks[c].length );

              // Format as FLX upload blocks in DMA buffer
              nbytes += formatChunkForUpload( hdr, nbytes,
                                              ebuf->data(), ebuf->size() );
              ++_pbChunkCntr;

              // Clear intermediate buffer
              ebuf->clear();
            }
          else if( chunks[c].type == CHUNKTYPE_FIRST ||
                   chunks[c].type == CHUNKTYPE_MIDDLE )
            {
              // Copy to intermediate buffer
              vector<char> *ebuf = &_eBuf[elinknr];
              ebuf->insert( ebuf->end(), chunkdata, chunkdata + chunks[c].length );
            }
        }
    }

  return nbytes;
}

// ----------------------------------------------------------------------------

int FlxUpload::formatChunkForUpload( int hdr, int dest_index,
                                     char *chunkdata, int chunksize )
{
  // Format and write the chunk data to the DMA buffer for upload
  int nblocks = (chunksize + _toFlxPayloadBytes-1) / _toFlxPayloadBytes;
  int final_bytes, final_size, final_hdr;
  final_bytes = chunksize - (nblocks-1) * _toFlxPayloadBytes;
  if( _fromHostFormat != FH_FORMAT_REGMAP4 )
    {
      final_hdr  = hdr & ~_toFlxLengthMask;
      final_hdr |= final_bytes; // Length in bytes
    }
  else
    {
      final_size = (final_bytes + 1) / 2; // Length in 2-byte units
      final_hdr  = hdr & ~_toFlxLengthMask;
      final_hdr |= ((final_size << TOFLX_LENGTH_SHIFT) | TOFLX_EOM_MASK);
    }

  //char *buf = &_dataBuffer[dest_index];
  //i64 buf_i = dest_index; // OLD method (one-shot DMAs from address 0)
  i64 buf_i = _writeIndex;
  static long cntr = 0;
  //unsigned char *p = (unsigned char *) chunkdata;

  // Fully-filled FromHost byte blocks
  int i, j;
  for( i=0; i<nblocks-1; ++i )
    {
      // SPECIAL: fill chunk with random data
      // (catch errors provoked by a certain byte order?)
      if( _pbRandomData )
        {
          for( j=_toFlxPayloadBytes-1; j>=0; --j )
            if( (i*_toFlxPayloadBytes+j) >= CHUNKHDR_SIZE )
              //chunkdata[j] = (char) 0x11;
              //chunkdata[j] = (char) (hdr >> TOFLX_ELINK_SHIFT);//Data=elinknr
              chunkdata[j] = (char) (random() & 0xFF);
        }

      for( j=_toFlxPayloadBytes-1; j>=0; --j, ++buf_i )
        _dataBuffer[buf_i] = chunkdata[j];
      chunkdata += _toFlxPayloadBytes;
      for( j=0; j<_toFlxHeaderSize; ++j, ++buf_i )
        _dataBuffer[buf_i] = (char) ((hdr >> j*8) & 0xFF);
      if( buf_i >= _bufferSize ) buf_i = 0;
    }

  // SPECIAL: fill chunk with random data
  // (catch errors provoked by a certain byte sequences?)
  if( _pbRandomData )
    {
      for( j=final_bytes-1; j>=0; --j )
        if( (i*_toFlxPayloadBytes+j) >= CHUNKHDR_SIZE )
          //chunkdata[j] = (char) 0x11;
          //chunkdata[j] = (char) (hdr >> TOFLX_ELINK_SHIFT);//Data=elinknr
          chunkdata[j] = (char) (random() & 0xFF);
      //unsigned char *p = (unsigned char *) chunkdata;
      // A counter in order to identify the chunk that generates an error
      //p[final_bytes-1] = ((cntr>>24) & 0xFF);
      //p[final_bytes-2] = ((cntr>>16) & 0xFF);
      //p[final_bytes-3] = ((cntr>> 8) & 0xFF);
      //p[final_bytes-4] = ((cntr>> 0) & 0xFF);
      // Following bytes to start at a 4-byte aligned position
      // ### 8-bit E-link: high likelyhood of receiving only byt 8d,
      // loosing 91, 6e, e7 and the next byte:
      //p[final_bytes-9+2]  = 0xe7;
      //p[final_bytes-10+2] = 0x6e;
      //p[final_bytes-11+2] = 0x91;
      //p[final_bytes-12+2] = 0x8d;
      // ### And this sequence has the same effect:
      //p[final_bytes-13+2] = 0xb3;
      //p[final_bytes-14+2] = 0x71;
      //p[final_bytes-15+2] = 0x8e;
      //p[final_bytes-16+2] = 0x92;
    }

  // After determining which random block (check the counter!)
  // triggers a problem use the following code to dump that block's content
  // to compare to the received data chunk to find out which byte sequence
  // in particular triggers the problem
  //if( random_data && cntr == 0x1d2de3 )
  /*if( random_data && cntr == 0x29fd4a )
    {
      cout << hex << endl;
      for( int k=0; k<200; ++k )
        {
          cout << setw(2) << ((u32) p[k] & 0xFF) << " ";
          if( (k & 0x1F) == 0x1F ) cout << endl;
        }
      cout << endl;
      }*/
  ++cntr;

  // Final (likely not fully filled) FromHost byte block
  for( j=_toFlxPayloadBytes-1; j>=final_bytes; --j, ++buf_i )
    _dataBuffer[buf_i] = _fillerByte; // 'Empty' block bytes
  for( j=final_bytes-1; j>=0; --j, ++buf_i )
    _dataBuffer[buf_i] = chunkdata[j]; // Final data bytes
  for( j=0; j<_toFlxHeaderSize; ++j, ++buf_i )
    _dataBuffer[buf_i] = (char) ((final_hdr >> j*8) & 0xFF);
  if( buf_i >= _bufferSize ) buf_i = 0;

  _writeIndex = buf_i;

  return( nblocks * _toFlxBlockBytes );
}

// ----------------------------------------------------------------------------

int FlxUpload::scaControl( int elink, int control )
{
  int sca_addr = 0; // Always 0?
  // Compose control HDLC frame for GBT-SCA: SOF+ADDR+CTRL, FCS, EOF
  u8 frame[2+2];
  frame[0] = (u8) (sca_addr & 0xFF);
  frame[1] = (u8) (control & 0xFF);
  u16 crc = crc16( frame, 2 );
  frame[2] = (u8) ((crc >> 8) & 0xFF);
  frame[3] = (u8) (crc & 0xFF);
  int upload_sz = this->prepareData( elink, sizeof(frame), frame );
  while( !this->uploadFinished() )
    {
      //cout << "scaControl upload:" << _bytesToUpload << endl;
      this->upload();
    }
  return upload_sz;
}

// ----------------------------------------------------------------------------

void FlxUpload::pause( int delay_us )
{
  // Use usleep() when appropriate, but run a busy-wait loop for short delays
  // (determined by usleep() overhead) to achieve better timing precision
  if( delay_us > _minimumUsleep )
    {
      usleep( delay_us - _minimumUsleep );
    }
  else if( delay_us > 0 )
    {
      struct timespec ts1, ts2;
      volatile long s, t;
      clock_gettime( CLOCK_REALTIME, &ts1 );
      s = ts1.tv_sec*1000000000 + ts1.tv_nsec;
      clock_gettime( CLOCK_REALTIME, &ts2 );
      t = ((ts2.tv_sec*1000000000 + ts2.tv_nsec) - s)/1000;
      while( t < delay_us )
        {
          clock_gettime( CLOCK_REALTIME, &ts2 );
          t = ((ts2.tv_sec*1000000000 + ts2.tv_nsec) - s)/1000;
        }
    }
}

// ----------------------------------------------------------------------------

int FlxUpload::minimumUsleep()
{
  // Determine the time overhead it takes to execute usleep(1), in microseconds
  struct timespec ts1, ts2;
  clock_gettime( CLOCK_REALTIME, &ts1 );
  // Average over 4 calls..
  usleep( 1 );
  usleep( 1 );
  usleep( 1 );
  usleep( 1 );
  clock_gettime( CLOCK_REALTIME, &ts2 );
  long t = ((ts2.tv_sec*1000000000 + ts2.tv_nsec) -
            (ts1.tv_sec*1000000000 + ts1.tv_nsec))/4;
  if( t < 1000 || t > 1000000 )
    t = 50000;
  return (t-1000+500)/1000; // In microseconds
}

// ----------------------------------------------------------------------------

int FlxUpload::mapElink( int elinknr )
{
#if REGMAP_VERSION >= 0x500
  if( !_mapElink )
    return elinknr;

  // EC+IC+AUX egroup? (sticking to the RM4 numbering)
  if( elinknr >= 0 &&
      ((elinknr & BLOCK_EGROUP_MASK) >> BLOCK_EGROUP_SHIFT) == 7 )
    {
      int epath = elinknr & BLOCK_EPATH_MASK;
      elinknr &= ~(BLOCK_EGROUP_MASK | BLOCK_EPATH_MASK);

      // Read EC and IC number/index from a register
      if( epath == 7 ) // EC link
        elinknr |= _bar2->AXI_STREAMS_FROMHOST.EC_INDEX;
      if( epath == 6 ) // IC link
        elinknr |= _bar2->AXI_STREAMS_FROMHOST.IC_INDEX;
      //if( epath == 5 ) // AUX link
      //  elinknr |= 18;

      /* ###OLD HARDCODED APPROACH:
      // FromHost direction: GBTX: EC=40,IC=41; lpGBT: EC=16,IC=17
      // Is it lpGBT-type firmware?
      if( _flx->lpgbt_type() )
        {
          if( epath == 7 ) elinknr |= 16; // EC link
          if( epath == 6 ) elinknr |= 17; // IC link
          //if( epath == 5 ) elinknr |= 18; // AUX link
        }
      else
        {
          if( epath == 7 ) elinknr |= 40; // EC link
          if( epath == 6 ) elinknr |= 41; // IC link
          //if( epath == 5 ) elinknr |= 42; // AUX link
        }
      */
    }
  if( _toFlxHeaderFields8Bit )
    elinknr = mapElinkTo8BitFieldHeader( elinknr );
#endif // REGMAP_VERSION
  return elinknr;
}

// ----------------------------------------------------------------------------

int FlxUpload::mapElinkTo8BitFieldHeader( int elinknr )
{
  // This function is meant to maintain compatibility between a new 32-bit
  // FromHost header format defined in which size, e-link index and link number
  // are all 8-bit fields, and firmware with a 32-bit header with 6-bit size,
  // 6-bit e-link index (3-bit group + 3-bit path) and 5-bit link number.
  // In this new header the e-link number has to be expressed using 8-bit
  // fields, while externally maintaining the e-link numbering as before,
  // hence this mapping function.
#if REGMAP_VERSION >= 0x500
  if( elinknr >= 0 )
    {
      int linknr = (elinknr & BLOCK_LNK_MASK) >> BLOCK_LNK_SHIFT;
      int index  = elinknr & 0x3F;
      // Map to link number and index as 8-bit fields
      elinknr = (linknr << 8) | index;
    }
#endif // REGMAP_VERSION
  return elinknr;
}

// ----------------------------------------------------------------------------
